# Magni

## Fonctionnalités

* Gestion des activités sportives dans différents sports (course à pied, cyclisme, natation, etc.)
* Graphe affichant les données d'une activité au cours du temps (fréquence cardiaque, vitesse, altitude, cadence, puissance).
* Découpage d'une activités en multiples segments (par exemple pour identifier les répétitions lors d'une séance de fractionné).
* Chargement des fichiers au format TCX et FIT.
* Calcul de la charge d'entrainement basé sur le TRIMP (TRaining IMPulse) et affichage d'un graphe avec l'évolution au cours du temps.
* Calendrier mensuel.
* Calcul des meilleures performances sur des distances ou durées prédéfinies, et estimation des meilleures performances possibles.
* Stastiques annuelles, mensuelles et hebdomadaires.
* Gestion des équipements sportifs (paires de chaussure, montre, vélo).
* Suivi des compétitions avec le résultat et le classement obtenus.
* Définition d'objectifs basés sur des critères automatiques (nombre d'activités par an, record à battre).

## Installation

* Python 3
* Modules Python : django, fitparse, mysqlclient
* Serveur Web

## Captures d'écran

![Calendrier](./screenshots/calendar.png)
![Statistiques](./screenshots/stats.png)
![Meilleures performances](./screenshots/records.png)
![Estimation des meilleures performances](./screenshots/forecast.png)
![Progression de la charge d'entrainement](./screenshots/progression.png)
