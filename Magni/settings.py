import os
from django.utils.translation import gettext_lazy as _


# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
BASE_URL = "/"

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "3d2f*+s9)poj9g#^6j3i6-qr)o$vopk)^dql&^ir0-^r@9v9*9"

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False
DEBUG_TOOLBAR = False

ALLOWED_HOSTS = []

INTERNAL_IPS = ["127.0.0.1"]

CSRF_COOKIE_PATH = BASE_URL
SESSION_COOKIE_PATH = BASE_URL
SESSION_COOKIE_NAME = "sessionid"

# Application definition
INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",

    "apps.base.apps.BaseAppConfig",
    "apps.user.apps.UserAppConfig",
    "apps.competitions.apps.CompetitionsAppConfig",
    "apps.activities.apps.ActivitiesAppConfig",
    "apps.equipments.apps.EquipmentsAppConfig",
    "apps.goals.apps.GoalsAppConfig",
    "apps.weight.apps.WeightAppConfig",
]

if DEBUG and DEBUG_TOOLBAR:
    INSTALLED_APPS += ["debug_toolbar"]
    MIDDLEWARE = ["debug_toolbar.middleware.DebugToolbarMiddleware"]
else:
    MIDDLEWARE = []

MIDDLEWARE += [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "apps.user.middleware.TimezoneUserMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

ROOT_URLCONF = "Magni.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [os.path.join(BASE_DIR, "templates")],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "apps.user.context_processors.notifications",
            ],
        },
    },
]

WSGI_APPLICATION = "Magni.wsgi.application"

# Database
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.mysql",
        "NAME": "sport",
        "USER": "sport",
        "PASSWORD": "sport",
        "HOST": "localhost",
        "PORT": "3306",
        "OPTIONS": {
            "init_command": "SET sql_mode='STRICT_TRANS_TABLES'",
        },
    },
    "sqlite": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": os.path.join(BASE_DIR, "db.sqlite3"),
    }
}

DEFAULT_AUTO_FIELD = "django.db.models.AutoField"

# Password validation
AUTH_PASSWORD_VALIDATORS = []

# Internationalization
#LANGUAGE_CODE = "fr-fr"
LANGUAGE_CODE = "fr"
TIME_ZONE = "Europe/Paris"
USE_I18N = True
USE_L10N = True
USE_TZ = True

LOCALE_PATHS = [
    os.path.join(BASE_DIR, "locale"),
]

LANGUAGES = [
  ("en", _("English")),
  ("fr", _("French")),
]

# Static files (CSS, JavaScript, Images)
STATIC_ROOT = os.path.join(BASE_DIR, "static-files/")
STATIC_URL = BASE_URL + "static/"

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static"),
]

MEDIA_ROOT = os.path.join(BASE_DIR, "media-files")
MEDIA_PATH = "media/"
MEDIA_URL = BASE_URL + MEDIA_PATH

LOGIN_URL = BASE_URL + "auth/login"
LOGIN_REDIRECT_URL = "home"
AUTH_USER_MODEL = "user.User"

MAX_UPLOAD_FILESIZE = 15 * 1024 * 1024
MAX_UPLOAD_COUNT = 20
USER_MAX_TOTAL_FILESIZE = 100 * 1024 * 1024
