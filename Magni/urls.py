from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path, register_converter

import apps.base.views


class IdUrlConverter:
    regex = "[0-9]{6}"

    def to_python(self, value):
        return int(value)

    def to_url(self, value):
        return "%06d" % value


register_converter(IdUrlConverter, "id")


urlpatterns = [
    path("", apps.base.views.view_index, name="home"),
    path("privacy", apps.base.views.view_privacy, name="privacy"),

    path("activities/", include("apps.activities.urls")),
    path("user/", include("apps.user.urls")),
    path("equipments/", include("apps.equipments.urls")),
    path("weight/", include("apps.weight.urls")),
    path("competitions/", include("apps.competitions.urls")),
    path("goals/", include("apps.goals.urls")),
    path("admin/", admin.site.urls),
    path("auth/", include("django.contrib.auth.urls")),
] + static(settings.MEDIA_PATH, document_root=settings.MEDIA_ROOT)

if settings.DEBUG and settings.DEBUG_TOOLBAR:
    import debug_toolbar
    urlpatterns = [
        path("__debug__/", include(debug_toolbar.urls)),
    ] + urlpatterns
