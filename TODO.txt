
Activités :
- Afficher les vignettes pour chaque équipement dans le formulaire d'édition d'une activité.
- Utiliser une icône distincte pour les activités de type "Home trainer".
- Quand on recalcule les données d'une activité à partir d'un autre fichier, si cela décale le début de l'activité, les meilleures performances et les segments ne sont pas décalés.
- Améliorer la gestion des "tracks" des activités : pour chaque fichier, pouvoir choisir de retenir ou pas chaque série de données (fréquence cardiaque, altitude, etc.) ; pouvoir arrondir à la seconde.
- Chaque fichier devrait avoir un "TrackFile" associé.

Calendrier :
- Ajouter un bouton pour afficher ou masquer les activités.
- Définir une couleur pour chaque note.

Objectifs :
- Pouvoir définir un objectif sous forme de texte, et la possibilité de valider l'objectif.

Équipements :
- Améliorer l'affichage de la liste (trier par type d'équipement ?).
- Afficher la dernière utilisation ?
