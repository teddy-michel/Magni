
from django.utils.translation import gettext as _


competitions_list = {
    "RoadRace":  {"name": _("Road race")},
    "TrailRace":  {"name": _("Trail race")},
    "TrackRace":  {"name": _("Track race")},
    "Duathlon": {"name": _("Duathlon")},
    "Triathlon": {"name": _("Triathlon")},
}
