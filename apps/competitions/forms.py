
import datetime
import re

from django.core.exceptions import ValidationError
from django.forms import ModelForm
from django.forms import Textarea, TextInput, DateInput, CharField, DateField, TimeField, TimeInput
from django.utils import timezone
from django.utils.timezone import localtime, make_aware
from django.utils.translation import gettext as _, pgettext

from apps.base.forms import HTML5DateInput, convert_duration_string
from apps.competitions.models import Competition


class CompetitionForm(ModelForm):
    date_date = DateField(label=_("Date"), required=True, widget=HTML5DateInput(format="%Y-%m-%d"))
    #date_time = TimeField(label=pgettext("datetime", "Time"), required=True, widget=HTML5TimeInput)
    date_time = TimeField(label=pgettext("datetime", "Time"), required=True)
    duration_txt = CharField(label=_("Official time"), max_length=20, required=False, widget=TextInput(attrs={"placeholder": "HH:MM:SS"}))
    duration_real_txt = CharField(label=_("Real time"), max_length=20, required=False, widget=TextInput(attrs={"placeholder": "HH:MM:SS"}))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        try:
            s, ms = divmod(self.instance.duration, 1000)
            value_h = s // 3600
            value_m = (s % 3600) // 60
            value_s = (s % 60)
            self.initial["duration_txt"] = _("%(hours)02d:%(minutes)02d:%(seconds)02d") % {"hours": value_h, "minutes": value_m, "seconds": value_s}
        except:
            pass

        try:
            s, ms = divmod(self.instance.duration_real, 1000)
            value_h = s // 3600
            value_m = (s % 3600) // 60
            value_s = (s % 60)
            self.initial["duration_real_txt"] = _("%(hours)02d:%(minutes)02d:%(seconds)02d") % {"hours": value_h, "minutes": value_m, "seconds": value_s}
        except:
            pass

        now = timezone.now()
        self.initial["date_date"] = now
        self.initial["date_time"] = now.strftime("%H:%M")

        try:
            self.initial["date_date"] = localtime(self.instance.date)
            self.initial["date_time"] = localtime(self.instance.date).strftime("%H:%M")
        except:
            pass

    def clean_duration_txt(self):
        duration = convert_duration_string(self.cleaned_data["duration_txt"])
        if duration is None:
            raise ValidationError(_("Invalid format."))
        elif duration == 0:
            return None
        else:
            return duration

    def clean_duration_real_txt(self):
        duration = convert_duration_string(self.cleaned_data["duration_real_txt"])
        if duration is None:
            raise ValidationError(_("Invalid format."))
        elif duration == 0:
            return None
        else:
            return duration

    def save(self, commit=True):
        duration = self.cleaned_data.get("duration_txt")
        self.instance.duration = duration

        duration_real = self.cleaned_data.get("duration_real_txt")
        self.instance.duration_real = duration_real

        sdate = self.cleaned_data.get("date_date")
        stime = self.cleaned_data.get("date_time")
        self.instance.date = make_aware(datetime.datetime.combine(sdate, stime))

        return super().save(commit=commit)

    class Meta:
        model = Competition
        fields = ["name", "date_date", "date_time", "type", "description", "duration_txt", "duration_real_txt",
                  "place", "participants", "place_category", "participants_category"]
        widgets = {
            "description": Textarea(attrs={"rows": 4}),
        }
