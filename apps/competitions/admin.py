from django.contrib import admin

from apps.competitions.models import Competition


@admin.register(Competition)
class CompetitionAdmin(admin.ModelAdmin):
    list_display = ("date", "name", "type", "user")
