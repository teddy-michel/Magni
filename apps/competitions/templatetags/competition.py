from django import template

from apps.competitions.models import competitions_list

register = template.Library()


@register.filter()
def competition_name(value):
    if value in competitions_list:
        return competitions_list[value]["name"]
    else:
        return value
