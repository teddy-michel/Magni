import datetime

from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.shortcuts import redirect, render

from apps.competitions.forms import CompetitionForm
from apps.competitions.models import Competition
from apps.activities.models import Activity


@login_required
def view_competitions(request):
    competitions1 = []
    competitions2 = []

    competitions_query_set = Competition.objects.filter(user=request.user).order_by("-date")
    for competition in competitions_query_set.all():
        if competition.duration or competition.duration_real or competition.place or competition.activities.count() > 0:
            competitions2.append(competition)
        else:
            competitions1.append(competition)

    return render(request, "competitions/competitions.html", {"competitions1": competitions1, "competitions2": competitions2})


@login_required
def view_competition(request, cid):
    try:
        competition = Competition.objects.get(id=cid, user=request.user)
    except Competition.DoesNotExist:
        return redirect("competitions")

    return render(request, "competitions/competition.html", {"competition": competition})


@login_required
def view_competition_create(request):
    if request.method == "POST":
        form = CompetitionForm(request.POST)
        if form.is_valid():
            competition = form.save(commit=False)
            competition.user = request.user
            competition.save()
            return redirect("competition", competition.id)
    else:
        form = CompetitionForm()

    return render(request, "competitions/competition_create.html", {"form": form})


@login_required
def view_competition_edit(request, cid):
    try:
        competition = Competition.objects.get(id=cid, user=request.user)
    except Competition.DoesNotExist:
        return redirect("competitions")

    if request.method == "POST":
        form = CompetitionForm(request.POST, instance=competition)
        if form.is_valid():
            competition = form.save()
            return redirect("competition", competition.id)
    else:
        form = CompetitionForm(instance=competition)

    return render(request, "competitions/competition_edit.html", {"competition": competition, "form": form})


@login_required
def view_competition_remove(request, cid):
    try:
        competition = Competition.objects.get(id=cid, user=request.user)
    except Competition.DoesNotExist:
        return redirect("competitions")

    if request.method == "POST" and "delete" in request.POST:
        competition.delete()
        return redirect("competitions")

    return render(request, "competitions/competition_remove.html", {"competition": competition})


@login_required
def view_competition_associate_activity(request, cid):
    try:
        competition = Competition.objects.get(id=cid, user=request.user)
    except Competition.DoesNotExist:
        return redirect("competitions")

    if request.method == "POST":
        new_activities = []
        for act_id in request.POST.getlist("activity", []):
            try:
                activity = Activity.objects.get(id=act_id, user=request.user)
                new_activities.append(activity)
            except Activity.DoesNotExist:
                pass

        old_activities = []
        for activity in Activity.objects.filter(competition=competition):
            old_activities.append(activity)

        for activity in set(new_activities) - set(old_activities):
            activity.competition = competition
            activity.save()
        for activity in set(old_activities) - set(new_activities):
            activity.competition = None
            activity.save()

        return redirect("competition", competition.id)
    else:
        # Matching activities
        date_min = competition.date - datetime.timedelta(days=1.0)
        date_max = competition.date + datetime.timedelta(days=1.0)
        activities = Activity.objects.filter(Q(user=request.user, planned=False, date__gte=date_min, date__lte=date_max) | Q(user=request.user, competition=competition))

        return render(request, "competitions/competition_associate_activity.html", {"competition": competition, "activities": activities})
