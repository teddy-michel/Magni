
from django.db import models
from django.utils.translation import gettext as _

from apps.competitions.globals import competitions_list
from apps.user.models import User


class Competition(models.Model):
    date = models.DateTimeField(_("date"))
    user = models.ForeignKey(User, verbose_name=_("user"), related_name="competitions", on_delete=models.CASCADE)
    name = models.CharField(_("competition name"), max_length=100)
    type = models.CharField(_("competition type"), max_length=100, choices=[(a, b["name"]) for a, b in competitions_list.items()])
    description = models.TextField(_("description"), max_length=1000, blank=True)
    duration = models.PositiveIntegerField(_("official time"), blank=True, null=True)
    duration_real = models.PositiveIntegerField(_("real time"), blank=True, null=True)
    place = models.PositiveIntegerField(_("place"), blank=True, null=True)
    participants = models.PositiveIntegerField(_("participants"), blank=True, null=True)
    place_category = models.PositiveIntegerField(_("place in category"), blank=True, null=True)
    participants_category = models.PositiveIntegerField(_("participants in category"), blank=True, null=True)

    def __str__(self):
        return "%s - %s" % (self.name, str(self.date))

    class Meta:
        verbose_name = _("Competition")
        verbose_name_plural = _("Competitions")
        ordering = ["-date"]
