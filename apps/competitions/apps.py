from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class CompetitionsAppConfig(AppConfig):
    name = "apps.competitions"
    verbose_name = _("Competitions")
