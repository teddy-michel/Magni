from django.urls import path

from apps.competitions import views


urlpatterns = [
    path("", views.view_competitions, name="competitions"),
    path("add", views.view_competition_create, name="add_competition"),
    path("<id:cid>", views.view_competition, name="competition"),
    #path("<int:cid>", views.view_competition_redirect),
    path("<id:cid>/edit", views.view_competition_edit, name="competition_edit"),
    #path("<int:cid>/edit", views.view_competition_edit_redirect),
    path("<id:cid>/remove", views.view_competition_remove, name="competition_remove"),
    #path("<int:cid>/remove", views.view_competition_remove_redirect),
    path("<id:cid>/associate_activity", views.view_competition_associate_activity, name="competition_associate_activity"),
    #path("<int:cid>/associate_activity", views.view_competition_associate_activity_redirect),
]
