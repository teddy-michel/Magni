from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class WeightAppConfig(AppConfig):
    name = "apps.weight"
    verbose_name = _("Weight")
