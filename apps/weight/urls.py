from django.urls import path

from apps.weight import views


urlpatterns = [
    path("", views.view_weights, name="weights"),
    path("add", views.view_weight_create, name="add_weight"),
    path("<id:wid>/edit", views.view_weight_edit, name="edit_weight"),
    path("<int:wid>/edit", views.view_weight_edit_redirect),
    path("<id:wid>/remove", views.view_weight_remove, name="remove_weight"),
    path("<int:wid>/remove", views.view_weight_remove_redirect),
]
