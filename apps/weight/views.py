import datetime

from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render
from django.utils import timezone
from django.utils.timezone import make_aware

from apps.weight.forms import WeightForm
from apps.weight.models import Weight


@login_required
def view_weights(request):
    current_year = timezone.now().year
    weights = Weight.objects.filter(user=request.user)

    date_min = date_max = None
    weight_min = weight_max = None
    for weight in weights:
        if date_min is None or weight.date < date_min:
            date_min = weight.date
        if date_max is None or weight.date > date_max:
            date_max = weight.date
        if weight_min is None or weight.weight < weight_min:
            weight_min = weight.weight
        if weight_max is None or weight.weight > weight_max:
            weight_max = weight.weight

    if date_min is not None and date_min < make_aware(datetime.datetime(current_year, 1, 1)):
        no_prev_years = False
    else:
        no_prev_years = True

    if date_max is not None and date_max >= make_aware(datetime.datetime(current_year + 1, 1, 1)):
        no_next_years = False
    else:
        no_next_years = True

    if weight_min is None or weight_min <= 2000:
        weight_min = 0
    else:
        weight_min -= 2000
        weight_min //= 10000
        weight_min *= 10000

    if weight_max is None or weight_max <= 0:
        weight_max = 10000
    else:
        weight_max += 12000
        weight_max //= 10000
        weight_max *= 10000

    return render(request, "weight/weights.html", {
        "weights": weights,
        "current_year": current_year,
        "no_prev_years": no_prev_years,
        "no_next_years": no_next_years,
        "weight_min": weight_min,
        "weight_max": weight_max,
    })


@login_required
def view_weight_create(request):
    if request.method == "POST":
        form = WeightForm(request.POST)
        if form.is_valid():
            weight = form.save(commit=False)
            weight.user = request.user
            weight.save()
            return redirect("weights")
    else:
        now = timezone.now()
        now = now.replace(microsecond=0)
        now = now.replace(second=0)
        form = WeightForm(initial={"date": now})

    return render(request, "weight/weight_create.html", {"form": form})


@login_required
def view_weight_edit(request, wid):
    try:
        weight = Weight.objects.get(id=wid, user=request.user)
    except Weight.DoesNotExist:
        return redirect("weights")

    if request.method == "POST":
        form = WeightForm(request.POST, instance=weight)
        if form.is_valid():
            form.save()
            return redirect("weights")
    else:
        form = WeightForm(instance=weight)

    return render(request, "weight/weight_edit.html", {"weight": weight, "form": form})


@login_required
def view_weight_edit_redirect(request, wid):
    return redirect("weight_edit", wid)


@login_required
def view_weight_remove(request, wid):
    try:
        weight = Weight.objects.get(id=wid, user=request.user)
    except Weight.DoesNotExist:
        return redirect("weights")

    if request.method == "POST" and "delete" in request.POST:
        weight.delete()
        return redirect("weights")

    return render(request, "weight/weight_remove.html", {"weight": weight})


@login_required
def view_weight_remove_redirect(request, wid):
    return redirect("weight_remove", wid)
