import datetime

from django.forms import DateField, TimeField
from django.forms import ModelForm
from django.utils import timezone
from django.utils.timezone import localtime, make_aware
from django.utils.translation import gettext as _, pgettext

from apps.base.forms import HTML5DateInput
from apps.weight.models import Weight


class WeightForm(ModelForm):
    date_date = DateField(label=_("Date"), required=True, widget=HTML5DateInput(format="%Y-%m-%d"))
    #date_time = TimeField(label=pgettext("datetime", "Time"), required=True, widget=HTML5TimeInput)
    date_time = TimeField(label=pgettext("datetime", "Time"), required=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        now = timezone.now()
        self.initial["date_date"] = now
        self.initial["date_time"] = now.strftime("%H:%M")

        try:
            self.initial["date_date"] = localtime(self.instance.date)
            self.initial["date_time"] = localtime(self.instance.date).strftime("%H:%M")
        except:
            pass

    def save(self, commit=True):
        sdate = self.cleaned_data.get("date_date")
        stime = self.cleaned_data.get("date_time")
        self.instance.date = make_aware(datetime.datetime.combine(sdate, stime))
        return super().save(commit=commit)

    class Meta:
        model = Weight
        fields = ["date_date", "date_time", "weight"]
