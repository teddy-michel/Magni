from django.contrib import admin

from apps.weight.models import Weight


@admin.register(Weight)
class WeightAdmin(admin.ModelAdmin):
    list_display = ("date", "weight", "user")
