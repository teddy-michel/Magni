from django.db import models
from django.utils.translation import gettext as _

from apps.user.models import User


class Weight(models.Model):
    date = models.DateTimeField(_("date"))
    weight = models.PositiveIntegerField(_("weight"), blank=False)
    user = models.ForeignKey(User, verbose_name=_("user"), related_name="weights", on_delete=models.CASCADE)

    def __str__(self):
        return "%s for %s (%d)" % (str(self.date), str(self.user), self.weight)

    class Meta:
        verbose_name = _("Weight")
        verbose_name_plural = _("Weights")
        ordering = ["date"]
