from django import template
from django.utils.safestring import mark_safe

register = template.Library()


@register.filter
def weight_format(value):
    if value is None:
        value = 0
    if not isinstance(value, float) and not isinstance(value, int):
        return value
    return mark_safe("%.2f&nbsp;kg" % (value / 1000))
