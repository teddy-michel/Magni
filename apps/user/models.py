import json

import pytz
from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import AbstractUser
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils.translation import gettext as _, pgettext

from Magni.settings import TIME_ZONE


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """Create and save a User with the given email and password."""
        if not email:
            raise ValueError("The given email must be set")
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        """Create and save a regular User with the given email and password."""
        extra_fields.setdefault("is_staff", False)
        extra_fields.setdefault("is_superuser", False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """Create and save a SuperUser with the given email and password."""
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True.")
        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")

        return self._create_user(email, password, **extra_fields)


class User(AbstractUser):
    username = None
    first_name = models.CharField(_("first name"), max_length=30)
    last_name = models.CharField(_("last name"), max_length=150)
    email = models.EmailField(_("email address"), unique=True)
    gender = models.CharField(_("gender"), max_length=1, choices=(('U', _("Unspecified")), ('M', _("Male")), ('F', _("Female")), ('O', pgettext("gender", "Other"))), default='U')
    birthdate = models.DateField(_("birth date"), null=True, blank=True)
    timezone = models.CharField(_("time zone"), max_length=100, choices=zip(pytz.common_timezones + ["Europe/Paris"], pytz.common_timezones + ["Europe/Paris"]), default=TIME_ZONE)
    heartrate_rest = models.PositiveSmallIntegerField(_("resting heart rate"), validators=[MinValueValidator(30), MaxValueValidator(100)], default=60)
    heartrate_max = models.PositiveSmallIntegerField(_("maximum heart rate"), validators=[MinValueValidator(120), MaxValueValidator(220)], default=200)
    threshold = models.PositiveSmallIntegerField(_("threshold"), validators=[MinValueValidator(150), MaxValueValidator(900)], default=360)
    ftp = models.PositiveSmallIntegerField(_("FTP"), validators=[MaxValueValidator(600)], default=150)
    use_pace = models.BooleanField(_("show pace instead of speed"), default=False)
    force_timezone_import = models.BooleanField(_("force timezone when importing activity"), default=False)
    data = models.TextField(_("data"), blank=True, null=True)  # format JSON

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["first_name", "last_name"]

    objects = UserManager()

    def __str__(self):
        return self.get_full_name()


class Notification(models.Model):
    date = models.DateTimeField(_("date"))
    user = models.ForeignKey(User, verbose_name=_("user"), related_name="notifications", on_delete=models.CASCADE)
    text = models.CharField(_("text"), max_length=150)
    icon = models.CharField(_("icon"), max_length=50, blank=True, null=True)
    url = models.CharField(_("URL"), max_length=150, blank=True, null=True)
    viewed = models.BooleanField(_("viewed"), default=False)

    def __str__(self):
        return "%s - %s - %s" % (self.date, self.user, self.text)

    class Meta:
        verbose_name = _("Notification")
        verbose_name_plural = _("Notifications")
        ordering = ["-date"]


def export_user_json(user):
    data = {
        "first_name": user.first_name,
        "last_name": user.last_name,
        "email": user.email,
        "gender": user.gender,
        "birthdate": user.birthdate,
        "timezone": user.timezone,
        "heartrate_rest": user.heartrate_rest,
        "heartrate_max": user.heartrate_max,
        "threshold": user.threshold,
        "ftp": user.ftp,
    }

    goals = []
    if user.data:
        try:
            user_data = json.loads(user.data)
        except:
            user_data = {}

        if "goals" in user_data and user_data["goals"]:
            goals = user_data["goals"]
        if "records" in user_data and user_data["records"]:
            data["records"] = user_data["records"]

    return data, goals
