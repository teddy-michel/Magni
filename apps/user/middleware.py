import pytz
from django.utils import timezone

from Magni.settings import TIME_ZONE


class TimezoneUserMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if not request.user.is_anonymous:
            tzname = request.user.timezone

            try:
                timezone.activate(pytz.timezone(tzname))
            except pytz.exceptions.UnknownTimeZoneError:
                request.user.timezone = TIME_ZONE
                request.user.save()

        return self.get_response(request)
