from django.urls import path

from apps.user import views


urlpatterns = [
    path("settings", views.view_settings, name="settings"),
    path("notifications", views.view_notifications, name="notifications"),
    path("export", views.view_export, name="export_user"),
    path("export_all", views.view_export_all, name="export_all"),
]
