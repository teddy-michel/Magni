
def notifications(request):
    result = []
    notifications_count = 0

    if request.user.is_authenticated:
        for notification in request.user.notifications.all():
            result.append(notification)
            if not notification.viewed:
                notifications_count += 1

    return {"notifications": result, "notifications_count": notifications_count}
