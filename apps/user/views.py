import gzip
import json
import os
import shutil
import uuid
import zipfile
from io import BytesIO

from django.contrib.auth.decorators import login_required
from django.core.serializers.json import DjangoJSONEncoder
from django.http import JsonResponse, FileResponse
from django.shortcuts import redirect, render

from Magni.settings import BASE_DIR
from apps.activities.models import Activity, export_activity_json, ActivityFile
from apps.competitions.models import Competition
from apps.equipments.models import Equipment
from apps.user.forms import UserBasicForm, UserPhysicalForm, UserDisplayForm
from apps.user.models import export_user_json
from apps.weight.models import Weight


def view_login(request):
    pass  # TODO


def view_register(request):
    pass  # TODO


@login_required
def view_settings(request):
    if request.method == "POST":
        form1 = UserBasicForm(request.POST, instance=request.user)
        form2 = UserPhysicalForm(request.POST, instance=request.user)
        form3 = UserDisplayForm(request.POST, instance=request.user)
        if form1.is_valid() and form2.is_valid() and form3.is_valid():
            user = form1.save()
            form2 = UserPhysicalForm(request.POST, instance=user)
            if form2.is_valid():
                user = form2.save()
                form3 = UserDisplayForm(request.POST, instance=user)
                if form3.is_valid():
                    user = form3.save()
                    return redirect("settings")
        else:
            form2 = UserPhysicalForm(request.POST, instance=request.user)
    else:
        form1 = UserBasicForm(instance=request.user)
        form2 = UserPhysicalForm(instance=request.user)
        form3 = UserDisplayForm(instance=request.user)

    return render(request, "user/settings.html", {"form1": form1, "form2": form2, "form3": form3})


@login_required
def view_notifications(request):
    for notification in request.user.notifications.all():
        if not notification.viewed:
            notification.viewed = True
            notification.save()
    return render(request, "user/notifications.html")


@login_required
def view_export(request):
    response, _ = export_user_json(request.user)
    return JsonResponse(response)


@login_required
def view_export_all(request):
    user, goals = export_user_json(request.user)

    # Export weight
    weight = []
    for w in Weight.objects.filter(user=request.user):
        weight.append({
            "date": w.date,
            "weight": w.weight,
        })

    # Export equipments
    equipments = {}
    for s in Equipment.objects.filter(user=request.user):
        equipments[s.id] = {
            "id": s.id,
            "name": s.name,
            "brand": s.brand,
            "model": s.model,
            "notes": s.notes,
            "date": s.date,
            "old": s.old,
            "distance": s.distance,
        }

    activities = {}
    for activity in Activity.objects.filter(user=request.user).select_related("competition"):
        activities[activity.id] = export_activity_json(activity)
        activities[activity.id]["id"] = activity.id
        activities[activity.id]["equipments"] = []

        if activity.competition:
            activities[activity.id]["competition_id"] = activity.competition.id

        for equipment in activity.equipments.all():
            activities[activity.id]["equipments"].append(equipment.id) # TODO: tester

    competitions = {}
    for competition in Competition.objects.filter(user=request.user):
        competitions[competition.id] = {
            "id": competition.id,
            "date": competition.date,
            "name": competition.name,
            "type": competition.type,
            "description": competition.description,
            "duration": competition.duration,
            "duration_real": competition.duration_real,
            "place": competition.place,
            "participants": competition.participants,
            "place_category": competition.place_category,
            "participants_category": competition.participants_category,
        }

    files = []
    for file in ActivityFile.objects.filter(activity__user=request.user):
        if file.filehash not in files:
            files.append(file.filehash)

    zipname = os.path.join(BASE_DIR, "data", "tmp", str(uuid.uuid4()) + ".zip")
    with zipfile.ZipFile(zipname, "w", zipfile.ZIP_DEFLATED, compresslevel=9) as zipf:
        zipf.writestr("user.json", json.dumps(user, cls=DjangoJSONEncoder))
        zipf.writestr("goals.json", json.dumps(goals, cls=DjangoJSONEncoder))
        zipf.writestr("weight.json", json.dumps(weight, cls=DjangoJSONEncoder))

        for aid, activity in activities.items():
            zipf.writestr("activities/%06d.json" % aid, json.dumps(activity, cls=DjangoJSONEncoder))

        for cid, competition in competitions.items():
            zipf.writestr("competitions/%06d.json" % cid, json.dumps(competition, cls=DjangoJSONEncoder))

        for eid, equipment in equipments.items():
            zipf.writestr("equipments/%06d.json" % eid, json.dumps(equipment, cls=DjangoJSONEncoder))

        for filehash in files:
            filename = os.path.join(BASE_DIR, "data", "files", filehash + ".gz")
            if os.path.isfile(filename):
                with gzip.open(filename, "rb") as f:
                    zipf.writestr("files/%s" % filehash, f.read())

    buff = BytesIO()
    with open(zipname, "rb") as f:
        shutil.copyfileobj(f, buff)
    buff.seek(0)
    os.remove(zipname)

    return FileResponse(buff, as_attachment=True, filename="data.zip")
