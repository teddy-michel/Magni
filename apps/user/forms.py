from django.forms import IntegerField, TextInput, CharField
from django.forms import ModelForm
from django.utils.translation import gettext as _

from apps.base.forms import HTML5DateInput
from apps.user.models import User


class UserCreateForm(ModelForm):
    class Meta:
        model = User
        fields = ["email", "last_name", "first_name", "gender", "birthdate"]
        widgets = {
            "birthdate": HTML5DateInput(format="%Y-%m-%d"),
        }


class UserBasicForm(ModelForm):
    email_display = CharField(label=_("Email address"))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        instance = getattr(self, "instance", None)
        if instance:
            self.initial["email_display"] = instance.email
        self.fields["email_display"].widget.attrs["readonly"] = True

    class Meta:
        model = User
        fields = ["email_display", "last_name", "first_name", "gender", "birthdate"]
        widgets = {
            "birthdate": HTML5DateInput(format="%Y-%m-%d"),
        }


class UserPhysicalForm(ModelForm):
    def clean(self):
        cleaned_data = super().clean()
        heartrate_rest = cleaned_data.get("heartrate_rest")
        heartrate_max = cleaned_data.get("heartrate_max")

        if heartrate_rest >= heartrate_max:
            self.add_error("heartrate_rest", _("Resting heart rate must be lower than maximum heart rate."))
            self.add_error("heartrate_max", _("Maximum heart rate must be greater than resting heart rate."))

    heartrate_rest = IntegerField(label=_("Resting heart rate"), min_value=30, max_value=100)
    heartrate_max = IntegerField(label=_("Maximum heart rate"), min_value=120, max_value=220)
    threshold = IntegerField(label=_("Threshold"), min_value=150, max_value=900)
    ftp = IntegerField(label=_("FTP"), min_value=0, max_value=600)

    class Meta:
        model = User
        fields = ["heartrate_rest", "heartrate_max", "threshold", "ftp"]
        widgets = {
            "threshold": TextInput(attrs={"placeholder": "MM:SS"}),
        }


class UserDisplayForm(ModelForm):
    class Meta:
        model = User
        fields = ["timezone", "use_pace", "force_timezone_import"]
