
import datetime

from django import template
from django.forms import BooleanField
from django.utils.safestring import mark_safe
from django.utils.translation import gettext as _

from apps.base.globals import sports
from apps.equipments.globals import equipments

register = template.Library()


@register.filter
def duration(value):
    if value is None:
        return ""
    if not isinstance(value, float) and not isinstance(value, int):
        return value
    value /= 1000

    s, ms = divmod(value, 1)

    s = int(s)
    if ms > 0.0:
        s += 1

    value_h = s // 3600
    value_m = (s % 3600) // 60
    value_s = (s % 60)

    return mark_safe(_("%(hours)02d:%(minutes)02d:%(seconds)02d") % {"hours": value_h, "minutes": value_m, "seconds": value_s})


@register.filter
def distance(value):
    if value is None:
        return ""
    if not isinstance(value, float) and not isinstance(value, int):
        return value
    value = int(value)

    distance_km = value // 1000
    distance_m =value % 1000

    if distance_km == 0:
        return mark_safe(_("%(m)d&nbsp;m") % {"m": distance_m})
    elif distance_km > 100:
        return mark_safe(_("%(km)d.%(m)01d&nbsp;km") % {"km": distance_km, "m": distance_m // 100})
    elif distance_km > 10:
        return mark_safe(_("%(km)d.%(m)02d&nbsp;km") % {"km": distance_km, "m": distance_m // 10})
    else:
        return mark_safe(_("%(km)d.%(m)03d&nbsp;km") % {"km": distance_km, "m": distance_m})


@register.filter
def distance_record(value):
    if value is None:
        value = 0
    if not isinstance(value, float) and not isinstance(value, int):
        return value
    value = int(value)

    if value >= 1000:
        return mark_safe(_("%(km)g&nbsp;km") % {"km": value / 1000})
    else:
        return mark_safe(_("%(m)d&nbsp;m") % {"m": value})


@register.filter
def pace(value):
    if value is None:
        value = 0
    if not isinstance(value, float) and not isinstance(value, int):
        return value

    if value > 0:
        value = 1000.0 / value
        s, ms = divmod(value, 1)

        s = int(s)
        if ms > 0.0:
            s += 1

        value_m = s // 60
        value_s = s % 60
    else:
        value_m = 0
        value_s = 0

    return mark_safe(_("%(minutes)02d:%(seconds)02d&nbsp;min/km") % {"minutes": value_m, "seconds": value_s})


@register.filter
def speed(value):
    if value is None:
        value = 0
    if not isinstance(value, float) and not isinstance(value, int):
        return value

    value *= 3.6  # m/s => km/h
    return mark_safe(_("%.1f&nbsp;km/h") % value)


def _date_to_string(date, format):
    if date is None or not isinstance(date, datetime.datetime):
        return ""

    days_long = [None, _("Monday"), _("Tuesday"), _("Wednesday"), _("Thursday"), _("Friday"), _("Saturday"), _("Sunday")]
    months_long = [None, _("January"), _("February"), _("March"), _("April"), _("May"), _("June"), _("July"), _("August"), _("September"), _("October"), _("November"), _("December")]
    days_short = [None, _("Mon."), _("Tue."), _("Wed."), _("Thu."), _("Fri."), _("Sat."), _("Sun.")]
    months_short = [None, _("Jan."), _("Feb."), _("Mar."), _("Apr."), _("May"), _("Jun."), _("Jul."), _("Aug."), _("Sep."), _("Oct."), _("Nov."), _("Dec.")]

    s_day_name_long_maj = days_long[date.isoweekday()]
    s_month_name_long_maj = months_long[date.month]
    s_day_name_short_maj = days_short[date.isoweekday()]
    s_month_name_short_maj = months_short[date.month]

    return format % {
        "day_name_maj": _(s_day_name_long_maj),
        "day_name": _(s_day_name_long_maj).lower(),
        "day_name_short_maj": _(s_day_name_short_maj),
        "day_name_short": _(s_day_name_short_maj).lower(),
        "day_zero": f"{date:%d}",
        "day": str(date.day),
        "month_name_maj": _(s_month_name_long_maj),
        "month_name": _(s_month_name_long_maj).lower(),
        "month_name_short_maj": _(s_month_name_short_maj),
        "month_name_short": _(s_month_name_short_maj).lower(),
        "month_zero": f"{date:%m}",
        "month": str(date.month),
        "year": f"{date:%Y}",
        "hour_24": f"{date:%H}",
        "hour_12": f"{date:%I}",
        "minute": f"{date:%M}",
        "second": f"{date:%S}",
    }


@register.filter()
def date_short(value):
    return _date_to_string(value, _("%(day_name_short_maj)s %(day_zero)s/%(month_zero)s/%(year)s %(hour_24)s:%(minute)s"))


@register.filter()
def date_long(value):
    return _date_to_string(value, _("%(day_name_maj)s, %(month_name)s %(day)s, %(year)s at %(hour_24)s:%(minute)s"))


@register.filter()
def field_add_class(value, arg):
    if isinstance(value.field, BooleanField):
        return value
    else:
        oldClasses = value.field.widget.attrs.get("class", "")
        if oldClasses:
            oldClasses += " "
        return value.as_widget(attrs={"class": oldClasses + arg})


@register.filter()
def field_add_class_cb(value, arg):
    oldClasses = value.field.widget.attrs.get("class", "")
    if oldClasses:
        oldClasses += " "
    return value.as_widget(attrs={"class": oldClasses + arg})


@register.filter()
def is_checkbox(value):
    return isinstance(value.field, BooleanField)


@register.filter()
def activity_name(value):
    if value in sports:
        return sports[value]["name"]
    else:
        return sports["Other"]["name"]


@register.filter()
def activity_icon(value):
    if value in sports:
        return sports[value]["icon"]
    else:
        return sports["Other"]["icon"]


@register.filter()
def activity_bgcolor(value):
    if value in sports:
        return sports[value]["bgcolor"]
    else:
        return sports["Other"]["bgcolor"]


@register.filter()
def activity_textcolor(value):
    if value in sports:
        return sports[value]["textcolor"]
    else:
        return sports["Other"]["textcolor"]


@register.filter()
def equipment_name(value):
    if value in equipments:
        return equipments[value]["name"]
    else:
        return equipments["Other"]["name"]
