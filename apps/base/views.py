import calendar
import datetime
import json
import math

from django.db.models import Sum, Q
from django.shortcuts import render
from django.utils import timezone

from apps.activities.models import Activity
from apps.goals.goals import get_goal_description, update_goals
from apps.base.templatetags.utils import distance as distance_to_string
from apps.base.templatetags.utils import duration as duration_to_string


def view_index(request):
    stats = {
        "All": {
            "count": 0,
            "count_year": 0,
            "distance": 0,
            "distance_year": 0,
            "duration": 0,
            "duration_year": 0,
        },
        "Running": {
            "count": 0,
            "count_year": 0,
            "distance": 0,
            "distance_year": 0,
            "duration": 0,
            "duration_year": 0,
        },
        "Biking": {
            "count": 0,
            "count_year": 0,
            "distance": 0,
            "distance_year": 0,
            "duration": 0,
            "duration_year": 0,
        },
        "Swimming": {
            "count": 0,
            "count_year": 0,
            "distance": 0,
            "distance_year": 0,
            "duration": 0,
            "duration_year": 0,
        },
    }

    if request.user.is_authenticated:
        year = timezone.now().year

        stats["All"]["count"] = Activity.objects.filter(user=request.user).count()
        stats["All"]["count_year"] = Activity.objects.filter(user=request.user, date__year=year).count()

        try:
            stats["All"]["distance"] = int(Activity.objects.filter(user=request.user).aggregate(Sum("distance"))["distance__sum"])
        except:
            stats["All"]["distance"] = 0

        try:
            stats["All"]["distance_year"] = int(Activity.objects.filter(user=request.user, date__year=year).aggregate(Sum("distance"))["distance__sum"])
        except:
            stats["All"]["distance_year"] = 0

        try:
            stats["All"]["duration"] = int(Activity.objects.filter(user=request.user).aggregate(Sum("duration"))["duration__sum"])
        except:
            stats["All"]["duration"] = 0

        try:
            stats["All"]["duration_year"] = int(Activity.objects.filter(user=request.user, date__year=year).aggregate(Sum("duration"))["duration__sum"])
        except:
            stats["All"]["duration_year"] = 0

        stats["Running"]["count"] = Activity.objects.filter(user=request.user, type="Running").count()
        stats["Running"]["count_year"] = Activity.objects.filter(user=request.user, type="Running", date__year=year).count()

        try:
            stats["Running"]["distance"] = int(Activity.objects.filter(user=request.user, type="Running").aggregate(Sum("distance"))["distance__sum"])
        except:
            stats["Running"]["distance"] = 0

        try:
            stats["Running"]["distance_year"] = int(Activity.objects.filter(user=request.user, type="Running", date__year=year).aggregate(Sum("distance"))["distance__sum"])
        except:
            stats["Running"]["distance_year"] = 0

        try:
            stats["Running"]["duration"] = int(Activity.objects.filter(user=request.user, type="Running").aggregate(Sum("duration"))["duration__sum"])
        except:
            stats["Running"]["duration"] = 0

        try:
            stats["Running"]["duration_year"] = int(Activity.objects.filter(user=request.user, type="Running", date__year=year).aggregate(Sum("duration"))["duration__sum"])
        except:
            stats["Running"]["duration_year"] = 0

        stats["Biking"]["count"] = Activity.objects.filter(Q(type='HomeTrainer') | Q(type='Biking'), user=request.user).count()
        stats["Biking"]["count_year"] = Activity.objects.filter(Q(type='HomeTrainer') | Q(type='Biking'), user=request.user, date__year=year).count()

        try:
            stats["Biking"]["distance"] = int(Activity.objects.filter(Q(type='HomeTrainer') | Q(type='Biking'), user=request.user).aggregate(Sum("distance"))["distance__sum"])
        except:
            stats["Biking"]["distance"] = 0

        try:
            stats["Biking"]["distance_year"] = int(Activity.objects.filter(Q(type='HomeTrainer') | Q(type='Biking'), user=request.user, date__year=year).aggregate(Sum("distance"))["distance__sum"])
        except:
            stats["Biking"]["distance_year"] = 0

        try:
            stats["Biking"]["duration"] = int(Activity.objects.filter(Q(type='HomeTrainer') | Q(type='Biking'), user=request.user).aggregate(Sum("duration"))["duration__sum"])
        except:
            stats["Biking"]["duration"] = 0

        try:
            stats["Biking"]["duration_year"] = int(Activity.objects.filter(Q(type='HomeTrainer') | Q(type='Biking'), user=request.user, date__year=year).aggregate(Sum("duration"))["duration__sum"])
        except:
            stats["Biking"]["duration_year"] = 0

        stats["Swimming"]["count"] = Activity.objects.filter(user=request.user, type="Swimming").count()
        stats["Swimming"]["count_year"] = Activity.objects.filter(user=request.user, type="Swimming", date__year=year).count()

        try:
            stats["Swimming"]["distance"] = int(Activity.objects.filter(user=request.user, type="Swimming").aggregate(Sum("distance"))["distance__sum"])
        except:
            stats["Swimming"]["distance"] = 0

        try:
            stats["Swimming"]["distance_year"] = int(Activity.objects.filter(user=request.user, type="Swimming", date__year=year).aggregate(Sum("distance"))["distance__sum"])
        except:
            stats["Swimming"]["distance_year"] = 0

        try:
            stats["Swimming"]["duration"] = int(Activity.objects.filter(user=request.user, type="Swimming").aggregate(Sum("duration"))["duration__sum"])
        except:
            stats["Swimming"]["duration"] = 0

        try:
            stats["Swimming"]["duration_year"] = int(Activity.objects.filter(user=request.user, type="Swimming", date__year=year).aggregate(Sum("duration"))["duration__sum"])
        except:
            stats["Swimming"]["duration_year"] = 0

        activities = Activity.objects.filter(user=request.user)[0:10]

        try:
            user_data = json.loads(request.user.data)
            goals_all = user_data["goals"]

            if update_goals(goals_all):
                user_data["goals"] = goals_all
                request.user.data = json.dumps(user_data)
                request.user.save()

            now = timezone.now().timetuple()
            progress_day = (now.tm_hour * 3600 + now.tm_min * 60 + now.tm_sec) / 86400.0
            week_progress = 100 * (now.tm_wday + progress_day) / 7
            month_progress = 100 * (now.tm_mday + progress_day) / calendar.monthrange(now.tm_year, now.tm_mon)[1]
            year_progress = 100 * (now.tm_yday + progress_day) / (366 if calendar.isleap(now.tm_year) else 365)

            # Génère la description de l'objectif
            goals = []
            for goal in goals_all:
                if goal["accomplished"] is True:
                    continue

                goal["description"] = get_goal_description(goal)

                if goal["progression"]["date"] is not None:
                    goal["progression"]["date"] = datetime.datetime.fromisoformat(goal["progression"]["date"])

                goal["progression"]["percentage_wanted"] = 100
                goal["progression"]["percentage_todo"] = 0
                if goal["type"] == "week":
                    goal["progression"]["percentage_wanted"] = week_progress
                    if goal["progression"]["percentage_wanted"] > goal["progression"]["percentage"]:
                        goal["progression"]["percentage_todo"] = goal["progression"]["percentage_wanted"] - goal["progression"]["percentage"]
                elif goal["type"] == "month":
                    goal["progression"]["percentage_wanted"] = month_progress
                    if goal["progression"]["percentage_wanted"] > goal["progression"]["percentage"]:
                        goal["progression"]["percentage_todo"] = goal["progression"]["percentage_wanted"] - goal["progression"]["percentage"]
                elif goal["type"] == "year":
                    goal["progression"]["percentage_wanted"] = year_progress
                    if goal["progression"]["percentage_wanted"] > goal["progression"]["percentage"]:
                        goal["progression"]["percentage_todo"] = goal["progression"]["percentage_wanted"] - goal["progression"]["percentage"]

                if goal["type"] in ["week", "month", "year"]:
                    goal["progression"]["count_wanted"] = goal["progression"]["total"] * goal["progression"]["percentage_wanted"] / 100
                    if goal["criteria"] == "duration":
                        goal["progression"]["count_wanted_str"] = duration_to_string(goal["progression"]["count_wanted"])
                    elif goal["criteria"] == "distance":
                        goal["progression"]["count_wanted_str"] = distance_to_string(goal["progression"]["count_wanted"])
                    elif goal["criteria"] == "count":
                        goal["progression"]["count_wanted_str"] = math.ceil(goal["progression"]["count_wanted"])
                    # other values for criteria = "trimp", "record-distance", "record-duration"

                # TODO: formater count et total (durée, distance, etc.)

                goals.append(goal)
        except:
            goals = []

        """
        # ----------------------------------------------------------------------------
        # Dashboard sur la préparation marathon
        marathon_dates = [
            (datetime.date(2021, 10, 15), datetime.date(2022, 4, 16)),
            (datetime.date(2022, 4, 22), datetime.date(2022, 10, 22)),
            (datetime.date(2024, 4, 26), datetime.date(2024, 10, 26)),
        ]

        marathon = {
            "Dates":                       ["" for i in range(len(marathon_dates))],
            "Nombre de jours":             ["" for i in range(len(marathon_dates))],
            "Séances":                     ["" for i in range(len(marathon_dates))],
            "...Séances (course à pied)":  ["" for i in range(len(marathon_dates))],
            "...Séances (vélo)":           ["" for i in range(len(marathon_dates))],
            "...Séances (home-trainer)":   ["" for i in range(len(marathon_dates))],
            "...Séances (natation)":       ["" for i in range(len(marathon_dates))],
            "Durée":                       ["" for i in range(len(marathon_dates))],
            "...Durée (course à pied)":    ["" for i in range(len(marathon_dates))],
            "Distance":                    ["" for i in range(len(marathon_dates))],
            "...Distance (course à pied)": ["" for i in range(len(marathon_dates))],
            "...Distance (vélo)":          ["" for i in range(len(marathon_dates))],
            "Sorties longues":             ["" for i in range(len(marathon_dates))],
            "...(plus de 1h30)":           ["" for i in range(len(marathon_dates))],
            "...(plus de 2h00)":           ["" for i in range(len(marathon_dates))],
            "...(plus de 15km)":           ["" for i in range(len(marathon_dates))],
            "...(plus de 20km)":           ["" for i in range(len(marathon_dates))],
            "...(plus de 25km)":           ["" for i in range(len(marathon_dates))],
            "...(plus de 30km)":           ["" for i in range(len(marathon_dates))],
        }

        for i in range(len(marathon_dates)):
            marathon["Dates"][i] = marathon_dates[i][0].strftime("%d/%m/%Y") + " - " + marathon_dates[i][1].strftime("%d/%m/%Y")

            today = datetime.date.today()
            if today < marathon_dates[i][1]:
                marathon["Nombre de jours"][i] = "%d/%d jours" % ((today - marathon_dates[i][0]).days, (marathon_dates[i][1] - marathon_dates[i][0]).days)
            else:
                marathon["Nombre de jours"][i] = "%d jours" % (marathon_dates[i][1] - marathon_dates[i][0]).days

            query_set = Activity.objects.filter(user=request.user).filter(date__gte=marathon_dates[i][0], date__lte=marathon_dates[i][1])

            marathon["Séances"][i] = query_set.count()
            marathon["...Séances (course à pied)"][i] = query_set.filter(type="Running").count()
            marathon["...Séances (vélo)"][i] = query_set.filter(type="Biking").count()
            marathon["...Séances (home-trainer)"][i] = query_set.filter(type="HomeTrainer").count()
            marathon["...Séances (natation)"][i] = query_set.filter(type="Swimming").count()
            marathon["Durée"][i] = duration_to_string(int(query_set.aggregate(Sum('duration'))["duration__sum"]))
            marathon["...Durée (course à pied)"][i] = duration_to_string(int(query_set.filter(type="Running").aggregate(Sum('duration'))["duration__sum"]))
            marathon["...Distance (course à pied)"][i] = distance_to_string(int(query_set.filter(type="Running").aggregate(Sum('distance'))["distance__sum"]))
            marathon["...Distance (vélo)"][i] = distance_to_string(int(query_set.filter(type="Biking").aggregate(Sum('distance'))["distance__sum"]))
            marathon["Sorties longues"][i] = query_set.filter(type="Running").filter(duration__gte=60 * 60 * 1000).count()
            marathon["...(plus de 1h30)"][i] = query_set.filter(type="Running").filter(duration__gte=90 * 60 * 1000).count()
            marathon["...(plus de 2h00)"][i] = query_set.filter(type="Running").filter(duration__gte=120 * 60 * 1000).count()
            marathon["...(plus de 15km)"][i] = query_set.filter(type="Running").filter(distance__gte=15 * 1000).count()
            marathon["...(plus de 20km)"][i] = query_set.filter(type="Running").filter(distance__gte=20 * 1000).count()
            marathon["...(plus de 25km)"][i] = query_set.filter(type="Running").filter(distance__gte=25 * 1000).count()
            marathon["...(plus de 30km)"][i] = query_set.filter(type="Running").filter(distance__gte=30 * 1000).count()

        # ----------------------------------------------------------------------------
        """

        return render(request, "base/home.html", {
            "activities": activities,
            "goals": goals,
            "stats": stats,
            #"marathon": marathon,
        })
    else:
        return render(request, "base/index.html")


def view_privacy(request):
    return render(request, "base/privacy.html")
