import re

from django.forms import TimeInput, DateInput


class HTML5DateInput(DateInput):
    input_type = "date"


class HTML5TimeInput(TimeInput):
    input_type = "time"
    # Ce champ est encore mal géré dans Firefox (problèmes avec la validation si les secondes sont différentes de la valeur initiale).


def convert_duration_string(string):
    """
    Transforme une chaine de caractère en durée en millisecondes.

    Formats acceptés (exemple pour 1 heure 4 minutes 9 secondes et 88 millisecondes) :
    - "3849" ou "3849s"
    - "64min"
    - "64min4" ou "64min04" ou "64min4s"
    - "1h4" ou "1h04"
    - "1h4min" ou "1h04min"
    - "1h04min04" ou "1h4min04" ou "1h04min4s"
    - "3849088ms"
    - "1:4:9" ou "01:04:09"
    - "64:4" ou "64:04"
    """

    duration = string.replace(" ", "")
    if duration == "":
        return 0

    m = re.match("^([0-9]{1,4}):([0-9]{1,2}):([0-9]{1,2})$", duration)
    if m:
        return (int(m.group(1)) * 3600 + int(m.group(2)) * 60 + int(m.group(3))) * 1000

    m = re.match("^([0-9]{1,3}):([0-9]{1,2})$", duration)
    if m:
        return (int(m.group(1)) * 60 + int(m.group(2))) * 1000

    m = re.match("^([0-9]+)min([0-9]{0,2})s?$", duration)
    if m:
        d = int(m.group(1)) * 60
        if m.group(2):
            d += int(m.group(2))
        return d * 1000

    m = re.match("^([0-9]+)h?$", duration)
    if m:
        return int(m.group(1)) * 3600 * 1000

    m = re.match("^([0-9]+)h([0-9]{1,2})(min([0-9]{0,2})s?)?$", duration)
    if m:
        d = int(m.group(1)) * 3600 + int(m.group(2)) * 60
        if m.group(4):
            d += int(m.group(4))
        return d * 1000

    m = re.match("^([0-9]+)s?$", duration)
    if m:
        return int(m.group(1)) * 1000

    m = re.match("^([0-9]+)ms$", duration)
    if m:
        return int(m.group(1))

    return None
