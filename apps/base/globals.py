
from django.utils.translation import gettext as _


# TODO: laisser le choix à chaque utilisateur des records à afficher ?
record_distances_running = [500, 1000, 1500, 2000, 3000, 5000, 10000, 15000, 20000, 21097, 25000, 30000, 40000, 42195, 50000, 100000]
record_distances_biking = [1000, 2000, 5000, 10000, 15000, 20000, 30000, 40000, 50000, 100000, 150000, 200000, 250000, 300000]
record_distances_swimming = [50, 100, 200, 400, 500, 800, 1000, 1500, 2000, 2500, 3000, 4000, 5000]
record_durations = [5*60*1000, 10*60*1000, 30*60*1000, 60*60*1000, 120*60*1000, 12*60*60*1000, 24*60*60*1000]
record_power_durations = [1*1000, 2*1000, 5*1000, 10*1000, 20*1000, 30*1000, 60*1000, 2*60*1000, 5*60*1000, 10*60*1000, 20*60*1000, 30*60*1000, 60*60*1000, 120*60*1000]

# TODO: utiliser un modèle pour pouvoir ajouter facilement des types d'activité ?
sports = {
    "Other": {
        "name": _("Other activity"),
        "icon": "quidditch",
        "bgcolor": "#3788d8",
        "textcolor": "black",
    },
    "Running": {
        "name": _("Running"),
        "icon": "running",
        "bgcolor": "#67AE84",
        "textcolor": "black",
        #"variantes": ["Course sur route", "Course sur piste", "Course sur tapis", "Trail"],
    },
    "Biking": {
        "name": _("Biking"),
        "icon": "biking",
        "bgcolor": "#CC7979",
        "textcolor": "black",
        #"variantes": ["Route", "VTT", "Vélo d'appartement", "Home-trainer"],
    },
    "HomeTrainer": {
        "name": _("Home-trainer"),
        "icon": "biking",
        "bgcolor": "#C46363",
        "textcolor": "black",
    },
    "Swimming": {
        "name": _("Swimming"),
        "icon": "swimmer",
        "bgcolor": "#86B0D2",
        "textcolor": "black",
        #"variantes": ["Piscine", "Eau douce", "Eaux vives", "Mer"],
    },
    "Walking": {
        "name": _("Walking"),
        "icon": "walking",
        "bgcolor": "#589571",
        "textcolor": "black",
        #"variantes": ["Randonnée", "Marche athlétique", "Marche nordique"],
    },
}
