import datetime
import json

from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render

from apps.goals.forms import GoalForm, GoalValidateForm
from apps.goals.goals import get_goal_description, update_goal, sort_goals, update_goals
from apps.activities.models import Activity
from apps.base.templatetags.utils import distance as distance_to_string
from apps.base.templatetags.utils import duration as duration_to_string


@login_required
def view_goals(request):
    try:
        user_data = json.loads(request.user.data)
        goals = user_data["goals"]
    except:
        user_data = {"goals": [], "records": []}
        goals = []

    if update_goals(goals):
        user_data["goals"] = goals
        request.user.data = json.dumps(user_data)
        request.user.save()

    # Génère la description de l'objectif
    for goal in goals:
        goal["description"] = get_goal_description(goal)
        if goal["progression"]["date"] is not None:
            goal["progression"]["date"] = datetime.datetime.fromisoformat(goal["progression"]["date"])

    return render(request, "goals/goals.html", {"goals": goals})


@login_required
def view_goal_create(request):
    if request.method == "POST":
        form = GoalForm(request.POST)

        if form.is_valid():
            goal = {
                "type": form.cleaned_data["type"],
                "sport": "All",
                "criteria": "other",
                "accomplished": False,
                "progression": {
                    "count": 0,
                    "total": 0,
                    "percentage": 0.0,
                    "date": None,
                },
            }

            if form.cleaned_data["description"]:
                goal["description"] = form.cleaned_data["description"]

            if goal["type"] != "other":
                goal["sport"] = form.cleaned_data["sport"]
                goal["criteria"] = form.cleaned_data["criteria"]

            if goal["type"] != "activity":
                goal["repetition"] = form.cleaned_data["repetition"]

            if goal["criteria"] in ["duration", "record-duration", "record-distance"]:
                duration = form.cleaned_data["duration"]
                goal["duration"] = duration
                if goal["criteria"] == "duration":
                    goal["progression"]["total"] = duration
                    goal["progression"]["total_str"] = duration_to_string(duration)

            if goal["criteria"] in ["distance", "record-duration", "record-distance"]:
                distance = form.cleaned_data["distance"]
                goal["distance"] = distance
                if goal["criteria"] == "distance":
                    goal["progression"]["total"] = distance
                    goal["progression"]["total_str"] = distance_to_string(distance)

            if goal["criteria"] in ["record-duration", "record-distance"]:
                goal["progression"]["total"] = 100

            if goal["criteria"] == "count":
                count = form.cleaned_data["count"]
                goal["count"] = count
                goal["progression"]["total"] = count

            if goal["criteria"] == "trimp":
                trimp = form.cleaned_data["trimp"]
                goal["trimp"] = trimp
                goal["progression"]["total"] = trimp

            try:
                user_data = json.loads(request.user.data)
                goals = user_data["goals"]
            except:
                user_data = {"goals": [], "records": []}
                goals = []

            activities = Activity.objects.filter(user=request.user, planned=False).order_by("date")
            update_goal(goal, activities, request.user)
            goals.append(goal)

            user_data["goals"] = sort_goals(goals)
            request.user.data = json.dumps(user_data)
            request.user.save()

            return redirect("goals")
    else:
        form = GoalForm()

    return render(request, "goals/goal_create.html", {"form": form})


@login_required
def view_goal_edit(request, gid):
    try:
        user_data = json.loads(request.user.data)
    except:
        return redirect("goals")

    try:
        goal = user_data["goals"][gid]
    except:
        return redirect("goals")

    if request.method == "POST":
        form = GoalForm(request.POST)

        if form.is_valid():
            goal = {
                "type": form.cleaned_data["type"],
                "sport": "All",
                "criteria": "other",
                "accomplished": False,
                "progression": {
                    "count": 0,
                    "total": 0,
                    "percentage": 0.0,
                    "date": None,
                },
            }

            if form.cleaned_data["description"]:
                goal["description"] = form.cleaned_data["description"]

            if goal["type"] != "other":
                goal["sport"] = form.cleaned_data["sport"]
                goal["criteria"] = form.cleaned_data["criteria"]

            if goal["type"] != "activity":
                goal["repetition"] = form.cleaned_data["repetition"]

            if goal["criteria"] in ["duration", "record-duration", "record-distance"]:
                duration = form.cleaned_data["duration"]
                goal["duration"] = duration
                if goal["criteria"] == "duration":
                    goal["progression"]["total"] = duration
                    goal["progression"]["total_str"] = duration_to_string(duration)

            if goal["criteria"] in ["distance", "record-duration", "record-distance"]:
                distance = form.cleaned_data["distance"]
                goal["distance"] = distance
                if goal["criteria"] == "distance":
                    goal["progression"]["total"] = distance
                    goal["progression"]["total_str"] = distance_to_string(distance)

            if goal["criteria"] in ["record-duration", "record-distance"]:
                goal["progression"]["total"] = 100

            if goal["criteria"] == "count":
                count = form.cleaned_data["count"]
                goal["count"] = count
                goal["progression"]["total"] = count

            if goal["criteria"] == "trimp":
                trimp = form.cleaned_data["trimp"]
                goal["trimp"] = trimp
                goal["progression"]["total"] = trimp

            activities = Activity.objects.filter(user=request.user, planned=False).order_by("date")
            update_goal(goal, activities, request.user)
            user_data["goals"][gid] = goal

            user_data["goals"] = sort_goals(user_data["goals"])
            request.user.data = json.dumps(user_data)
            request.user.save()

            return redirect("goals")
    else:
        form = GoalForm(initial=goal)

    return render(request, "goals/goal_edit.html", {"form": form})


@login_required
def view_goal_remove(request, gid):
    try:
        user_data = json.loads(request.user.data)
    except:
        return redirect("goals")

    try:
        goal = user_data["goals"][gid]
        goal["description"] = get_goal_description(goal)
    except:
        return redirect("goals")

    if request.method == "POST" and "delete" in request.POST:
        del user_data["goals"][gid]
        request.user.data = json.dumps(user_data)
        request.user.save()

        return redirect("goals")

    return render(request, "goals/goal_remove.html", {"goal": goal})


@login_required
def view_goal_validate(request, gid):
    try:
        user_data = json.loads(request.user.data)
    except:
        return redirect("goals")

    try:
        goal = user_data["goals"][gid]
        goal["description"] = get_goal_description(goal)
    except:
        return redirect("goals")

    if goal["type"] != "other" or goal["accomplished"] is True:
        return redirect("goals")

    if request.method == "POST":
        form = GoalValidateForm(request.POST)

        if form.is_valid():
            goal_date = form.cleaned_data["date"]
            goal_time = form.cleaned_data["time"]
            goal_datetime = datetime.datetime.combine(goal_date, goal_time)

            goal["accomplished"] = True
            goal["progression"]["percentage"] = 100.0
            goal["progression"]["date"] = str(goal_datetime)

            user_data["goals"][gid] = goal
            user_data["goals"] = sort_goals(user_data["goals"])
            request.user.data = json.dumps(user_data)
            request.user.save()

            return redirect("goals")
    else:
        form = GoalValidateForm(initial=goal)

    return render(request, "goals/goal_validate.html", {"goal": goal, "form": form})
