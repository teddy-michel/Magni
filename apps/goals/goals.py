import datetime
import json

from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext as _

from apps.base.globals import sports
from apps.base.templatetags.utils import distance as distance_to_string
from apps.base.templatetags.utils import duration as duration_to_string
from apps.user.models import Notification


def get_goal_description(goal):
    if "description" in goal and goal["description"]:
        return goal["description"]

    description = ""

    if goal["type"] in ["week", "month", "year"]:
        period = ""
        if goal["repetition"]:
            if goal["type"] == "week":
                period = _("each week")
            elif goal["type"] == "month":
                period = _("each month")
            elif goal["type"] == "year":
                period = _("each year")
        else:
            if goal["type"] == "week":
                period = _("in one week")
            elif goal["type"] == "month":
                period = _("in one month")
            elif goal["type"] == "year":
                period = _("in one year")

        if goal["criteria"] == "count":
            if goal["sport"] == "All":
                description = _("Do more than %(count)d activities %(period)s") % {"count": goal["count"], "period": period}
            else:
                description = _("Do more than %(count)d %(sport)s sessions %(period)s") % {
                        "count": goal["count"],
                        "sport": sports[goal["sport"]]["name"],
                        "period": period}
        elif goal["criteria"] == "duration":
            if goal["sport"] == "All":
                description = _("Do more than %(duration)s %(period)s") % {
                        "duration": duration_to_string(goal["duration"]),
                        "period": period}
            else:
                description = _("Do more than %(duration)s in %(sport)s session %(period)s") % {
                        "duration": duration_to_string(goal["duration"]),
                        "sport": sports[goal["sport"]]["name"],
                        "period": period}
        elif goal["criteria"] == "distance":
            if goal["sport"] == "All":
                description = _("Do more than %(distance)s %(period)s") % {
                        "distance": distance_to_string(goal["distance"]),
                        "period": period}
            else:
                description = _("Do more than %(distance)s in %(sport)s sessions %(period)s") % {
                        "distance": distance_to_string(goal["distance"]),
                        "sport": sports[goal["sport"]]["name"],
                        "period": period}
        elif goal["criteria"] == "trimp":
            if goal["sport"] == "All":
                description = _("Do more than %(trimp)d TRIMP %(period)s") % {
                        "trimp": goal["trimp"],
                        "period": period}
            else:
                description = _("Do more than %(trimp)d TRIMP in %(sport)s session %(period)s") % {
                        "trimp": goal["trimp"],
                        "sport": sports[goal["sport"]]["name"],
                        "period": period}
    elif goal["type"] == "activity":
        if goal["criteria"] == "distance":
            if goal["sport"] == "All":
                description = _("Do more than %(distance)s in one activity") % {
                        "distance": distance_to_string(goal["distance"])}
            else:
                description = _("Do more than %(distance)s in one %(sport)s session") % {
                        "distance": distance_to_string(goal["distance"]),
                        "sport": sports[goal["sport"]]["name"]}
        elif goal["criteria"] == "duration":
            if goal["sport"] == "All":
                description = _("Do more than %(duration)s in one activity") % {
                        "duration": duration_to_string(goal["duration"])}
            else:
                description = _("Do more than %(duration)s in one %(sport)s session") % {
                        "duration": duration_to_string(goal["duration"]),
                        "sport": sports[goal["sport"]]["name"]}
        elif goal["criteria"] == "trimp":
            if goal["sport"] == "All":
                description = _("Do more than %(trimp)d TRIMP in one activity") % {"trimp": goal["trimp"]}
            else:
                description = _("Do more than %(trimp)d TRIMP in one %(sport)s session") % {
                        "trimp": goal["trimp"],
                        "sport": sports[goal["sport"]]["name"]}
        elif goal["criteria"] == "record-distance":
            if goal["sport"] == "All":
                description = _("Do less than %(duration)s for %(distance)s in one activity") % {
                        "duration": duration_to_string(goal["duration"]),
                        "distance": distance_to_string(goal["distance"])}
            else:
                description = _("Do less than %(duration)s for %(distance)s in one %(sport)s session") % {
                        "duration": duration_to_string(goal["duration"]),
                        "distance": distance_to_string(goal["distance"]),
                        "sport": sports[goal["sport"]]["name"]}
        elif goal["criteria"] == "record-duration":
            if goal["sport"] == "All":
                description = _("Do more than %(distance)s in %(duration)s in one activity") % {
                        "duration": duration_to_string(goal["duration"]),
                        "distance": distance_to_string(goal["distance"])}
            else:
                description = _("Do more than %(distance)s in %(duration)s in one %(sport)s session") % {
                        "duration": duration_to_string(goal["duration"]),
                        "distance": distance_to_string(goal["distance"]),
                        "sport": sports[goal["sport"]]["name"]}

    return description


def update_goal_add_activity(goal, activity, user):
    """
        Met-à-jour un objectif après la création d'une activité.
        Retourne True si l'objectif est accompli.
    """

    if goal["type"] == "other":
        return

    if is_activity_match_goal(goal, activity):
        if goal["type"] in ["week", "month", "year"]:
            if goal["criteria"] == "duration":
                if activity.duration is None:
                    return False
                goal["progression"]["count"] += activity.duration
            elif goal["criteria"] == "distance":
                if activity.distance is None:
                    return False
                goal["progression"]["count"] += activity.distance
            elif goal["criteria"] == "trimp":
                if activity.trimp is None:
                    return False
                goal["progression"]["count"] += activity.trimp
            elif goal["criteria"] == "count":
                goal["progression"]["count"] += 1
        elif goal["type"] == "activity":
            if goal["criteria"] == "duration":
                if activity.duration is None:
                    return False
                if activity.duration > goal["progression"]["count"]:
                    goal["progression"]["count"] = activity.duration
            elif goal["criteria"] == "distance":
                if activity.distance is None:
                    return False
                if activity.distance > goal["progression"]["count"]:
                    goal["progression"]["count"] = activity.distance
            elif goal["criteria"] == "trimp":
                if activity.trimp is None:
                    return False
                if activity.trimp > goal["progression"]["count"]:
                    goal["progression"]["count"] = activity.trimp
            elif goal["criteria"] == "record-duration":
                try:
                    activity_data = json.loads(activity.data)
                    for record in activity_data["records"]:
                        if record["type"] == "duration" and record["duration"] == goal["duration"]:
                            if record["distance"] >= goal["distance"]:
                                goal["progression"]["count"] = 100
                            else:
                                progress = int(100 * record["distance"] / goal["distance"])
                                if progress > goal["progression"]["count"]:
                                    goal["progression"]["count"] = progress
                except:
                    return False
            elif goal["criteria"] == "record-distance":
                try:
                    activity_data = json.loads(activity.data)
                    for record in activity_data["records"]:
                        if record["type"] == "distance" and record["distance"] == goal["distance"]:
                            if record["time"] <= goal["duration"]:
                                goal["progression"]["count"] = 100
                            else:
                                progress = int(100 * goal["duration"] / record["time"])
                                if progress > goal["progression"]["count"]:
                                    goal["progression"]["count"] = progress
                except:
                    return False

        # Formatage
        if goal["criteria"] == "duration":
            goal["progression"]["count_str"] = duration_to_string(goal["progression"]["count"])
        elif goal["criteria"] == "distance":
            goal["progression"]["count_str"] = distance_to_string(goal["progression"]["count"])

    if goal["progression"]["count"] >= goal["progression"]["total"]:
        goal["progression"]["percentage"] = 100.0
        if "repetition" not in goal or goal["repetition"] is False:
            goal["accomplished"] = True

            # Create notification for user
            Notification.objects.create(user=user,
                                        date=timezone.now(),
                                        text=_("Goal accomplished: %s") % get_goal_description(goal),
                                        url=reverse("goals"))
        return True
    elif goal["progression"]["total"] > 0:
        goal["progression"]["percentage"] = 100.0 * goal["progression"]["count"] / goal["progression"]["total"]

    return False


def update_goals_add_activity(goals, activity, user):
    """ Met-à-jour les objectifs après la création d'une activité. """

    for goal in goals:
        if goal["accomplished"]:
            continue

        if goal["type"] == "other":
            continue

        update_goal_add_activity(goal, activity, user)

        if goal["accomplished"] is True:
            goal["progression"]["date"] = str(activity.date)
        else:
            goal["progression"]["date"] = str(datetime.datetime.now())


def update_goals_edit_activity(goals, activities, user):
    """ Met-à-jour les objectifs après la modification d'une activité. """

    for goal in goals:
        if goal["accomplished"]:
            continue

        if goal["type"] == "other":
            continue

        goal["progression"]["count"] = 0
        goal["progression"]["percentage"] = 0.0

        for activity in activities:
            if update_goal_add_activity(goal, activity, user):
                if goal["accomplished"] is True:
                    goal["progression"]["date"] = str(activity.date)
                else:
                    goal["progression"]["date"] = str(datetime.datetime.now())
                break
        else:
            goal["progression"]["date"] = str(datetime.datetime.now())


def update_goal_remove_activity(goal, activity):
    """
        Met-à-jour un objectif après la suppression d'une activité.
    """

    if goal["type"] == "other":
        return

    if is_activity_match_goal(goal, activity):
        if goal["type"] in ["week", "month", "year"]:
            if goal["criteria"] == "duration":
                if activity.duration is None:
                    return
                goal["progression"]["count"] -= activity.duration
            elif goal["criteria"] == "distance":
                if activity.distance is None:
                    return
                goal["progression"]["count"] -= activity.distance
            elif goal["criteria"] == "trimp":
                if activity.trimp is None:
                    return
                goal["progression"]["count"] -= activity.trimp
            elif goal["criteria"] == "count":
                goal["progression"]["count"] -= 1

    # Formatage
    if goal["criteria"] == "duration":
        goal["progression"]["count_str"] = duration_to_string(goal["progression"]["count"])
    elif goal["criteria"] == "distance":
        goal["progression"]["count_str"] = distance_to_string(goal["progression"]["count"])

    if goal["progression"]["count"] >= goal["progression"]["total"]:
        goal["progression"]["percentage"] = 100.0
        if "repetition" not in goal or goal["repetition"] is False:
            goal["accomplished"] = True
    elif goal["progression"]["total"] > 0:
        goal["progression"]["percentage"] = 100.0 * goal["progression"]["count"] / goal["progression"]["total"]


def update_goals_remove_activity(goals, activity):
    """ Met-à-jour les objectifs après la suppression d'une activité. """

    for goal in goals:
        if goal["accomplished"]:
            continue

        if goal["type"] == "other":
            continue

        update_goal_remove_activity(goal, activity)
        goal["progression"]["date"] = str(datetime.datetime.now())


def update_goal(goal, activities, user):
    """ Met-à-jour un objectif. """

    for activity in activities:
        update_goal_add_activity(goal, activity, user)

        if goal["accomplished"] is True:
            goal["progression"]["date"] = str(activity.date)

            # Create notification for user
            Notification.objects.create(user=user,
                                        date=timezone.now(),
                                        text=_("Goal accomplished: %s") % get_goal_description(goal),
                                        url=reverse("goals"))
            return True

    goal["progression"]["date"] = str(datetime.datetime.now())
    return False


# TODO: utiliser une tâche cron
def update_goals(goals):
    """ Met-à-jour les objectifs si nécessaire (changement de semaine/mois/année). """

    now = datetime.datetime.now()
    updated = False

    for goal in goals:
        if goal["accomplished"] is True:
            continue

        if goal["type"] is "other":
            continue

        need_update = False

        if goal["progression"]["date"] is None:
            need_update = True
        else:
            date = datetime.datetime.fromisoformat(goal["progression"]["date"])
            if goal["type"] == "week":
                if date.year != now.year or date.isocalendar()[1] != now.isocalendar()[1]:
                    need_update = True
            elif goal["type"] == "month":
                if date.year != now.year or date.month != now.month:
                    need_update = True
            elif goal["type"] == "year":
                if date.year != now.year:
                    need_update = True

        if need_update:
            goal["progression"]["count"] = 0
            goal["progression"]["percentage"] = 0.0
            goal["progression"]["date"] = str(datetime.datetime.now())
            updated = True

    return updated


def is_activity_match_goal(goal, activity):
    """ Indique si un objectif est concerné par une activité. """

    if goal["type"] == "other":
        return False

    if goal["sport"] == "All" or goal["sport"] == activity.type:
        now = datetime.datetime.now()
        if goal["type"] == "week":
            if activity.date.year != now.year:
                return False
            if activity.date.isocalendar()[1] != now.isocalendar()[1]:
                return False
            return True
        elif goal["type"] == "month":
            if activity.date.year != now.year:
                return False
            if activity.date.month != now.month:
                return False
            return True
        elif goal["type"] == "year":
            if activity.date.year != now.year:
                return False
            return True
        elif goal["type"] == "activity":
            if goal["criteria"] == "duration":
                return activity.duration is not None and activity.duration > goal["progression"]["count"]
            elif goal["criteria"] == "distance":
                return activity.distance is not None and activity.distance > goal["progression"]["count"]
            elif goal["criteria"] == "trimp":
                return activity.trimp is not None and activity.trimp > goal["progression"]["count"]
            elif goal["criteria"] == "record-duration":
                return True
            elif goal["criteria"] == "record-distance":
                return True
    else:
        return False

    return False


def _get_goal_key(goal):
    key = ""

    if goal["accomplished"]:
        key += "1@"
    else:
        key += "0@"

    if goal["type"] == "week":
        key += "0@"
    elif goal["type"] == "month":
        key += "1@"
    elif goal["type"] == "year":
        key += "2@"
    elif goal["type"] == "activity":
        key += "3@"
    elif goal["type"] == "other":
        key += "4@"
    else:
        key += "5@"

    if goal["type"] == "other":
        key += goal["description"]
    else:
        key += goal["criteria"] + "@" + goal["sport"]

        if goal["criteria"] == "distance":
            key += "@%06d" % goal["distance"]
        elif goal["criteria"] == "duration":
            key += "@%06d" % goal["duration"]
        elif goal["criteria"] == "trimp":
            key += "@%06d" % goal["trimp"]
        elif goal["criteria"] == "count":
            key += "@%06d" % goal["count"]
        elif goal["criteria"] == "record-duration":
            key += "@%06d" % goal["duration"]
            key += "@%06d" % goal["distance"]
        elif goal["criteria"] == "record-distance":
            key += "@%06d" % goal["distance"]
            key += "@%06d" % goal["duration"]

    return key


def sort_goals(goals):
    goals = sorted(goals, key=_get_goal_key)
    return goals
