import re

from django.forms import Form
from django.forms import IntegerField, TextInput, BooleanField, CharField, ChoiceField, DateField, TimeField
from django.utils.translation import gettext as _, pgettext

from apps.base.globals import sports
from apps.base.forms import convert_duration_string
from apps.base.forms import HTML5DateInput


class GoalForm(Form):
    type = ChoiceField(label=_("Goal type"), choices=[("activity", _("Per activity")), ("week", _("Per week")), ("month", _("Per month")), ("year", _("Per year")), ("other", pgettext("goal", "Other")),])
    sport = ChoiceField(label=_("Activity type"), choices=[("All", _("All"))] + [(a, b["name"]) for a, b in sports.items()], required=False)
    criteria = ChoiceField(label=_("Criteria"), choices=[("distance", _("Distance")), ("duration", _("Duration")), ("trimp", _("TRIMP")), ("count", _("Activity count")), ("record-distance", _("Distance record")), ("record-duration", _("Duration record")),], required=False)
    distance = IntegerField(label=_("Distance"), min_value=0, required=False, initial=1000)
    duration = CharField(label=_("Duration"), max_length=20, required=False, widget=TextInput(attrs={"placeholder": "HH:MM:SS"}))
    count = IntegerField(label=_("Activity count"), min_value=1, required=False, initial=1)
    trimp = IntegerField(label=_("TRIMP"), min_value=0, required=False, initial=100)
    repetition = BooleanField(label=_("Repetition"), required=False)
    description = CharField(label=_("Description"), max_length=80, required=False, help_text=_("You can override the description generated by the application."))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        try:
            s, ms = divmod(self.initial["duration"], 1000)
            value_h = s // 3600
            value_m = (s % 3600) // 60
            value_s = (s % 60)
            self.initial["duration"] = _("%(hours)02d:%(minutes)02d:%(seconds)02d") % {"hours": value_h, "minutes": value_m, "seconds": value_s}
        except:
            pass

    def clean_duration(self):
        duration = convert_duration_string(self.cleaned_data["duration"])
        if duration is None:
            return 0
        else:
            return duration

    def clean(self):
        cleaned_data = super().clean()
        value_type = cleaned_data.get("type")
        value_criteria = cleaned_data.get("criteria")

        if value_type == "activity":
            if value_criteria == "count":
                self.add_error("criteria", _("Invalid criteria."))
        elif value_type == "other":
            if cleaned_data.get("description").strip() == "":
                self.add_error("description", _("Invalid description."))
        else:
            if value_criteria in ["record-duration", "record-distance"]:
                self.add_error("criteria", _("Invalid criteria."))

        if value_type != "other":
            if value_criteria in ["duration", "record-duration", "record-distance"]:
                value_duration = cleaned_data.get("duration")
                if value_duration is None or value_duration <= 0:
                    self.add_error("duration", _("Invalid duration."))

            if value_criteria in ["distance", "record-duration", "record-distance"]:
                value_distance = cleaned_data.get("distance")
                if value_distance is None or value_distance <= 0:
                    self.add_error("distance", _("Invalid distance."))

            if value_criteria == "count":
                value_count = cleaned_data.get("count")
                if value_count is None or value_count <= 0:
                    self.add_error("count", _("Invalid activity count."))
            elif value_criteria == "trimp":
                value_trimp = cleaned_data.get("trimp")
                if value_trimp is None or value_trimp <= 0:
                    self.add_error("trimp", _("Invalid TRIMP."))


class GoalValidateForm(Form):
    date = DateField(label=_("Date"), required=True, widget=HTML5DateInput(format="%Y-%m-%d"))
    time = TimeField(label=pgettext("datetime", "Time"), required=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.initial["date"] = ""  # TODO
        self.initial["time"] = ""  # TODO
