from django.urls import path

from apps.goals import views


urlpatterns = [
    path("", views.view_goals, name="goals"),
    path("add", views.view_goal_create, name="add_goal"),
    path("<id:gid>/edit", views.view_goal_edit, name="edit_goal"),
    #path("<int:gid>/edit", views.view_goal_edit_redirect),
    path("<id:gid>/remove", views.view_goal_remove, name="remove_goal"),
    #path("<int:gid>/remove", views.view_goal_remove_redirect),
    path("<id:gid>/validate", views.view_goal_validate, name="validate_goal"),
]
