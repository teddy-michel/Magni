from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class GoalsAppConfig(AppConfig):
    name = "apps.goals"
    verbose_name = _("Goals")
