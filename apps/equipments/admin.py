from django.contrib import admin

from apps.equipments.models import Equipment, EquipmentSet


@admin.register(Equipment)
class EquipmentAdmin(admin.ModelAdmin):
    list_display = ("type", "brand", "model", "name", "date", "user")


@admin.register(EquipmentSet)
class EquipmentSetAdmin(admin.ModelAdmin):
    list_display = ("name", "user")
