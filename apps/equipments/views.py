
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect

from apps.equipments.forms import EquipmentForm, EquipmentSetForm
from apps.equipments.models import Equipment, EquipmentSet
from apps.equipments.globals import equipments
from apps.activities.models import Activity
from django.db.models import Sum, Count


@login_required
def view_equipments(request):
    equipments = Equipment.objects.filter(user=request.user)
    equipmentsets = EquipmentSet.objects.filter(user=request.user)
    return render(request, "equipments/equipments.html", {"equipments": equipments, "equipmentsets": equipmentsets})


@login_required
def view_equipment_info(request, sid):
    try:
        equipment = Equipment.objects.get(id=sid, user=request.user)
    except Equipment.DoesNotExist:
        return redirect("equipments")

    # Attention: order_by est indispensable car sinon Django ajoute la date dans la clause "GROUP BY" !
    stats = equipment.activities.values("type").annotate(total_distance=Sum("distance"), total_duration=Sum("duration"), total=Count("id")).order_by("type")

    return render(request, "equipments/equipment_info.html", {"equipment": equipment, "stats": stats})


@login_required
def view_equipment_create(request):
    if request.method == "POST":
        form = EquipmentForm(request.POST, request.FILES)
        if form.is_valid():
            equipment = form.save(commit=False)
            equipment.user = request.user
            equipment.save()

            """
            # TODO: user default equipments
            if form.cleaned_data["is_default"]:
                request.user.default_equipment = equipment
                request.user.save()
            """

            return redirect("equipment_info", equipment.id)
    else:
        form = EquipmentForm()

    return render(request, "equipments/equipment_create.html", {"form": form})


@login_required
def view_equipment_edit(request, sid):
    try:
        equipment = Equipment.objects.get(id=sid, user=request.user)
    except Equipment.DoesNotExist:
        return redirect("equipments")

    if request.method == "POST":
        form = EquipmentForm(request.POST, request.FILES, instance=equipment)
        if form.is_valid():
            form.save()

            # TODO: remove unused image

            """
            # TODO: user default equipments
            if form.cleaned_data["is_default"]:
                if request.user.default_equipment != equipment:
                    request.user.default_equipment = equipment
                    request.user.save()
            elif request.user.default_equipment == equipment:
                request.user.default_equipment = None
                request.user.save()
            """

            return redirect("equipment_info", equipment.id)
    else:
        #form = EquipmentForm(initial={"is_default": request.user.default_equipment == equipment}, instance=equipment)
        form = EquipmentForm(initial={"is_default": False}, instance=equipment)  # TODO: user default equipments

    return render(request, "equipments/equipment_edit.html", {"equipment": equipment, "form": form})


@login_required
def view_equipment_edit_activities(request, sid):
    try:
        equipment = Equipment.objects.get(id=sid, user=request.user)
    except Equipment.DoesNotExist:
        return redirect("equipments")

    activities_selected = equipment.activities.values_list("id", flat=True)

    # On sélectionne toutes les activités de l'utilisateur
    activities = Activity.objects.filter(user=request.user)

    # On ne garde que les activités effectuées après l'achat de l'équipement
    if equipment.date:
        activities = activities.filter(date__gte=equipment.date)

    # On filtre le type d'activité en fonction du type d'équipement
    if equipments[equipment.type]["sports"]:
        activities = activities.filter(type__in=equipments[equipment.type]["sports"])

    if request.method == "POST":
        new_selection = request.POST.getlist("activity")
        new_selection = [int(v) for v in new_selection]

        if new_selection != list(activities_selected):
            equipment.activities.clear()
            equipment.activities.add(*new_selection)

        return redirect("equipment_info", equipment.id)

    return render(request, "equipments/equipment_edit_activities.html", {
        "equipment": equipment,
        "activities": activities,
        "activities_selected": activities_selected,
    })


@login_required
def view_equipment_remove(request, sid):
    try:
        equipment = Equipment.objects.get(id=sid, user=request.user)
    except Equipment.DoesNotExist:
        return redirect("equipments")

    if request.method == "POST" and "delete" in request.POST:
        equipment.delete()
        return redirect("equipments")

    return render(request, "equipments/equipment_remove.html", {"equipment": equipment})


@login_required
def view_equipment_set_create(request):
    if request.method == "POST":
        form = EquipmentSetForm(request.POST)
        if form.is_valid():
            equipmentset = form.save(commit=False)
            equipmentset.user = request.user
            equipmentset.save()
            form.save_m2m()

            return redirect("equipments")
    else:
        form = EquipmentSetForm()

    return render(request, "equipments/equipment_set_create.html", {"form": form})


@login_required
def view_equipment_set_edit(request, sid):
    try:
        equipmentset = EquipmentSet.objects.get(id=sid, user=request.user)
    except EquipmentSet.DoesNotExist:
        return redirect("equipments")

    if request.method == "POST":
        form = EquipmentSetForm(request.POST, instance=equipmentset)
        if form.is_valid():
            form.save()
            return redirect("equipments")
    else:
        form = EquipmentSetForm(instance=equipmentset)

    return render(request, "equipments/equipment_set_edit.html", {"equipmentset": equipmentset, "form": form})


@login_required
def view_equipment_set_remove(request, sid):
    try:
        equipmentset = EquipmentSet.objects.get(id=sid, user=request.user)
    except EquipmentSet.DoesNotExist:
        return redirect("equipments")

    if request.method == "POST" and "delete" in request.POST:
        equipmentset.delete()
        return redirect("equipments")

    return render(request, "equipments/equipment_set_remove.html", {"equipmentset": equipmentset})
