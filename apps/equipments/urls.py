from django.urls import path

from apps.equipments import views


urlpatterns = [
    path("", views.view_equipments, name="equipments"),

    path("add", views.view_equipment_create, name="add_equipment"),
    path("<id:sid>", views.view_equipment_info, name="equipment_info"),
    path("<id:sid>/edit", views.view_equipment_edit, name="equipment_edit"),
    #path("<int:sid>/edit", views.view_equipment_edit_redirect),
    path("<id:sid>/edit_activities", views.view_equipment_edit_activities, name="equipment_edit_activities"),
    path("<id:sid>/remove", views.view_equipment_remove, name="equipment_remove"),
    #path("<int:sid>/remove", views.view_equipment_remove_redirect),
    path("add_set", views.view_equipment_set_create, name="add_equipment_set"),
    path("set-<id:sid>/edit", views.view_equipment_set_edit, name="edit_equipment_set"),
    path("set-<id:sid>/remove", views.view_equipment_set_remove, name="remove_equipment_set"),
]
