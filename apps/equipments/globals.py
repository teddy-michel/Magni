
from django.utils.translation import gettext as _


equipments = {
    "Other": {
        "name": _("Other equipment"),
        "sports": [],
    },
    "Shoes": {
        "name": _("Shoes"),
        "sports": ["Other", "Running", "Biking", "HomeTrainer", "Walking"],
    },
    "Watch": {
        "name": _("Watch"),
        "sports": [],
    },
    "Bike": {
        "name": _("Bike"),
        "sports": ["Biking", "HomeTrainer"],
    },
    "Home-Trainer": {
        "name": _("Home-Trainer"),
        "sports": ["HomeTrainer"],
    },
    "Wheel": {
        "name": _("Wheel"),
        "sports": ["Biking", "HomeTrainer"],
    },
    "Cassette": {
        "name": _("Cassette"),
        "sports": ["Biking", "HomeTrainer"],
    },
    "Tire": {
        "name": _("Tire"),
        "sports": ["Biking", "HomeTrainer"],
    },
    "BicycleChain": {
        "name": _("Bicycle chain"),
        "sports": ["Biking", "HomeTrainer"],
    },
}
