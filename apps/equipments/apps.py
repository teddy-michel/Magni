from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class EquipmentsAppConfig(AppConfig):
    name = "apps.equipments"
    verbose_name = _("Equipments")
