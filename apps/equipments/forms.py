
from django.forms import ModelForm
from django.forms import Textarea, TextInput, BooleanField, FileInput, HiddenInput, CharField
from django.utils.translation import gettext as _

from apps.equipments.models import Equipment, EquipmentSet


class EquipmentForm(ModelForm):
    #is_default = BooleanField(label=_("Default equipment"), required=False)  # TODO
    image_clear = CharField(widget=HiddenInput(), max_length=1, required=False, initial="0")

    def save(self, commit=True):
        image_clear_value = self.cleaned_data.get("image_clear")
        if image_clear_value == "1":
            self.instance.image = None

        return super(ModelForm, self).save(commit=commit)

    class Meta:
        model = Equipment
        fields = ["type", "brand", "model", "name", "date", "notes", "old", "image", "image_clear"]
        widgets = {
            "date": TextInput(attrs={"placeholder": "YYYY-MM-DD"}),
            "notes": Textarea(attrs={"rows": 4}),
            "image": FileInput(),
        }


class EquipmentSetForm(ModelForm):
    class Meta:
        model = EquipmentSet
        fields = ["name", "type", "equipments"]
