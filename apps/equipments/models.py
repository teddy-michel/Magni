
import os

from django.db import models
from django.db.models import Sum
from django.utils.translation import gettext as _

from apps.base.globals import sports
from apps.equipments.globals import equipments
from apps.user.models import User

from Magni.storage import OverwriteStorage


def random_filename(instance, filename):
    ext = os.path.splitext(filename)[1].lower()
    if ext == ".jpeg":
        ext = ".jpg"
    filename = "equipment_%04d%s" % (instance.pk, ext)
    return filename


class Equipment(models.Model):
    type = models.CharField(_("equipment type"), max_length=16, choices=[(a, b["name"]) for a, b in equipments.items()], default="Other")
    brand = models.CharField(_("brand"), max_length=50)
    model = models.CharField(_("model"), blank=True, max_length=50)
    name = models.CharField(_("name"), blank=True, max_length=50)
    notes = models.TextField(_("notes"), blank=True, max_length=500)
    date = models.CharField(_("date"), max_length=10, blank=True)
    user = models.ForeignKey(User, verbose_name=_("user"), related_name="equipments", on_delete=models.CASCADE)
    old = models.BooleanField(_("old equipment"), default=False)
    image = models.ImageField(verbose_name=_("Image"), storage=OverwriteStorage(), upload_to=random_filename, null=True, blank=True)
    # TODO: date end
    #cost ???

    def save(self, *args, **kwargs):
        if self.pk is None:
            saved_image = self.image
            self.image = None
            super(Equipment, self).save(*args, **kwargs)
            self.image = saved_image
            if "force_insert" in kwargs:
                kwargs.pop("force_insert")

        super(Equipment, self).save(*args, **kwargs)

    def __str__(self):
        name = str(self.name)
        if name:
            return "[%s] %s" % (equipments[str(self.type)]["name"], name)
        else:
            name = "[%s] %s" % (equipments[str(self.type)]["name"], str(self.brand))
            model = str(self.model)
            if model:
                name += " " + model
            if len(str(self.date)) >= 4:
                return "%s (%s)" % (name, self.date[0:4])
            else:
                return name

    def nname(self):
        name = str(self.name)
        if name:
            return name
        else:
            name = str(self.brand)
            model = str(self.model)
            if model:
                name += " " + model
            if len(str(self.date)) >= 4:
                return "%s (%s)" % (name, self.date[0:4])
            else:
                return name

    @property
    def distance(self):
        d = self.activities.aggregate(Sum("distance"))["distance__sum"]
        if d is None:
            return 0
        else:
            return d

    @property
    def duration(self):
        d = self.activities.aggregate(Sum("duration"))["duration__sum"]
        if d is None:
            return 0
        else:
            return d

    class Meta:
        verbose_name = _("Equipment")
        verbose_name_plural = _("Equipments")
        ordering = ["type", "-date"]


class EquipmentSet(models.Model):
    name = models.CharField(_("name"), max_length=50, blank=False, null=False)
    user = models.ForeignKey(User, verbose_name=_("user"), related_name="equipmentsets", on_delete=models.CASCADE)
    equipments = models.ManyToManyField(Equipment, related_name="equipmentsets")
    type = models.CharField(_("activity type"), max_length=16, choices=[("All", _("All activities"))] + [(a, b["name"]) for a, b in sports.items()], default="All")

    class Meta:
        verbose_name = _("Equipment set")
        verbose_name_plural = _("Equipment sets")
        ordering = ["name"]
