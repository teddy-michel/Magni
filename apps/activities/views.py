
import datetime
import gzip
import hashlib
import json
import os
import shutil
import traceback
import uuid
from calendar import monthrange
from io import BytesIO
from dateutil.rrule import DAILY, rrule
from collections import OrderedDict

from django.contrib.auth.decorators import login_required
from django.http import JsonResponse, FileResponse, HttpResponseGone
from django.shortcuts import render, redirect
from django.urls import reverse
from django.utils import timezone
from django.utils.timezone import make_aware
from django.utils.translation import gettext as _
from django.db.models import Q

from Magni.settings import MAX_UPLOAD_COUNT, BASE_DIR, MAX_UPLOAD_FILESIZE
from apps.activities.fileformats import ImporterTCX, ImporterKMZ, ImporterKML, ImporterGPX, ImporterFIT
from apps.activities.forms import SegmentForm, SegmentEditForm, MultiSegmentForm, ActivityAdvancedForm, ActivityForm, ActivityCreateForm, NoteForm
from apps.activities.models import ActivityFile, Activity, export_activity_json, Note
from apps.activities.score import update_user_records, compute_level, compute_ngp, get_heartrate_stats, compute_trimp, \
    compute_best_performances, compute_fitness, get_power_stats
from apps.activities.trackfile import TrackFile
from apps.activities.records import compute_previsions
from apps.activities.datasource import DataSource, DataSourceCount, DataSourceTotalDuration, DataSourceAverageDuration, DataSourceMovingAverage
from apps.base.globals import sports, record_durations, record_distances_running, record_distances_biking, \
    record_power_durations, record_distances_swimming
from apps.base.templatetags.utils import date_short
from apps.base.templatetags.utils import distance as distance_to_string
from apps.base.templatetags.utils import duration as duration_to_string
from apps.base.forms import convert_duration_string
from apps.competitions.models import Competition
from apps.goals.goals import update_goals_add_activity, update_goals_edit_activity, update_goals_remove_activity
from apps.equipments.globals import equipments as equipments_globals
from apps.equipments.models import Equipment, EquipmentSet
from apps.user.models import Notification


@login_required
def view_activities(request):
    #activities = Activity.objects.filter(user=request.user)
    return render(request, "activities/activities.html", {"sports": sports})


@login_required
def view_activity_create(request):
    if request.method == "POST":
        form1 = ActivityCreateForm(request.POST)
        form2 = ActivityAdvancedForm(request.POST)

        if form1.is_valid() and form2.is_valid():
            activity = form1.save(commit=False)
            activity.user = request.user
            activity.data = json.dumps({})
            activity.save()

            form2 = ActivityAdvancedForm(request.POST, instance=activity)
            if form2.is_valid():
                activity = form2.save()

            # Update goals
            try:
                user_data = json.loads(request.user.data)
                update_goals_add_activity(user_data["goals"], activity, request.user)
                request.user.data = json.dumps(user_data)
                request.user.save()
            except:
                pass

            return redirect("activity", activity.id)
    else:
        time_shift_delta = timezone.localtime(timezone.now()).utcoffset()
        time_shift = time_shift_delta.days * 24 * 60 + time_shift_delta.seconds // 60
        form1 = ActivityCreateForm(initial={"time_shift": time_shift})
        form2 = ActivityAdvancedForm()

    return render(request, "activities/activity_create.html", {"form1": form1, "form2": form2})


def _import_file(file, user, time_shift=None):
    activities = []
    errors = []

    filename = file.name
    filesize = file.size

    os.makedirs(os.path.join(BASE_DIR, "data", "tmp"), exist_ok=True)
    os.makedirs(os.path.join(BASE_DIR, "data", "files"), exist_ok=True)

    if filesize > MAX_UPLOAD_FILESIZE:
        errors.append(_("File \"%s\" is too big.") % filename)
    else:
        filename_tmp = os.path.join(BASE_DIR, "data", "tmp", str(uuid.uuid4()) + ".gz")
        sha1 = hashlib.sha1()
        with gzip.open(filename_tmp, "wb") as dest:
            for chunk in file.chunks():
                dest.write(chunk)
                sha1.update(chunk)
        filehash = sha1.hexdigest()
        filename_dst = os.path.join(BASE_DIR, "data", "files", filehash + ".gz")

        if os.path.isfile(filename_dst):
            os.remove(filename_tmp)
        else:
            shutil.move(filename_tmp, filename_dst)

        compressed_size = os.path.getsize(filename_dst)

        importers = [ImporterTCX, ImporterFIT, ImporterGPX, ImporterKML, ImporterKMZ]
        for importer in importers:
            try:
                with gzip.open(filename_dst, "rb") as f:
                    imp = importer(f, filename_dst)
                    if imp.is_valid():
                        activities, errors = imp.load_activities()
                        break
            except:
                traceback.print_exc()
                pass
        else:
            errors = [_("Can't import file.")]

        try:
            user_data = json.loads(user.data)
            goals = user_data["goals"]
        except:
            user_data = {"goals": [], "records": []}
            goals = []

        for activity in activities:
            activity.user = user

            # Check if Biking is in fact HomeTrainer
            if activity.type == "Biking":
                device = str(activity.device)
                if device.startswith("zwift") or device.startswith("Tacx App"):
                    activity.type = "HomeTrainer"

            if time_shift is not None:
                activity.time_shift = time_shift

            activity.trackfile.points, activity.ngp = compute_ngp(activity.trackfile.points)

            try:
                data = json.loads(activity.data)
            except:
                data = {}

            data["heartrate_stats"] = get_heartrate_stats(activity.trackfile.points)
            data["power_stats"] = get_power_stats(activity.trackfile.points)
            activity.trimp = compute_trimp(data["heartrate_stats"], user)
            activity.level = compute_level(activity)
            data["records"] = compute_best_performances(activity.trackfile.points, activity.type)

            # Update threshold and FTP
            if activity.type == "Running":
                distance_1hour = None
                for record in data["records"]:
                    if record["type"] == "duration":
                        if record["duration"] == 60 * 60 * 1000:
                            distance_1hour = record["distance"]
                            break

                if distance_1hour is not None and distance_1hour > 0:
                    threshold = 1000000 * 3600 / distance_1hour
                    if threshold < 1000 * user.threshold:
                        Notification.objects.create(user=user,
                                                    date=timezone.now(),
                                                    text=_("New estimated threshold: %s. Please update your settings.") % duration_to_string(threshold),
                                                    url=reverse("settings"))
            elif activity.type == "Biking" or activity.type == "HomeTrainer":
                power_1hour = None
                power_20min = None
                for record in data["records"]:
                    if record["type"] == "power":
                        if record["duration"] == 60 * 60 * 1000:
                            power_1hour = int(record["power"])
                            break
                        elif record["duration"] == 20 * 60 * 1000:
                            power_20min = int(record["power"])

                if power_20min is not None and power_1hour is None:
                    power_1hour = int(0.95 * power_20min)

                if power_1hour is not None and power_1hour > user.ftp:
                    Notification.objects.create(user=user,
                                                date=timezone.now(),
                                                text=_("New estimated FTP: %s&nbsp;W. Please update your settings.") % power_1hour,
                                                url=reverse("settings"))

            if "user" not in data:
                data["user"] = {}
            data["user"]["heartrate_rest"] = user.heartrate_rest
            data["user"]["heartrate_max"] = user.heartrate_max

            activity.data = json.dumps(data)
            activity.save()

            # Update user records
            user_data["records"] = update_user_records(user)

            # Check global records
            need_save = False
            for record in data["records"]:
                activity_id = None
                try:
                    if record["type"] == "distance":
                        activity_id = user_data["records"][activity.type]["distance"][str(record["distance"])]["activity_id"]
                    elif record["type"] == "duration":
                        activity_id = user_data["records"][activity.type]["duration"][str(record["duration"])]["activity_id"]
                    elif record["type"] == "power":
                        activity_id = user_data["records"][activity.type]["power"][str(record["duration"])]["activity_id"]
                except:
                    continue
                if activity_id == activity.id:
                    record["best"] = True
                    need_save = True
            if need_save:
                activity.data = json.dumps(data)
                activity.save()

            # Save track
            filename_track = os.path.join(BASE_DIR, "data", "tracks", "%06d" % activity.id)
            activity.trackfile.save_to_file(filename_track)

            # Update goals
            update_goals_add_activity(goals, activity, user)

            ActivityFile.objects.create(activity=activity, filehash=filehash, filesize=filesize,
                                        compressed_size=compressed_size, filename=filename)

        # Update user records and goals
        user_data["goals"] = goals
        user.data = json.dumps(user_data)
        user.save()

    return activities, errors


@login_required
def view_activity_import(request):
    activities = []
    errors = []

    # TODO: check free space on storage

    if request.method == "POST":
        if "force_timezone" in request.POST:
            time_shift_delta = timezone.localtime(timezone.now()).utcoffset()
            time_shift = time_shift_delta.days * 24 * 60 + time_shift_delta.seconds // 60
        else:
            time_shift = None

        if "files" in request.FILES:
            files = request.FILES.getlist("files")
            if len(files) > MAX_UPLOAD_COUNT:
                errors.append(_("Too much files."))
            else:
                for file in files:
                    new_activies, new_errors = _import_file(file, request.user, time_shift)
                    activities += new_activies
                    errors += new_errors
        elif "file" in request.FILES:
            new_activies, new_errors = _import_file(request.FILES["file"], request.user, time_shift)
            activities += new_activies
            errors += new_errors

    return render(request, "activities/import_activity.html", {"activities": activities, "errors": errors})


@login_required
def view_files(request):
    fileslist = ActivityFile.objects.filter(activity__user=request.user).select_related("activity")
    return render(request, "activities/files.html", {"files": fileslist})


@login_required
def view_map(request):
    activities = Activity.objects.filter(user=request.user, type="Running")

    os.makedirs(os.path.join(BASE_DIR, "data", "tracks"), exist_ok=True)

    for activity in activities:
        filename_track = os.path.join(BASE_DIR, "data", "tracks", "%06d" % activity.id)
        if os.path.isfile(filename_track):
            activity.trackfile.load_from_file(filename_track)

    return render(request, "activities/map.html", {"activities": activities})


@login_required
def view_records(request):
    try:
        user_data = json.loads(request.user.data)
        records = user_data["records"]
    except:
        records = {}

    if "Running" not in records:
        records["Running"] = {
            "distance": {d: {"time": None, "distance": d, "activity_id": None} for d in record_distances_running},
            "duration": {d: {"distance": None, "duration": d, "activity_id": None} for d in record_durations},
            "max_distance": {"distance": None, "activity_id": None},
            "max_duration": {"duration": None, "activity_id": None},
        }

    if "Biking" not in records:
        records["Biking"] = {
            "distance": {d: {"time": None, "distance": d, "activity_id": None} for d in record_distances_biking},
            "duration": {d: {"distance": None, "duration": d, "activity_id": None} for d in record_durations},
            "power": {d: {"power": None, "duration": d, "activity_id": None} for d in record_power_durations},
            "max_distance": {"distance": None, "activity_id": None},
            "max_duration": {"duration": None, "activity_id": None},
        }

    if "Swimming" not in records:
        records["Swimming"] = {
            "distance": {d: {"time": None, "distance": d, "activity_id": None} for d in record_distances_swimming},
            "max_distance": {"distance": None, "activity_id": None},
            "max_duration": {"duration": None, "activity_id": None},
        }

    # Associate activity to each record
    for sport in records:
        if "distance" in records[sport]:
            for distance in list(records[sport]["distance"]):
                records[sport]["distance"][distance]["activity"] = None
                if records[sport]["distance"][distance]["activity_id"] is not None:
                    try:
                        records[sport]["distance"][distance]["activity"] = Activity.objects.get(id=records[sport]["distance"][distance]["activity_id"], user=request.user)
                    except:
                        pass
                if records[sport]["distance"][distance]["activity"] is None:
                    del records[sport]["distance"][distance]
            if len(records[sport]["distance"]) == 0:
                del records[sport]["distance"]

        if "duration" in records[sport]:
            for duration in list(records[sport]["duration"]):
                records[sport]["duration"][duration]["activity"] = None
                if records[sport]["duration"][duration]["activity_id"] is not None:
                    try:
                        records[sport]["duration"][duration]["activity"] = Activity.objects.get(id=records[sport]["duration"][duration]["activity_id"], user=request.user)
                    except:
                        pass
                if records[sport]["duration"][duration]["activity"] is None:
                    del records[sport]["duration"][duration]
            if len(records[sport]["duration"]) == 0:
                del records[sport]["duration"]

        if "power" in records[sport]:
            for duration in list(records[sport]["power"]):
                records[sport]["power"][duration]["activity"] = None
                if records[sport]["power"][duration]["activity_id"] is not None:
                    try:
                        records[sport]["power"][duration]["activity"] = Activity.objects.get(id=records[sport]["power"][duration]["activity_id"], user=request.user)
                    except:
                        pass
                if records[sport]["power"][duration]["activity"] is None:
                    del records[sport]["power"][duration]
            if len(records[sport]["power"]) == 0:
                del records[sport]["power"]

        if "max_distance" in records[sport]:
            records[sport]["max_distance"]["activity"] = None
            if records[sport]["max_distance"]["activity_id"] is not None:
                try:
                    records[sport]["max_distance"]["activity"] = Activity.objects.get(id=records[sport]["max_distance"]["activity_id"], user=request.user)
                except:
                    pass
            if records[sport]["max_distance"]["activity"] is None:
                del records[sport]["max_distance"]

        if "max_duration" in records[sport]:
            records[sport]["max_duration"]["activity"] = None
            if records[sport]["max_duration"]["activity_id"] is not None:
                try:
                    records[sport]["max_duration"]["activity"] = Activity.objects.get(id=records[sport]["max_duration"]["activity_id"], user=request.user)
                except:
                    pass
            if records[sport]["max_duration"]["activity"] is None:
                del records[sport]["max_duration"]

    previsions = compute_previsions(records)

    return render(request, "activities/records.html", {"records": records, "previsions": previsions})


@login_required
def view_record_evolution(request):
    type = request.GET.get("type")
    if not type or type not in ["distance", "duration", "power"]:
        return redirect("records")

    sport = request.GET.get("sport")
    if not sport or sport not in ["Running", "Biking", "HomeTrainer", "Swimming"]:
        return redirect("records")

    records = []
    records_improve = []
    record_previous = None

    distance = request.GET.get("distance", None)
    duration = request.GET.get("duration", None)

    if type == "distance":
        if not distance:
            return redirect("records")
        try:
            distance = int(distance)
        except:
            return redirect("records")

        if sport == "Running":
            if distance not in record_distances_running:
                return redirect("records")
        elif sport == "Biking":
            if distance not in record_distances_biking:
                return redirect("records")
        elif sport == "Swimming":
            if distance not in record_distances_swimming:
                return redirect("records")

        activities = Activity.objects.filter(type=sport, user=request.user).order_by("date")
        for activity in activities:
            try:
                act_records = json.loads(activity.data)["records"]
            except:
                act_records = []

            for record in act_records:
                if "distance" not in record or record["distance"] is None:
                    continue
                if "time" not in record or record["time"] is None:
                    continue
                if "type" not in record or record["type"] == "distance":
                    if record["distance"] == distance:
                        entry = {"improved": False, "activity": activity, "distance": distance, "duration": int(record["time"]), "duration_txt": duration_to_string(record["time"])}
                        if record_previous is None or entry["duration"] < record_previous["duration"]:
                            entry["improved"] = True
                            record["best"] = True

                            # Update activity records
                            try:
                                data = json.loads(activity.data)
                                data["records"] = act_records
                                activity.data = json.dumps(data)
                                activity.save()
                            except:
                                pass

                            records.append(entry)
                            record_previous = entry
                        else:
                            records.append(entry)
                            entry = record_previous.copy()
                            entry["activity"] = activity
                        records_improve.append(entry)

    elif type == "duration":
        if not duration:
            return redirect("records")
        try:
            duration = int(duration)
        except:
            return redirect("records")
        if duration not in record_durations:
            return redirect("records")

        activities = Activity.objects.filter(type=sport, user=request.user).order_by("date")
        for activity in activities:
            try:
                act_records = json.loads(activity.data)["records"]
            except:
                act_records = []

            for record in act_records:
                if "duration" not in record or record["duration"] is None:
                    continue
                if "distance" not in record or record["distance"] is None:
                    continue
                if "type" in record and record["type"] == "duration":
                    if record["duration"] == duration:
                        entry = {"improved": False, "activity": activity, "distance": int(record["distance"]), "duration": duration}
                        if record_previous is None or entry["distance"] > record_previous["distance"]:
                            entry["improved"] = True
                            record["best"] = True

                            # Update activity records
                            try:
                                ddd = json.loads(activity.data)
                                ddd["records"] = act_records
                                activity.data = json.dumps(ddd)
                                activity.save()
                            except:
                                pass

                            records.append(entry)
                            record_previous = entry
                        else:
                            records.append(entry)
                            entry = record_previous.copy()
                            entry["activity"] = activity
                        records_improve.append(entry)
    elif type == "power":
        if not duration:
            return redirect("records")
        try:
            duration = int(duration)
        except:
            return redirect("records")
        if duration not in record_power_durations:
            return redirect("records")

        activities = Activity.objects.filter(type=sport, user=request.user).order_by("date")
        for activity in activities:
            try:
                act_records = json.loads(activity.data)["records"]
            except:
                act_records = []

            for record in act_records:
                if "duration" not in record or record["duration"] is None:
                    continue
                if "power" not in record or record["power"] is None:
                    continue
                if "type" in record and record["type"] == "power":
                    if record["duration"] == duration:
                        entry = {"improved": False, "activity": activity, "power": int(record["power"]), "duration": duration}
                        if record_previous is None or entry["power"] > record_previous["power"]:
                            entry["improved"] = True
                            record["best"] = True

                            # Update activity records
                            try:
                                ddd = json.loads(activity.data)
                                ddd["records"] = act_records
                                activity.data = json.dumps(ddd)
                                activity.save()
                            except:
                                pass

                            records.append(entry)
                            record_previous = entry
                        else:
                            records.append(entry)
                            entry = record_previous.copy()
                            entry["activity"] = activity
                        records_improve.append(entry)

    return render(request, "activities/record_evolution.html", {
        "type": type,
        "sport": sport,
        "distance": distance,
        "duration": duration,
        "records": records,
        "records_improve": records_improve,
    })


@login_required
def view_threshold(request):
    stats = {}  # date -> stats for the day
    current_year = timezone.now().year
    years = [current_year]

    activities = Activity.objects.filter(Q(type='HomeTrainer') | Q(type='Biking'), user=request.user).order_by("date")
    for activity in activities:
        date = activity.date.date()
        if date not in stats:
            stats[date] = {"count": 0, "duration": 0, "ftp_estimated": None}

        if activity.date.year not in years:
            years.append(activity.date.year)

        stats[date]["count"] += 1

        try:
            data = json.loads(activity.data)
            if "records" not in data:
                continue
        except:
            continue

        # Compute FTP
        power_1hour = None
        power_20min = None
        for record in data["records"]:
            if record["type"] == "power":
                if record["duration"] == 60 * 60 * 1000:
                    power_1hour = int(record["power"])
                    break
                elif record["duration"] == 20 * 60 * 1000:
                    power_20min = int(record["power"])

        if power_20min is not None and power_1hour is None:
            power_1hour = int(0.95 * power_20min)

        # No FTP for this activity
        if power_1hour is None:
            continue

        if activity.duration is not None:
            stats[date]["duration"] += activity.duration // (60*1000)

        if stats[date]["ftp_estimated"] is None or power_1hour > stats[date]["ftp_estimated"]:
            stats[date]["ftp_estimated"] = power_1hour

    years = sorted(years)

    date_start = datetime.date(years[0], 1, 1)
    date_end = datetime.datetime.now() + datetime.timedelta(days=180)

    # Fill stats with all date
    for date in rrule(DAILY, dtstart=date_start, until=date_end):
        date = date.date()
        if date.year not in years:
            years.append(date.year)
        if date not in stats:
            stats[date] = {"count": 0, "duration": 0, "ftp_estimated": None}

    years = sorted(years)
    stats = dict(sorted(stats.items()))

    min_ftp = 100.0
    factor = 0.997

    changes = []
    previous_ftp = min_ftp
    days_without_increase = 1
    for date, score in stats.items():
        if days_without_increase < 10:
            ftp = previous_ftp
        else:
            ftp = previous_ftp * factor
            if ftp < min_ftp:
                ftp = min_ftp

        if score["count"] == 0:
            days_without_increase += 1
            del score["ftp_estimated"]
        else:
            if score["ftp_estimated"] is None:
                del score["ftp_estimated"]
            elif score["ftp_estimated"] > ftp:
                ftp = score["ftp_estimated"]
                changes.append((date, ftp))
                days_without_increase = 1

        previous_ftp = score["ftp"] = ftp

    if len(changes) > 1:
        index = 0
        days = 0
        shift = (changes[index + 1][1] - changes[index][1]) / (changes[index + 1][0] - changes[index][0]).days

        for date in rrule(DAILY, dtstart=changes[0][0], until=changes[len(changes) - 1][0]):
            date = date.date()

            if date == changes[index][0]:  # first day
                continue
            if date == changes[len(changes) - 1][0]:  # last day
                break

            if date == changes[index + 1][0]:
                index += 1
                days = 0
                if changes[index + 1][1] < changes[index][1]:
                    shift = (changes[index + 1][1] - changes[index][1]) / ((changes[index + 1][0] - changes[index][0]).days - 10)
                else:
                    shift = (changes[index + 1][1] - changes[index][1]) / (changes[index + 1][0] - changes[index][0]).days
            else:
                days += 1
                if shift < 0:
                    if days < 10:
                        stats[date]["ftp"] = changes[index][1]
                    else:
                        stats[date]["ftp"] = changes[index][1] + shift * (days - 10)
                else:
                    stats[date]["ftp"] = changes[index][1] + shift * days

    return render(request, "activities/threshold.html", {"current_year": current_year, "years": years, "stats": stats,})


@login_required
def view_api_stats(request):
    if "type" in request.GET:
        type = request.GET["type"].strip()
    else:
        return JsonResponse({"status": "error", "msg": "The parameter 'type' is mandatory."})

    if "period" in request.GET:
        period = request.GET["period"].strip()
    else:
        return JsonResponse({"status": "error", "msg": "The parameter 'period' is mandatory."})

    # Filters
    if "sport" in request.GET:
        sport = request.GET["sport"].strip()
        sport_txt = sport
        if sport in ["", "*", "All"]:
            sport = None
            sport_txt = "All"
        elif sport not in sports:
            return JsonResponse({"status": "error", "msg": "Invalid sport."})
    else:
        sport = None
        sport_txt = "All"

    date_min = timezone.now() - datetime.timedelta(days=365*8)
    if "date_min" in request.GET:
        try:
            date_min = make_aware(datetime.datetime.combine(datetime.date.fromisoformat(request.GET["date_min"]), datetime.datetime.min.time()))
        except:
            return JsonResponse({"status": "error", "msg": "Invalid value for parameter 'date_min'."})

    date_max = timezone.now()
    if "date_max" in request.GET:
        try:
            date_max = make_aware(datetime.datetime.combine(datetime.date.fromisoformat(request.GET["date_max"]), datetime.datetime.max.time()))
        except:
            return JsonResponse({"status": "error", "msg": "Invalid value for parameter 'date_max'."})

    if type == "count":
        data = DataSourceCount()
    elif type == "total_duration":
        data = DataSourceTotalDuration()
    elif type == "average_duration":
        data = DataSourceAverageDuration()
    else:
        return JsonResponse({"status": "error", "msg": "Invalid value for parameter 'type'."})

    # TODO: moyenne glissante pondérée
    #data = DataSourceMovingAverage(data.get_monthly_data(date_min, date_max, request.user, sport), 3)

    if period == "year":
        date_min = date_min.replace(month=1, day=1, hour=0, minute=0, second=0, microsecond=0)
        date_max = date_max.replace(month=12, day=31, hour=23, minute=59, second=59, microsecond=999)
        data = data.get_yearly_data(date_min, date_max, request.user, sport)
    elif period == "month":
        date_min = date_min.replace(day=1, hour=0, minute=0, second=0, microsecond=0)
        date_max = date_max.replace(day=monthrange(date_max.year, date_max.month)[1], hour=23, minute=59, second=59, microsecond=999)
        data = data.get_monthly_data(date_min, date_max, request.user, sport)
    elif period == "week":
        # TODO: trouver le début et la fin de la semaine
        # calendar.weekday(year, month, day)
        date_min = date_min.replace(hour=0, minute=0, second=0, microsecond=0)
        date_max = date_max.replace(hour=23, minute=59, second=59, microsecond=999)
        data = data.get_weekly_data(date_min, date_max, request.user, sport)
    elif period == "day":
        date_min = date_min.replace(hour=0, minute=0, second=0, microsecond=0)
        date_max = date_max.replace(hour=23, minute=59, second=59, microsecond=999)
        data = data.get_daily_data(date_min, date_max, request.user, sport)
    else:
        return JsonResponse({"status": "error", "msg": "Invalid value for parameter 'period'."})

    response = {"status": "success", "date_min": date_min, "date_max": date_max, "type": type, "period": period, "sport": sport_txt, "data": data}

    return JsonResponse(response)


@login_required
def view_stats(request):
    stats_types = ["count", "duration", "trimp", "distance", "cadence", "cadence_duration", "heartrate", "heartrate_duration"]

    current_year = timezone.now().year
    activities_month = {"All": {stats_type: {} for stats_type in stats_types}}
    activities_year = {"All": {stats_type: {} for stats_type in stats_types}}
    activities_week = {"All": {stats_type: {} for stats_type in stats_types}}
    years = [current_year]
    activities_in_current_year = False

    def set_activities_year(year, type):
        for stats_type in stats_types:
            if year not in activities_month[type][stats_type]:
                activities_month[type][stats_type][year] = [0 for _ in range(12)]
            if year not in activities_month["All"][stats_type]:
                activities_month["All"][stats_type][year] = [0 for _ in range(12)]
            if year not in activities_year[type][stats_type]:
                activities_year[type][stats_type][year] = 0
            if year not in activities_year["All"][stats_type]:
                activities_year["All"][stats_type][year] = 0

            first_week_number = 1
            last_week_number = datetime.datetime(year, 12, 31).isocalendar().week
            if last_week_number == 1:
                last_week_number = datetime.datetime(year, 12, 24).isocalendar().week + 1
            if datetime.datetime(year, 1, 1).isocalendar().week != 1:
                first_week_number = 0
            if year not in activities_week[type][stats_type]:
                activities_week[type][stats_type][year] = {w: 0 for w in range(first_week_number, last_week_number + 1)}
            if year not in activities_week["All"][stats_type]:
                activities_week["All"][stats_type][year] = {w: 0 for w in range(first_week_number, last_week_number + 1)}

    activities = Activity.objects.filter(user=request.user, planned=False)
    for activity in activities:
        if activity.type not in activities_month:
            activities_month[activity.type] = {stats_type: {} for stats_type in stats_types}
        if activity.type not in activities_year:
            activities_year[activity.type] = {stats_type: {} for stats_type in stats_types}
        if activity.type not in activities_week:
            activities_week[activity.type] = {stats_type: {} for stats_type in stats_types}

        if activity.date.year not in years:
            years.append(activity.date.year)

        set_activities_year(activity.date.year, activity.type)

        # Remplissage des tableaux d'activités
        week, other_week = DataSource.compute_week(activity.date)
        if other_week is not None:
            set_activities_year(other_week[0], activity.type)

        activities_month[activity.type]["count"][activity.date.year][activity.date.month - 1] += 1
        activities_month["All"]["count"][activity.date.year][activity.date.month - 1] += 1
        activities_year[activity.type]["count"][activity.date.year] += 1
        activities_year["All"]["count"][activity.date.year] += 1
        activities_week[activity.type]["count"][activity.date.year][week] += 1
        activities_week["All"]["count"][activity.date.year][week] += 1
        if other_week is not None:
            activities_week[activity.type]["count"][other_week[0]][other_week[1]] += 1
            activities_week["All"]["count"][other_week[0]][other_week[1]] += 1

        if activity.duration is not None:
            activities_month[activity.type]["duration"][activity.date.year][activity.date.month - 1] += activity.duration // (60*1000)
            activities_month["All"]["duration"][activity.date.year][activity.date.month - 1] += activity.duration // (60*1000)
            activities_year[activity.type]["duration"][activity.date.year] += activity.duration // (60*1000)
            activities_year["All"]["duration"][activity.date.year] += activity.duration // (60*1000)
            activities_week[activity.type]["duration"][activity.date.year][week] += activity.duration // (60*1000)
            activities_week["All"]["duration"][activity.date.year][week] += activity.duration // (60*1000)
            if other_week is not None:
                activities_week[activity.type]["duration"][other_week[0]][other_week[1]] += activity.duration // (60*1000)
                activities_week["All"]["duration"][other_week[0]][other_week[1]] += activity.duration // (60*1000)

            if activity.cadence_avg is not None:
                activities_month[activity.type]["cadence"][activity.date.year][activity.date.month - 1] += activity.cadence_avg * activity.duration
                activities_month["All"]["cadence"][activity.date.year][activity.date.month - 1] += activity.cadence_avg * activity.duration
                activities_year[activity.type]["cadence"][activity.date.year] += activity.cadence_avg * activity.duration
                activities_year["All"]["cadence"][activity.date.year] += activity.cadence_avg * activity.duration
                activities_week[activity.type]["cadence"][activity.date.year][week] += activity.cadence_avg * activity.duration
                activities_week["All"]["cadence"][activity.date.year][week] += activity.cadence_avg * activity.duration
                if other_week is not None:
                    activities_week[activity.type]["cadence"][other_week[0]][other_week[1]] += activity.cadence_avg * activity.duration
                    activities_week["All"]["cadence"][other_week[0]][other_week[1]] += activity.cadence_avg * activity.duration

                activities_month[activity.type]["cadence_duration"][activity.date.year][activity.date.month - 1] += activity.duration
                activities_month["All"]["cadence_duration"][activity.date.year][activity.date.month - 1] += activity.duration
                activities_year[activity.type]["cadence_duration"][activity.date.year] += activity.duration
                activities_year["All"]["cadence_duration"][activity.date.year] += activity.duration
                activities_week[activity.type]["cadence_duration"][activity.date.year][week] += activity.duration
                activities_week["All"]["cadence_duration"][activity.date.year][week] += activity.duration
                if other_week is not None:
                    activities_week[activity.type]["cadence_duration"][other_week[0]][other_week[1]] += activity.duration
                    activities_week["All"]["cadence_duration"][other_week[0]][other_week[1]] += activity.duration

            if activity.heart_rate_avg is not None:
                activities_month[activity.type]["heartrate"][activity.date.year][activity.date.month - 1] += activity.heart_rate_avg * activity.duration
                activities_month["All"]["heartrate"][activity.date.year][activity.date.month - 1] += activity.heart_rate_avg * activity.duration
                activities_year[activity.type]["heartrate"][activity.date.year] += activity.heart_rate_avg * activity.duration
                activities_year["All"]["heartrate"][activity.date.year] += activity.heart_rate_avg * activity.duration
                activities_week[activity.type]["heartrate"][activity.date.year][week] += activity.heart_rate_avg * activity.duration
                activities_week["All"]["heartrate"][activity.date.year][week] += activity.heart_rate_avg * activity.duration
                if other_week is not None:
                    activities_week[activity.type]["heartrate"][other_week[0]][other_week[1]] += activity.heart_rate_avg * activity.duration
                    activities_week["All"]["heartrate"][other_week[0]][other_week[1]] += activity.heart_rate_avg * activity.duration

                activities_month[activity.type]["heartrate_duration"][activity.date.year][activity.date.month - 1] += activity.duration
                activities_month["All"]["heartrate_duration"][activity.date.year][activity.date.month - 1] += activity.duration
                activities_year[activity.type]["heartrate_duration"][activity.date.year] += activity.duration
                activities_year["All"]["heartrate_duration"][activity.date.year] += activity.duration
                activities_week[activity.type]["heartrate_duration"][activity.date.year][week] += activity.duration
                activities_week["All"]["heartrate_duration"][activity.date.year][week] += activity.duration
                if other_week is not None:
                    activities_week[activity.type]["heartrate_duration"][other_week[0]][other_week[1]] += activity.duration
                    activities_week["All"]["heartrate_duration"][other_week[0]][other_week[1]] += activity.duration

        if activity.distance is not None:
            activities_month[activity.type]["distance"][activity.date.year][activity.date.month - 1] += activity.distance / 1000
            activities_month["All"]["distance"][activity.date.year][activity.date.month - 1] += activity.distance / 1000
            activities_year[activity.type]["distance"][activity.date.year] += activity.distance / 1000
            activities_year["All"]["distance"][activity.date.year] += activity.distance / 1000
            activities_week[activity.type]["distance"][activity.date.year][week] += activity.distance / 1000
            activities_week["All"]["distance"][activity.date.year][week] += activity.distance / 1000
            if other_week is not None:
                activities_week[activity.type]["distance"][other_week[0]][other_week[1]] += activity.distance / 1000
                activities_week["All"]["distance"][other_week[0]][other_week[1]] += activity.distance / 1000

        if activity.trimp is not None:
            activities_month[activity.type]["trimp"][activity.date.year][activity.date.month - 1] += activity.trimp
            activities_month["All"]["trimp"][activity.date.year][activity.date.month - 1] += activity.trimp
            activities_year[activity.type]["trimp"][activity.date.year] += activity.trimp
            activities_year["All"]["trimp"][activity.date.year] += activity.trimp
            activities_week[activity.type]["trimp"][activity.date.year][week] += activity.trimp
            activities_week["All"]["trimp"][activity.date.year][week] += activity.trimp
            if other_week is not None:
                activities_week[activity.type]["trimp"][other_week[0]][other_week[1]] += activity.trimp
                activities_week["All"]["trimp"][other_week[0]][other_week[1]] += activity.trimp

        if activity.date.year == current_year:
            activities_in_current_year = True

    years = sorted(years)

    """
    # Ajout du type d'activité "Autre" s'il y a au moins une activité dans l'année courante (pourquoi ??? => sans doute un bug en 2019)
    if activities_in_current_year is False:
        if "Other" not in activities_month:
            activities_month["Other"] = {stats_type: {} for stats_type in stats_types}
        if "Other" not in activities_week:
            activities_week["Other"] = {stats_type: {} for stats_type in stats_types}

        first_week_number = 1
        last_week_number = datetime.datetime(current_year, 12, 31).isocalendar().week
        if last_week_number == 1:
            last_week_number = datetime.datetime(current_year, 12, 24).isocalendar().week + 1
        if datetime.datetime(current_year, 1, 1).isocalendar().week != 1:
            first_week_number = 0

        for stats_type in stats_types:
            activities_month["Other"][stats_type][current_year] = [0 for _ in range(12)]
            activities_week["Other"][stats_type][current_year] = {w: 0 for w in range(first_week_number, last_week_number + 1)}
    """

    # Calcul des moyennes mensuelles
    for sport, act_type in activities_month.items():
        # Tri par année
        for type, months_act in act_type.items():
            activities_month[sport][type] = dict(sorted(months_act.items()))

        activities_month[sport]["duration_avg"] = {}
        activities_month[sport]["distance_avg"] = {}
        activities_month[sport]["cadence_avg"] = {}
        activities_month[sport]["heartrate_avg"] = {}

        for year in years:
            if year in activities_month[sport]["count"]:
                activities_month[sport]["duration_avg"][year] = [0 for _ in range(12)]
                activities_month[sport]["distance_avg"][year] = [0 for _ in range(12)]
                activities_month[sport]["cadence_avg"][year] = [0 for _ in range(12)]
                activities_month[sport]["heartrate_avg"][year] = [0 for _ in range(12)]
                for m in range(12):
                    try:
                        activities_month[sport]["duration_avg"][year][m] = activities_month[sport]["duration"][year][m] // activities_month[sport]["count"][year][m]
                    except:
                        pass
                    try:
                        activities_month[sport]["distance_avg"][year][m] = activities_month[sport]["distance"][year][m] / activities_month[sport]["count"][year][m]
                    except:
                        pass
                    try:
                        activities_month[sport]["cadence_avg"][year][m] = activities_month[sport]["cadence"][year][m] // activities_month[sport]["cadence_duration"][year][m]
                    except:
                        pass
                    try:
                        activities_month[sport]["heartrate_avg"][year][m] = activities_month[sport]["heartrate"][year][m] // activities_month[sport]["heartrate_duration"][year][m]
                    except:
                        pass

        del activities_month[sport]["cadence"]
        del activities_month[sport]["cadence_duration"]
        del activities_month[sport]["heartrate"]
        del activities_month[sport]["heartrate_duration"]

    # Calcul des moyennes annuelles
    for sport, years_type in activities_year.items():
        for type, years_act in years_type.items():
            for year in years:
                if year not in years_act:
                    years_act[year] = 0
            activities_year[sport][type] = dict(sorted(years_act.items()))

        activities_year[sport]["duration_avg"] = {}
        activities_year[sport]["distance_avg"] = {}
        activities_year[sport]["cadence_avg"] = {}
        activities_year[sport]["heartrate_avg"] = {}

        for year in years:
            try:
                activities_year[sport]["duration_avg"][year] = activities_year[sport]["duration"][year] // activities_year[sport]["count"][year]
            except:
                activities_year[sport]["duration_avg"][year] = 0
            try:
                activities_year[sport]["distance_avg"][year] = activities_year[sport]["distance"][year] / activities_year[sport]["count"][year]
            except:
                activities_year[sport]["distance_avg"][year] = 0
            try:
                activities_year[sport]["cadence_avg"][year] = activities_year[sport]["cadence"][year] // activities_year[sport]["cadence_duration"][year]
            except:
                activities_year[sport]["cadence_avg"][year] = 0
            try:
                activities_year[sport]["heartrate_avg"][year] = activities_year[sport]["heartrate"][year] // activities_year[sport]["heartrate_duration"][year]
            except:
                activities_year[sport]["heartrate_avg"][year] = 0

        del activities_year[sport]["cadence"]
        del activities_year[sport]["cadence_duration"]
        del activities_year[sport]["heartrate"]
        del activities_year[sport]["heartrate_duration"]

    for sport, act_type in activities_week.items():
        # Tri par année
        for type, weeks_act in act_type.items():
            activities_week[sport][type] = dict(sorted(weeks_act.items()))

        activities_week[sport]["duration_avg"] = {}
        activities_week[sport]["distance_avg"] = {}
        activities_week[sport]["cadence_avg"] = {}
        activities_week[sport]["heartrate_avg"] = {}

        for year in years:
            if year in activities_week[sport]["count"]:
                first_week_number = 1
                last_week_number = datetime.datetime(year, 12, 31).isocalendar().week
                if last_week_number == 1:
                    last_week_number = datetime.datetime(year, 12, 24).isocalendar().week + 1
                if datetime.datetime(year, 1, 1).isocalendar().week != 1:
                    first_week_number = 0

                activities_week[sport]["duration_avg"][year] = {w: 0 for w in range(first_week_number, last_week_number + 1)}
                activities_week[sport]["distance_avg"][year] = {w: 0 for w in range(first_week_number, last_week_number + 1)}
                activities_week[sport]["cadence_avg"][year] = {w: 0 for w in range(first_week_number, last_week_number + 1)}
                activities_week[sport]["heartrate_avg"][year] = {w: 0 for w in range(first_week_number, last_week_number + 1)}
                for w in range(first_week_number, last_week_number + 1):
                    try:
                        activities_week[sport]["duration_avg"][year][w] = activities_week[sport]["duration"][year][w] // activities_week[sport]["count"][year][w]
                    except:
                        pass
                    try:
                        activities_week[sport]["distance_avg"][year][w] = activities_week[sport]["distance"][year][w] / activities_week[sport]["count"][year][w]
                    except:
                        pass
                    try:
                        activities_week[sport]["cadence_avg"][year][w] = activities_week[sport]["cadence"][year][w] // activities_week[sport]["cadence_duration"][year][w]
                    except:
                        pass
                    try:
                        activities_week[sport]["heartrate_avg"][year][w] = activities_week[sport]["heartrate"][year][w] // activities_week[sport]["heartrate_duration"][year][w]
                    except:
                        pass

        del activities_week[sport]["cadence"]
        del activities_week[sport]["cadence_duration"]
        del activities_week[sport]["heartrate"]
        del activities_week[sport]["heartrate_duration"]

    return render(request, "activities/stats.html", {
        "current_year": current_year,
        "years": years,
        "activities_month": activities_month,
        "activities_year": activities_year,
        "activities_week": activities_week,
    })


@login_required
def view_progression(request):
    current_year = timezone.now().year
    scores = {"All": {}}
    years = [current_year]

    if "tss" in request.GET:
        with_tss = True
    else:
        with_tss = False

    activities = Activity.objects.filter(user=request.user)
    for activity in activities:
        if activity.date.year not in years:
            years.append(activity.date.year)

        if activity.trimp or activity.level or activity.tss:
            date = activity.date.date()
            if date not in scores["All"]:
                scores["All"][date] = {"trimp": 0, "tss": 0, "level": 0, "fitness": 0, "fatigue": 0, "form": 0}

            type = activity.type
            if type == "HomeTrainer":
                type = "Biking"

            if activity.trimp:
                scores["All"][date]["trimp"] += activity.trimp
                if type in ["Running", "Biking", "Swimming"]:
                    if type not in scores:
                        scores[type] = {}
                    if date not in scores[type]:
                        scores[type][date] = {"trimp": 0, "tss": 0, "level": 0, "fitness": 0, "fatigue": 0, "form": 0}
                    scores[type][date]["trimp"] += activity.trimp

            if activity.tss:
                scores["All"][date]["tss"] += activity.tss
                if type in ["Running", "Biking", "Swimming"]:
                    if type not in scores:
                        scores[type] = {}
                    if date not in scores[type]:
                        scores[type][date] = {"trimp": 0, "tss": 0, "level": 0, "fitness": 0, "fatigue": 0, "form": 0}
                    scores[type][date]["tss"] += activity.tss

            if activity.level:
                if activity.level > scores["All"][date]["level"]:
                    scores["All"][date]["level"] = activity.level
                if type in ["Running"]:
                    if type not in scores:
                        scores[type] = {}
                    if date not in scores[type]:
                        scores[type][date] = {"trimp": 0, "tss": 0, "level": 0, "fitness": 0, "fatigue": 0, "form": 0}
                    if activity.level > scores[type][date]["level"]:
                        scores[type][date]["level"] = activity.level

    years = sorted(years)

    # Compute scores for each day and each activity type
    for activity_type, activity_scores in scores.items():
        scores[activity_type] = compute_fitness(activity_scores, years[0], with_tss)

    return render(request, "activities/progression.html", {"current_year": current_year, "years": years, "scores": scores, "with_tss": with_tss})


@login_required
def view_calendar(request):
    activities = Activity.objects.filter(user=request.user).order_by("date")

    weeks = {}
    for activity in activities:
        year = activity.date.isocalendar().year
        week = activity.date.isocalendar().week

        if year not in weeks:
            weeks[year] = {}
        if week not in weeks[year]:
            weeks[year][week] = {"count": 0, "duration": 0, "trimp": 0}

        weeks[year][week]["count"] += 1
        if activity.duration is not None:
            weeks[year][week]["duration"] += activity.duration
            weeks[year][week]["duration_txt"] = duration_to_string(weeks[year][week]["duration"])
        if activity.trimp is not None:
            weeks[year][week]["trimp"] += activity.trimp

    competitions = Competition.objects.filter(user=request.user).order_by("date")
    notes = Note.objects.filter(user=request.user).order_by("date_start")

    return render(request, "activities/calendar_v2.html", {
        "activities": activities,
        "weeks": weeks,
        "competitions": competitions,
        "notes": notes,
    })


@login_required
def view_notes(request):
    notes = Note.objects.filter(user=request.user)

    return render(request, "activities/notes.html", {
        "notes": notes,
    })


@login_required
def view_api_activities(request):
    activities = Activity.objects.filter(user=request.user)

    try:
        draw = int(request.GET["draw"])
    except:
        draw = None

    start = 0
    if "start" in request.GET:
        try:
            start = int(request.GET["start"])
        except:
            return JsonResponse({"status": "error", "msg": "Invalid start."})
    if start < 0:
        return JsonResponse({"status": "error", "msg": "Invalid start."})

    length = -1
    if "length" in request.GET:
        try:
            length = int(request.GET["length"])
        except:
            return JsonResponse({"status": "error", "msg": "Invalid length."})
    if length == 0:
        return JsonResponse({"status": "error", "msg": "Invalid length."})
    if length < 0:
        length = -1

    count = activities.count()

    # Filters
    if "sport" in request.GET:
        sport = request.GET["sport"].strip()
        if sport in sports:
            activities = activities.filter(type=sport)
        elif sport in ["", "*", "All"]:
            sport = "All"
        else:
            return JsonResponse({"status": "error", "msg": "Invalid sport."})
    else:
        sport = "All"

    if "dateMin" in request.GET:
        try:
            date_min = make_aware(datetime.datetime.combine(datetime.date.fromisoformat(request.GET["dateMin"]), datetime.datetime.min.time()))
            activities = activities.filter(date__gte=date_min)
        except:
            return JsonResponse({"status": "error", "msg": "Invalid dateMin."})

    if "dateMax" in request.GET:
        try:
            date_max = make_aware(datetime.datetime.combine(datetime.date.fromisoformat(request.GET["dateMax"]), datetime.datetime.max.time()))
            activities = activities.filter(date__lte=date_max)
        except:
            return JsonResponse({"status": "error", "msg": "Invalid dateMax."})

    # TODO: filter by equipment

    count_filtered = activities.count()

    # Ordering
    order = "date"
    try:
        column_id = int(request.GET["order[0][column]"])
        if column_id >= 0:
            order = request.GET["columns[%d][name]" % column_id]
            if order == "heartrate":
                order = "heart_rate_avg"
            elif order == "cadence":
                order = "cadence_avg"
            elif order == "power":
                order = "power_avg"
    except:
        order = "date"

    if order not in ["name", "date", "type", "duration", "distance", "heart_rate_avg", "cadence_avg", "power_avg", "trimp", "tss", "calories"]:
        order = "date"

    if "order[0][dir]" in request.GET and request.GET["order[0][dir]"] == "desc":
        order = "-" + order

    activities = activities.order_by(order)

    if length > 0:
        activities = activities[start:start+length]
    else:
        activities = activities[start:]

    data = []
    for activity in activities:
        equipments = []
        for equipment in activity.equipments.all():
            equipments.append(str(equipment))

        if activity.type in sports:
            activity_sport = sports[activity.type]
        else:
            activity_sport = sports["Other"]

        data.append({
            "id": activity.id,
            "date": str(activity.date.replace(tzinfo=None)),
            "date_txt": date_short(activity.date_local),
            "name": activity.name or activity_sport["name"],
            "url": reverse("activity", kwargs={"aid": activity.id}),
            "type": activity.type,
            "type_txt": activity_sport["name"],
            "icon": activity_sport["icon"],
            "bgcolor": activity_sport["bgcolor"],
            "textcolor": activity_sport["textcolor"],
            "duration": activity.duration,
            "duration_txt": duration_to_string(activity.duration),
            "distance": activity.distance,
            "distance_txt": distance_to_string(activity.distance),
            "notes": activity.notes,
            "trimp": activity.trimp,
            "tss": activity.tss,
            "heartrate": activity.heart_rate_avg,
            "cadence": activity.cadence_avg,
            "power": activity.power_avg,
            "calories": activity.calories,
            "equipments": ", ".join(equipments),
        })

    response = {"status": "success", "start": start, "recordsFiltered": count_filtered, "recordsTotal": count, "sport": sport, "data": data}

    if draw is not None:
        response["draw"] = draw

    return JsonResponse(response)


@login_required
def view_activity(request, aid):
    try:
        activity = Activity.objects.get(id=aid, user=request.user)
    except Activity.DoesNotExist:
        return redirect("activities")

    try:
        user_data = json.loads(request.user.data)
    except:
        user_data = {"goals": [], "records": []}

    activity.first_point = None
    activity.last_point = None

    try:
        activity.data = json.loads(activity.data)
    except:
        activity.data = {
            "pauses": [],
            "heartrate_stats": {hr: 0 for hr in range(40, 220 + 1)},
            "power_stats": {power: 0 for power in range(0, 1000)},
            "segments": [],
            "records": [],
        }

    try:
        filename_track = os.path.join(BASE_DIR, "data", "tracks", "%06d" % activity.id)
        if os.path.isfile(filename_track):
            activity.trackfile.load_from_file(filename_track)

        for point in activity.trackfile.points:
            if "latitude" not in point or "longitude" not in point:
                continue
            if point["latitude"] and point["longitude"]:
                activity.first_point = point
                break

        for point in reversed(activity.trackfile.points):
            if "latitude" not in point or "longitude" not in point:
                continue
            if point["latitude"] and point["longitude"]:
                activity.last_point = point
                break

        # Fix bad latitude or longitude (for map)
        first_latitude = first_longitude = None
        prev_latitude = prev_longitude = None
        for point in activity.trackfile.points:
            if first_latitude is None and point["latitude"] is not None and point["latitude"] != 0:
                first_latitude = point["latitude"]
            if first_longitude is None and point["longitude"] is not None and point["longitude"] != 0:
                first_longitude = point["longitude"]

            if point["latitude"] is None or point["latitude"] == 0:
                point["latitude"] = prev_latitude
            else:
                prev_latitude = point["latitude"]
            if point["longitude"] is None or point["longitude"] == 0:
                point["longitude"] = prev_longitude
            else:
                prev_longitude = point["longitude"]

        # Fix bad latitude or longitude for activity start
        for point in activity.trackfile.points:
            if point["latitude"] is not None and point["latitude"] != 0 and point["longitude"] is not None and point["longitude"] != 0:
                break
            if point["latitude"] is None or point["latitude"] == 0:
                point["latitude"] = first_latitude
            if point["longitude"] is None or point["longitude"] == 0:
                point["longitude"] = first_longitude
    except:
        pass

    records_distance = []
    records_duration = []
    records_power = []

    # Check global records
    if "records" in activity.data:
        for record in activity.data["records"]:
            record["current_record"] = False
            activity_id = None
            if record["type"] == "distance":
                try:
                    activity_id = user_data["records"][activity.type]["distance"][str(record["distance"])]["activity_id"]
                except:
                    activity_id = None
                records_distance.append(record)
            elif record["type"] == "duration":
                try:
                    activity_id = user_data["records"][activity.type]["duration"][str(record["duration"])]["activity_id"]
                except:
                    activity_id = None
                records_duration.append(record)
            elif record["type"] == "power":
                try:
                    activity_id = user_data["records"][activity.type]["power"][str(record["duration"])]["activity_id"]
                except:
                    activity_id = None
                records_power.append(record)
            if activity_id == activity.id:
                record["current_record"] = True

    segments_duration = 0
    segments_distance = 0
    segments_speed = 0.0

    if "segments" in activity.data and len(activity.data["segments"]) > 0:
        for segment in activity.data["segments"]:
            segments_duration += segment["duration"]

            if segment["distance"] is not None:
                segments_distance += segment["distance"]

        segments_duration /= len(activity.data["segments"])
        segments_distance /= len(activity.data["segments"])
        if segments_duration > 0:
            segments_speed = 1000 * segments_distance / segments_duration

    hr_reserve = request.user.heartrate_max - request.user.heartrate_rest
    heartrate_zones = [
        int(request.user.heartrate_rest + 0.5 * hr_reserve),
        int(request.user.heartrate_rest + 0.6 * hr_reserve),
        int(request.user.heartrate_rest + 0.7 * hr_reserve),
        int(request.user.heartrate_rest + 0.8 * hr_reserve),
        int(request.user.heartrate_rest + 0.9 * hr_reserve),
    ]

    power_zones = [
        int(0.55 * request.user.ftp),
        int(0.75 * request.user.ftp),
        int(0.90 * request.user.ftp),
        int(1.05 * request.user.ftp),
        int(1.20 * request.user.ftp),
    ]

    return render(request, "activities/activity.html", {
        "activity": activity,
        "files": activity.files.all(),
        "heartrate_zones": heartrate_zones,
        "power_zones": power_zones,
        "segments_duration": segments_duration,
        "segments_distance": segments_distance,
        "segments_speed": segments_speed,
        "records_distance": records_distance,
        "records_duration": records_duration,
        "records_power": records_power,
    })


@login_required
def view_activity_redirect(request, aid):
    return redirect("activity", aid)


@login_required
def view_activity_edit(request, aid):
    try:
        activity = Activity.objects.get(id=aid, user=request.user)
    except Activity.DoesNotExist:
        return redirect("activities")

    # TODO: filtrer les équipements de l'utilisateur !

    if request.method == "POST":
        form1 = ActivityForm(request.POST, instance=activity)
        form2 = ActivityAdvancedForm(request.POST, instance=activity)

        if form1.is_valid() and form2.is_valid():
            activity = form1.save(commit=False)
            activity.save()
            form1.save_m2m()

            form2 = ActivityAdvancedForm(request.POST, instance=activity)
            if form2.is_valid():
                activity = form2.save()

            if activity.duration is not None and activity.distance is not None and activity.speed_avg is None and activity.duration > 0:
                activity.speed_avg = 1000 * activity.distance / activity.duration
                activity.save()

            # Update goals
            try:
                user_data = json.loads(request.user.data)
                activities = Activity.objects.filter(user=request.user, planned=False)
                update_goals_edit_activity(user_data["goals"], activities, request.user)
                request.user.data = json.dumps(user_data)
                request.user.save()
            except:
                pass

            return redirect("activity", activity.id)
    else:
        form1 = ActivityForm(instance=activity)
        form2 = ActivityAdvancedForm(instance=activity)

    equipments = Equipment.objects.filter(user=request.user)
    equipmentsets = EquipmentSet.objects.filter(user=request.user)

    # Liste des équipements et des ensembles d'équipements compatibles pour chaque type d'activité
    activities_equipments = {}
    for activity_type in sports.keys():
        activities_equipments[activity_type] = {"equipments_show": [], "equipments_hide": [], "equipmentsets_show": [], "equipmentsets_hide": []}

    for equipment in equipments.all():
        for activity_type in sports.keys():
            if len(equipments_globals[equipment.type]["sports"]) == 0 or activity_type in equipments_globals[equipment.type]["sports"]:
                activities_equipments[activity_type]["equipments_show"].append(equipment.id)
            else:
                activities_equipments[activity_type]["equipments_hide"].append(equipment.id)

    for equipmentset in equipmentsets.all():
        if equipmentset.type == "All":
            for activity_type in sports.keys():
                activities_equipments[activity_type]["equipmentsets_show"].append(equipmentset.id)
        else:
            for activity_type in sports.keys():
                if activity_type == equipmentset.type:
                    activities_equipments[activity_type]["equipmentsets_show"].append(equipmentset.id)
                else:
                    activities_equipments[activity_type]["equipmentsets_hide"].append(equipmentset.id)

    return render(request, "activities/activity_edit.html", {
        "activity": activity,
        "form1": form1,
        "form2": form2,
        "equipments": equipments,
        "equipmentsets": equipmentsets,
        "activities_equipments": activities_equipments,
    })


@login_required
def view_activity_edit_redirect(request, aid):
    return redirect("activity_edit", aid)


@login_required
def view_activity_remove(request, aid):
    try:
        activity = Activity.objects.get(id=aid, user=request.user)
    except Activity.DoesNotExist:
        return redirect("activities")

    if request.method == "POST" and "delete" in request.POST:
        os.makedirs(os.path.join(BASE_DIR, "data", "tracks"), exist_ok=True)

        # Remove track file
        filename_track = os.path.join(BASE_DIR, "data", "tracks", "%06d" % activity.id)
        if os.path.isfile(filename_track):
            os.remove(filename_track)

        # Update goals
        try:
            user_data = json.loads(request.user.data)
            update_goals_remove_activity(user_data["goals"], activity)
            request.user.data = json.dumps(user_data)
            request.user.save()
        except:
            pass

        activity.delete()
        return redirect("activities")

    return render(request, "activities/activity_remove.html", {"activity": activity})


@login_required
def view_activity_remove_redirect(request, aid):
    return redirect("activity_remove", aid)


@login_required
def view_activity_export(request, aid):
    try:
        activity = Activity.objects.select_related("competition").get(id=aid, user=request.user)
    except Activity.DoesNotExist:
        return redirect("activities")

    response = export_activity_json(activity)
    return JsonResponse(response)


@login_required
def view_activity_export_redirect(request, aid):
    return redirect("activity_export", aid)


@login_required
def view_activity_add_file(request, aid):
    try:
        activity = Activity.objects.get(id=aid, user=request.user)
    except Activity.DoesNotExist:
        return redirect("activities")

    # TODO: check free space on storage

    errors = []

    if request.method == "POST":
        if "file" in request.FILES:
            filesize = request.FILES["file"].size
            if filesize > MAX_UPLOAD_FILESIZE:
                errors.append("File is too big.")
            else:
                os.makedirs(os.path.join(BASE_DIR, "data", "tmp"), exist_ok=True)
                os.makedirs(os.path.join(BASE_DIR, "data", "files"), exist_ok=True)

                filename = request.FILES["file"].name
                filename_tmp = os.path.join(BASE_DIR, "data", "tmp", str(uuid.uuid4()) + ".gz")
                sha1 = hashlib.sha1()
                with gzip.open(filename_tmp, "wb") as dest:
                    for chunk in request.FILES["file"].chunks():
                        dest.write(chunk)
                        sha1.update(chunk)
                filehash = sha1.hexdigest()
                filename_dst = os.path.join(BASE_DIR, "data", "files", filehash + ".gz")

                if os.path.isfile(filename_dst):
                    os.remove(filename_tmp)
                else:
                    shutil.move(filename_tmp, filename_dst)

                compressed_size = os.path.getsize(filename_dst)

                try:
                    ActivityFile.objects.get(activity=activity, filehash=filehash)
                    duplicate = True
                except:
                    duplicate = False

                if duplicate:
                    errors.append("Duplicate file.")
                else:
                    file = ActivityFile.objects.create(activity=activity, filename=filename, filesize=filesize,
                                                       compressed_size=compressed_size, filehash=filehash)
                    return redirect("activity", activity.id)

    return render(request, "activities/activity_add_file.html", {"activity": activity, "errors": errors})


@login_required
def view_activity_add_file_redirect(request, aid):
    return redirect("activity_add_file", aid)


@login_required
def view_activity_track(request, aid):
    try:
        activity = Activity.objects.get(id=aid, user=request.user)
    except Activity.DoesNotExist:
        return redirect("activities")

    os.makedirs(os.path.join(BASE_DIR, "data", "tracks"), exist_ok=True)

    filename_track = os.path.join(BASE_DIR, "data", "tracks", "%06d" % activity.id)
    if os.path.isfile(filename_track):
        activity.trackfile.load_from_file(filename_track)

    return render(request, "activities/activity_track.html", {"activity": activity})


@login_required
def view_activity_track_redirect(request, aid):
    return redirect("activity_track", aid)


@login_required
def view_activity_download_file(request, aid, hash):
    try:
        activity = Activity.objects.get(id=aid, user=request.user)
    except Activity.DoesNotExist:
        return redirect("activities")

    try:
        file = ActivityFile.objects.get(activity=activity, filehash=hash)
    except ActivityFile.DoesNotExist:
        return redirect("activity", activity.id)

    os.makedirs(os.path.join(BASE_DIR, "data", "files"), exist_ok=True)

    filename = os.path.join(BASE_DIR, "data", "files", file.filehash + ".gz")
    if os.path.isfile(filename):
        # FileResponse utilise la taille du fichier compressé, on doit donc passer par un fichier temporaire
        buff = BytesIO()
        with gzip.open(filename, "rb") as f:
            shutil.copyfileobj(f, buff)
        buff.seek(0)

        return FileResponse(buff, as_attachment=True, filename=file.filename)
        #return FileResponse(gzip.open(filename, "rb"), as_attachment=True, filename=file.filename)
    else:
        return HttpResponseGone("File unavailable.")


@login_required
def view_activity_remove_file(request, aid, hash):
    try:
        activity = Activity.objects.get(id=aid, user=request.user)
    except Activity.DoesNotExist:
        return redirect("activities")

    try:
        file = ActivityFile.objects.get(activity=activity, filehash=hash)
    except ActivityFile.DoesNotExist:
        return redirect("activity", activity.id)

    if request.method == "POST" and "delete" in request.POST:
        file.delete()
        return redirect("activity", activity.id)

    return render(request, "activities/activity_remove_file.html", {"activity": activity, "file": file})


def get_activity_from_file(filename:str):
    errors = []
    activities = []
    activity = None

    importers = [ImporterTCX, ImporterFIT, ImporterGPX, ImporterKML, ImporterKMZ]
    for importer in importers:
        try:
            with gzip.open(filename, "rb") as f:
                imp = importer(f, filename)
                if imp.is_valid():
                    activities, errors = imp.load_activities()
                    break
        except:
            pass
    else:
        errors = [_("Can't import file.")]
        return activity, errors

    if len(activities) == 1:
        activity = activities[0]
    elif len(activities) > 1:
        errors.append(_("Multiple activities in this file."))
    else:
        errors.append(_("No activity in this file."))

    return activity, errors


@login_required
def view_activity_recalculate_file(request, aid, hash):
    try:
        activity = Activity.objects.get(id=aid, user=request.user)
    except Activity.DoesNotExist:
        return redirect("activities")

    try:
        file = ActivityFile.objects.get(activity=activity, filehash=hash)
    except ActivityFile.DoesNotExist:
        return redirect("activity", activity.id)

    os.makedirs(os.path.join(BASE_DIR, "data", "files"), exist_ok=True)
    os.makedirs(os.path.join(BASE_DIR, "data", "tracks"), exist_ok=True)

    filename = os.path.join(BASE_DIR, "data", "files", file.filehash + ".gz")
    if not os.path.isfile(filename):
        return redirect("activity", activity.id)

    file_activity, errors = get_activity_from_file(filename)

    try:
        data_new = json.loads(file_activity.data)
    except:
        data_new = {}

    if "segments" not in data_new:
        data_new["segments"] = []

    if file_activity and request.method == "POST":
        filename_track = os.path.join(BASE_DIR, "data", "tracks", "%06d" % activity.id)
        data_updated = False
        records = None

        try:
            data = json.loads(activity.data)
        except:
            data = {}

        if "recalculate_infos" in request.POST:
            if "force_timezone" in request.POST:
                time_shift_delta = timezone.localtime(timezone.now()).utcoffset()
                time_shift = time_shift_delta.days * 24 * 60 + time_shift_delta.seconds // 60
            else:
                time_shift = file_activity.time_shift

            activity.date = file_activity.date
            activity.time_shift = time_shift
            activity.type = file_activity.type
            activity.duration = file_activity.duration
            activity.duration_total = file_activity.duration_total
            activity.distance = file_activity.distance
            activity.heart_rate_avg = file_activity.heart_rate_avg
            activity.heart_rate_max = file_activity.heart_rate_max
            activity.cadence_avg = file_activity.cadence_avg
            activity.cadence_max = file_activity.cadence_max
            activity.power_avg = file_activity.power_avg
            activity.power_max = file_activity.power_max
            activity.speed_avg = file_activity.speed_avg
            activity.speed_max = file_activity.speed_max
            activity.calories = file_activity.calories
            activity.elevation_gain = file_activity.elevation_gain
            activity.elevation_loss = file_activity.elevation_loss
            activity.device = file_activity.device

        if "recalculate_points" in request.POST:
            types = TrackFile.MASK_FORMAT_TIME
            if "recalculate_points_coordinates" in request.POST:
                types |= TrackFile.MASK_FORMAT_LATITUDE
                types |= TrackFile.MASK_FORMAT_LONGITUDE
            if "recalculate_points_altitude" in request.POST:
                types |= TrackFile.MASK_FORMAT_ALTITUDE
                activity.elevation_gain = file_activity.elevation_gain
                activity.elevation_loss = file_activity.elevation_loss
            if "recalculate_points_distance" in request.POST:
                types |= TrackFile.MASK_FORMAT_DISTANCE
            if "recalculate_points_heartrate" in request.POST:
                types |= TrackFile.MASK_FORMAT_HEARTRATE
                activity.heart_rate_avg = file_activity.heart_rate_avg
                activity.heart_rate_max = file_activity.heart_rate_max
            if "recalculate_points_cadence" in request.POST:
                types |= TrackFile.MASK_FORMAT_CADENCE
                activity.cadence_avg = file_activity.cadence_avg
                activity.cadence_max = file_activity.cadence_max
            if "recalculate_points_power" in request.POST:
                types |= TrackFile.MASK_FORMAT_POWER
                activity.power_avg = file_activity.power_avg
                activity.power_max = file_activity.power_max
            if "recalculate_points_speed" in request.POST:
                types |= TrackFile.MASK_FORMAT_SPEED
                types |= TrackFile.MASK_FORMAT_NGP
                activity.speed_avg = file_activity.speed_avg
                activity.speed_max = file_activity.speed_max

            if types == TrackFile.MASK_FORMAT_ALL:
                activity.trackfile = file_activity.trackfile
            elif types != TrackFile.MASK_FORMAT_TIME:
                if not activity.trackfile.loaded:
                    activity.trackfile.load_from_file(filename_track)
                shift_time = activity.date - file_activity.date
                shift_time = shift_time.days * 24*60*60*1000 + shift_time.seconds * 1000 + shift_time.microseconds // 1000
                if shift_time < 0:
                    file_activity.trackfile.shift(-shift_time)
                    file_activity.duration += -shift_time
                    file_activity.duration_total += -shift_time
                else:
                    activity.date = file_activity.date
                    activity.trackfile.shift(shift_time)
                    activity.duration += shift_time
                    activity.duration_total += shift_time
                    # TODO: décaler les segments, pauses, records
                if file_activity.duration > activity.duration:
                    activity.duration = file_activity.duration
                if file_activity.duration_total > activity.duration_total:
                    activity.duration_total = file_activity.duration_total
                activity.trackfile.merge(file_activity.trackfile, types)
            else:
                if not activity.trackfile.loaded:
                    activity.trackfile.load_from_file(filename_track)

            activity.trackfile.points, activity.ngp = compute_ngp(activity.trackfile.points)
            activity.trackfile.save_to_file(filename_track)
            data["heartrate_stats"] = get_heartrate_stats(activity.trackfile.points)
            data["power_stats"] = get_power_stats(activity.trackfile.points)

            if "pauses" in data_new:
                data["pauses"] = data_new["pauses"]  # TODO: pas génial, donc on efface les pauses existantes ?

            data_updated = True

        if "recalculate_records" in request.POST:
            if not activity.trackfile.loaded:
                activity.trackfile.load_from_file(filename_track)
            data["records"] = compute_best_performances(activity.trackfile.points, activity.type)

            # Update threshold and FTP
            if activity.type == "Running":
                distance_1hour = None
                for record in data["records"]:
                    if record["type"] == "duration":
                        if record["duration"] == 60 * 60 * 1000:
                            distance_1hour = record["distance"]
                            break

                if distance_1hour is not None and distance_1hour > 0:
                    threshold = 1000000 * 3600 / distance_1hour
                    if threshold < 1000 * request.user.threshold:
                        Notification.objects.create(user=request.user,
                                                    date=timezone.now(),
                                                    text=_("New estimated threshold: %s. Please update your settings.") % duration_to_string(threshold),
                                                    url=reverse("settings"))
            elif activity.type == "Biking" or activity.type == "HomeTrainer":
                power_1hour = None
                power_20min = None
                for record in data["records"]:
                    if record["type"] == "power":
                        if record["duration"] == 60 * 60 * 1000:
                            power_1hour = int(record["power"])
                            break
                        elif record["duration"] == 20 * 60 * 1000:
                            power_20min = int(record["power"])

                if power_20min is not None and power_1hour is None:
                    power_1hour = int(0.95 * power_20min)

                if power_1hour is not None and power_1hour > request.user.ftp:
                    Notification.objects.create(user=request.user,
                                                date=timezone.now(),
                                                text=_("New estimated FTP: %s&nbsp;W. Please update your settings.") % power_1hour,
                                                url=reverse("settings"))

            # Update user records
            records = update_user_records(request.user)

            data_updated = True

        if "recalculate_segments" in request.POST:
            if "segments" in data:
                data["segments"].extend(data_new["segments"])
                data["segments"] = sorted(data["segments"], key=lambda segment: segment["time_start"])
                data["segments"] = list(OrderedDict((frozenset(item.items()), item) for item in data["segments"]).values())
            else:
                data["segments"] = data_new["segments"]
            data_updated = True

        if "recalculate_score" in request.POST:
            if "heartrate_stats" in data:
                activity.trimp = compute_trimp(data["heartrate_stats"], activity.user)

                if "user" not in data:
                    data["user"] = {}
                data["user"]["heartrate_rest"] = request.user.heartrate_rest
                data["user"]["heartrate_max"] = request.user.heartrate_max
                data_updated = True

            activity.level = compute_level(activity)

            # DEBUG: calcul du rTSS
            """
            intensity = activity.ngp * activity.user.threshold / 1000
            print("IF = %f" % intensity)
            rtss = compute_rtss(activity.trackfile.points, activity.user)
            print("rTSS v1 = %f" % rtss)
            rtss = 100 * activity.duration * intensity**2 / 3600000
            print("rTSS v2 = %f" % rtss)
            """

        if data_updated:
            activity.data = json.dumps(data)

        activity.save()

        # Update goals
        try:
            user_data = json.loads(request.user.data)
            activities = Activity.objects.filter(user=request.user, planned=False)
            update_goals_edit_activity(user_data["goals"], activities, request.user)

            if records is not None:
                user_data["records"] = records

            request.user.data = json.dumps(user_data)
            request.user.save()
        except:
            pass

        return redirect("activity", activity.id)

    return render(request, "activities/activity_recalculate_file.html", {"activity": activity, "file": file, "file_activity": file_activity, "segments": data_new["segments"], "errors": errors})


@login_required
def view_activity_add_segment(request, aid):
    try:
        activity = Activity.objects.get(id=aid, user=request.user)
    except Activity.DoesNotExist:
        return redirect("activities")

    os.makedirs(os.path.join(BASE_DIR, "data", "tracks"), exist_ok=True)

    filename_track = os.path.join(BASE_DIR, "data", "tracks", "%06d" % activity.id)
    if os.path.isfile(filename_track):
        activity.trackfile.load_from_file(filename_track)
    else:
        return redirect("activity", activity.id)

    if request.method == "POST":
        form = SegmentForm(request.POST)
        if form.is_valid():
            segment = activity.trackfile.get_segment(form.cleaned_data["time_start"], form.cleaned_data["time_end"])

            if segment is not None:
                try:
                    data = json.loads(activity.data)
                except:
                    data = {}

                if "segments" in data:
                    if segment not in data["segments"]:
                        segment["description"] = form.cleaned_data["description"]
                        data["segments"].append(segment)
                        data["segments"] = sorted(data["segments"], key=lambda segment: segment["time_start"])
                else:
                    data["segments"] = [segment]

                activity.data = json.dumps(data)
                activity.save()

            return redirect("activity", activity.id)
    else:
        form = SegmentForm()

    return render(request, "activities/activity_add_segment.html", {"activity": activity, "form": form})


@login_required
def view_activity_add_segments(request, aid):
    try:
        activity = Activity.objects.get(id=aid, user=request.user)
    except Activity.DoesNotExist:
        return redirect("activities")

    os.makedirs(os.path.join(BASE_DIR, "data", "tracks"), exist_ok=True)

    filename_track = os.path.join(BASE_DIR, "data", "tracks", "%06d" % activity.id)
    if os.path.isfile(filename_track):
        activity.trackfile.load_from_file(filename_track)
    else:
        return redirect("activity", activity.id)

    if request.method == "POST":
        form = MultiSegmentForm(request.POST)
        if form.is_valid():
            try:
                data = json.loads(activity.data)
            except:
                data = {}

            start_time = form.cleaned_data["time_start"]
            repetitions = form.cleaned_data["repetitions"]
            duration1 = form.cleaned_data["duration1"]
            duration2 = form.cleaned_data["duration2"]

            if "segments" not in data:
                data["segments"] = []

            for i in range(repetitions):
                segment = activity.trackfile.get_segment(start_time + i * (duration1 + duration2), start_time + i * (duration1 + duration2) + duration1)
                if segment is not None and segment not in data["segments"]:
                    data["segments"].append(segment)

            data["segments"] = sorted(data["segments"], key=lambda segment: segment["time_start"])

            activity.data = json.dumps(data)
            activity.save()

            return redirect("activity", activity.id)
    else:
        form = MultiSegmentForm()

    return render(request, "activities/activity_add_segments.html", {"activity": activity, "form": form})


@login_required
def view_activity_edit_segment(request, aid):
    try:
        activity = Activity.objects.get(id=aid, user=request.user)
    except Activity.DoesNotExist:
        return redirect("activities")

    try:
        time_start = int(request.GET["start"])
        time_end = int(request.GET["end"])
    except:
        return redirect("activity", activity.id)

    try:
        data = json.loads(activity.data)
    except:
        data = {}

    if "segments" not in data:
        data["segments"] = []

    segment = None
    for s in data["segments"]:
        if s["time_start"] == time_start and s["time_end"] == time_end:
            segment = s
            break

    if segment is None:
        return redirect("activity", activity.id)

    if request.method == "POST":
        form = SegmentEditForm(request.POST)
        if form.is_valid():
            segment["description"] = form.cleaned_data["description"]

            if activity.type == "Swimming":
                segment["distance"] = form.cleaned_data["distance"]
                if segment["duration"] > 0:
                    segment["speed"] = 1000 * segment["distance"] / segment["duration"]

            activity.data = json.dumps(data)
            activity.save()
            return redirect("activity", activity.id)
    else:
        form = SegmentEditForm(initial={
            "time_start": duration_to_string(segment["time_start"]),
            "time_end": duration_to_string(segment["time_end"]),
            "distance": int(segment["distance"]) if segment["distance"] is not None else 0,
            "description": segment["description"] if "description" in segment else "",
        })

    if activity.type != "Swimming":
        form.fields.get("distance").disabled = True

    return render(request, "activities/activity_edit_segment.html", {"activity": activity, "segment": segment, "form": form})


@login_required
def view_activity_remove_segment(request, aid):
    try:
        activity = Activity.objects.get(id=aid, user=request.user)
    except Activity.DoesNotExist:
        return redirect("activities")

    try:
        time_start = int(request.GET["start"])
        time_end = int(request.GET["end"])
    except:
        return redirect("activity", activity.id)

    try:
        data = json.loads(activity.data)
    except:
        data = {}

    if "segments" not in data:
        data["segments"] = []

    segment = None
    for s in data["segments"]:
        if s["time_start"] == time_start and s["time_end"] == time_end:
            segment = s
            break

    if segment is None:
        return redirect("activity", activity.id)

    if request.method == "POST" and "delete" in request.POST:
        data["segments"].remove(segment)
        activity.data = json.dumps(data)
        activity.save()
        return redirect("activity", activity.id)

    return render(request, "activities/activity_remove_segment.html", {"activity": activity, "segment": segment})


@login_required
def view_activity_edit_records(request, aid):
    try:
        activity = Activity.objects.get(id=aid, user=request.user)
    except Activity.DoesNotExist:
        return redirect("activities")

    if activity.type != "Swimming":
        return redirect("activity", activity.id)

    try:
        data = json.loads(activity.data)
    except:
        data = {}

    records = {}
    if "records" in data:
        for record in data["records"]:
            if record["type"] != "distance":
                continue
            records[record["distance"]] = record["time"]

    for distance in record_distances_swimming:
        if distance not in records and distance <= activity.distance:
            records[distance] = None

    records = dict(sorted(records.items()))

    if request.method == "POST":
        for distance in records.keys():
            key = "record_%d" % distance
            if key in request.POST:
                value = request.POST.get(key)
                if value:
                    records[distance] = convert_duration_string(request.POST.get(key))
                else:
                    records[distance] = None

        new_records = []

        if "records" in data:
            # Supprimer les anciens records de distance
            for record in data["records"]:
                if record["type"] != "distance":
                    new_records.append(record)

        # Ajoute les nouveaux records de distance définis
        for distance, duration in records.items():
            if duration:
                new_records.append({
                    "type": "distance",
                    "distance": distance,
                    "time": duration,
                })

        data["records"] = new_records
        activity.data = json.dumps(data)
        activity.save()

        # Update user records
        user_data = json.loads(request.user.data)
        user_data["records"] = update_user_records(request.user)
        request.user.data = json.dumps(user_data)
        request.user.save()

        return redirect("activity", activity.id)

    return render(request, "activities/activity_edit_records.html", {"activity": activity, "records": records})


@login_required
def view_activity_edit_records_redirect(request, aid):
    return redirect("activity_edit_records", aid)


@login_required
def view_note_create(request):
    if request.method == "POST":
        form = NoteForm(request.POST)

        if form.is_valid():
            note = form.save(commit=False)
            note.user = request.user
            note.save()

            return redirect("calendar")
    else:
        form = NoteForm()

    return render(request, "activities/note_create.html", {"form": form})


@login_required
def view_note_edit(request, id):
    try:
        note = Note.objects.get(id=id, user=request.user)
    except Note.DoesNotExist:
        return redirect("calendar")

    if request.method == "POST":
        form = NoteForm(request.POST, instance=note)

        if form.is_valid():
            note = form.save()
            return redirect("calendar")
    else:
        form = NoteForm(instance=note)

    return render(request, "activities/note_edit.html", {"form": form, "note": note})


@login_required
def view_note_remove(request, id):
    try:
        note = Note.objects.get(id=id, user=request.user)
    except Note.DoesNotExist:
        return redirect("calendar")

    note.delete()

    return redirect("calendar")
