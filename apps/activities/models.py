import datetime
import json
import os

from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils.timezone import make_naive
from django.utils.translation import gettext as _

from Magni.settings import BASE_DIR
from apps.base.globals import sports
from apps.activities.trackfile import TrackFile
from apps.competitions.models import Competition
from apps.equipments.models import Equipment
from apps.user.models import User


class Activity(models.Model):
    date = models.DateTimeField(_("date"))
    name = models.CharField(_("name"), max_length=100, blank=True, null=True)
    time_shift = models.SmallIntegerField(_("time shift"), validators=[MinValueValidator(-12*60), MaxValueValidator(12*60)], default=0)
    type = models.CharField(_("activity type"), max_length=16, choices=[(a, b["name"]) for a, b in sports.items()], default="Other")
    planned = models.BooleanField(_("planned activity"), default=False)
    duration = models.PositiveIntegerField(_("duration"), blank=True, null=True)
    duration_total = models.PositiveIntegerField(_("total duration"), blank=True, null=True)
    distance = models.PositiveIntegerField(_("distance"), blank=True, null=True)
    notes = models.TextField(_("notes"), max_length=1000, blank=True)
    user = models.ForeignKey(User, verbose_name=_("user"), related_name="activities", on_delete=models.CASCADE)
    device = models.CharField(_("device"), max_length=50, blank=True)
    heart_rate_avg = models.PositiveSmallIntegerField(_("average heart rate"), validators=[MaxValueValidator(220)], blank=True, null=True)
    heart_rate_max = models.PositiveSmallIntegerField(_("maximum heart rate"), validators=[MaxValueValidator(220)], blank=True, null=True)
    cadence_avg = models.PositiveSmallIntegerField(_("average cadence"), blank=True, null=True)
    cadence_max = models.PositiveSmallIntegerField(_("maximum cadence"), blank=True, null=True)
    power_avg = models.PositiveSmallIntegerField(_("average power"), blank=True, null=True)
    power_max = models.PositiveSmallIntegerField(_("maximum power"), blank=True, null=True)
    speed_avg = models.FloatField(_("average speed"), blank=True, null=True)
    speed_max = models.FloatField(_("maximum speed"), blank=True, null=True)
    calories = models.PositiveSmallIntegerField(_("calories"), blank=True, null=True)
    elevation_gain = models.PositiveIntegerField(_("elevation gain"), blank=True, null=True)
    elevation_loss = models.PositiveIntegerField(_("elevation loss"), blank=True, null=True)
    trimp = models.PositiveSmallIntegerField(_("TRIMP"), blank=True, null=True)
    level = models.PositiveSmallIntegerField(_("level"), blank=True, null=True)
    tss = models.PositiveSmallIntegerField(_("TSS"), blank=True, null=True)
    ngp = models.FloatField(_("NGP"), blank=True, null=True)
    feeling = models.PositiveSmallIntegerField(_("feeling"), default=0, validators=[MaxValueValidator(5)])
    exertion = models.PositiveSmallIntegerField(_("exertion"), default=0, validators=[MaxValueValidator(10)])
    equipments = models.ManyToManyField(Equipment, verbose_name=_("equipments"), related_name="activities", blank=True)
    competition = models.ForeignKey(Competition, verbose_name=_("competition"), related_name="activities", blank=True, null=True, on_delete=models.SET_NULL)
    data = models.TextField(_("data"), blank=True, null=True)  # format JSON

    # TODO: links

    def __init__(self, *args, **kwargs):
        self.trackfile = TrackFile()
        super(Activity, self).__init__(*args, **kwargs)

    def __str__(self):
        return "%s (%s) by %s" % (str(self.date), self.type, self.user)

    @property
    def date_local(self):
        return self.date.replace(tzinfo=None) + datetime.timedelta(seconds=self.time_shift * 60)

    @property
    def date_user(self):
        return make_naive(self.date)

    class Meta:
        verbose_name = _("Activity")
        verbose_name_plural = _("Activities")
        ordering = ["-date"]


def export_activity_json(activity):
    response = {
        "date": activity.date,
        "type": activity.type,
        "files": [],
    }

    if activity.duration is not None:
        response["duration"] = activity.duration
    if activity.duration_total is not None:
        response["duration_total"] = activity.duration_total
    if activity.distance is not None:
        response["distance"] = activity.distance
    if activity.notes:
        response["notes"] = activity.notes
    if activity.device is not None:
        response["device"] = activity.device
    if activity.trimp is not None:
        response["trimp"] = activity.trimp
    if activity.ngp is not None:
        response["ngp"] = activity.ngp
    if activity.speed_avg is not None:
        response["speed_avg"] = activity.speed_avg
    if activity.speed_max is not None:
        response["speed_max"] = activity.speed_max
    if activity.cadence_avg is not None:
        response["cadence_avg"] = activity.cadence_avg
    if activity.cadence_max is not None:
        response["cadence_max"] = activity.cadence_max
    if activity.power_avg is not None:
        response["power_avg"] = activity.power_avg
    if activity.power_max is not None:
        response["power_max"] = activity.power_max
    if activity.elevation_gain is not None:
        response["elevation_gain"] = activity.elevation_gain
    if activity.elevation_loss is not None:
        response["elevation_loss"] = activity.elevation_loss
    if activity.heart_rate_avg is not None:
        response["heart_rate_avg"] = activity.heart_rate_avg
    if activity.heart_rate_max is not None:
        response["heart_rate_max"] = activity.heart_rate_max
    if activity.calories is not None:
        response["calories"] = activity.calories
    if activity.feeling:
        response["feeling"] = activity.feeling
    if activity.exertion:
        response["exertion"] = activity.exertion

    if activity.data:
        try:
            data = json.loads(activity.data)
        except:
            data = {}

        if "records" in data and data["records"]:
            response["records"] = data["records"]
        if "pauses" in data and data["pauses"]:
            response["pauses"] = data["pauses"]
        if "heartrate_stats" in data and data["heartrate_stats"]:
            response["heartrate_stats"] = data["heartrate_stats"]
        if "power_stats" in data and data["power_stats"]:
            response["power_stats"] = data["power_stats"]
        if "segments" in data and data["segments"]:
            response["segments"] = data["segments"]

    response["equipments"] = []
    for equipment in activity.equipments.all():
        response["equipments"].append(str(equipment))

    if activity.competition:
        response["competition"] = activity.competition.name

    # Files
    for file in activity.files.all():
        response["files"].append({
            "name": file.filename,
            "sha1": file.filehash,
            "size": file.filesize,
            "compressed_size": file.compressed_size,
        })

    return response


class ActivityFile(models.Model):
    activity = models.ForeignKey(Activity, verbose_name=_("activity"), related_name="files", on_delete=models.CASCADE)
    filename = models.CharField(_("file name"), max_length=100)
    filehash = models.CharField(_("file hash"), max_length=40)
    filesize = models.PositiveIntegerField(_("file size"), default=0)
    compressed_size = models.PositiveIntegerField(_("compressed size"), default=0)
    # TODO: ajouter le format du fichier (TCX, FIT, etc.)
    # TODO: ajouter les types de données contenus dans le fichier (fréquence cardiaque, puissance, etc.)

    def __str__(self):
        return "%s \"%s\" (activity %s)" % (self.filehash, self.filename, self.activity)

    @property
    def downloadable(self):
        # Check if file is available in storage
        return os.path.isfile(self.get_path)

    @property
    def get_path(self):
        return os.path.join(BASE_DIR, "data", "files", str(self.filehash) + ".gz")

    class Meta:
        verbose_name = _("Activity file")
        verbose_name_plural = _("Activity files")
        unique_together = ("activity", "filehash")
        ordering = ["filename"]


class Note(models.Model):
    date_start = models.DateTimeField(_("start date"))
    date_end = models.DateTimeField(_("end date"), blank=True, null=True)
    title = models.CharField(_("title"), max_length=100)
    content = models.TextField(_("content"), max_length=1000, blank=True)
    user = models.ForeignKey(User, verbose_name=_("user"), related_name="notes", on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Note")
        verbose_name_plural = _("Notes")
        ordering = ["date_start", "date_end"]
