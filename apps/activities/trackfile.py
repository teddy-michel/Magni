
import struct


def _create_point():
    return {
        "time": None,
        "latitude": None,
        "longitude": None,
        "altitude": None,
        "distance": None,
        "heartrate": None,
        "cadence": None,
        "power": None,
        "speed": None,
        "ngp": None,
    }


class TrackFile(object):
    MASK_FORMAT_TIME = 1 << 0
    MASK_FORMAT_LATITUDE = 1 << 1
    MASK_FORMAT_LONGITUDE = 1 << 2
    MASK_FORMAT_ALTITUDE = 1 << 3
    MASK_FORMAT_DISTANCE = 1 << 4
    MASK_FORMAT_HEARTRATE = 1 << 5
    MASK_FORMAT_CADENCE = 1 << 6
    MASK_FORMAT_POWER = 1 << 7
    MASK_FORMAT_SPEED = 1 << 8
    MASK_FORMAT_NGP = 1 << 9
    MASK_FORMAT_ALL = 0b1111111111

    def __init__(self):
        self._points = []
        self._loaded = False
        self._need_sort = False

        self._has_time = False
        self._has_latitude = False
        self._has_longitude = False
        self._has_altitude = False
        self._has_distance = False
        self._has_heartrate = False
        self._has_cadence = False
        self._has_power = False
        self._has_speed = False
        self._has_ngp = False

    def reset(self):
        self._points = []
        self._loaded = False
        self._need_sort = False

        self._has_time = False
        self._has_latitude = False
        self._has_longitude = False
        self._has_altitude = False
        self._has_distance = False
        self._has_heartrate = False
        self._has_cadence = False
        self._has_power = False
        self._has_speed = False
        self._has_ngp = False

    @property
    def points(self) -> list:
        if self._need_sort:
            self._points = sorted(self._points, key=lambda point: point["time"])
            self._need_sort = False
        return self._points

    @property
    def loaded(self) -> bool:
        return self._loaded

    @points.setter
    def points(self, p:list):
        self.reset()

        for point in p:
            if not isinstance(point, dict):
                continue
            new_point = _create_point()
            if "time" in point:
                try:
                    new_point["time"] = int(point["time"])
                    self._has_time = True
                except:
                    pass
            if "latitude" in point:
                try:
                    new_point["latitude"] = float(point["latitude"])
                    self._has_latitude = True
                except:
                    pass
            if "longitude" in point:
                try:
                    new_point["longitude"] = float(point["longitude"])
                    self._has_longitude = True
                except:
                    pass
            if "altitude" in point:
                try:
                    new_point["altitude"] = float(point["altitude"])
                    self._has_altitude = True
                except:
                    pass
            if "distance" in point:
                try:
                    new_point["distance"] = float(point["distance"])
                    self._has_distance = True
                except:
                    pass
            if "heartrate" in point:
                try:
                    new_point["heartrate"] = int(point["heartrate"])
                    self._has_heartrate = True
                except:
                    pass
            if "cadence" in point:
                try:
                    new_point["cadence"] = int(point["cadence"])
                    self._has_cadence = True
                except:
                    pass
            if "power" in point:
                try:
                    new_point["power"] = int(point["power"])
                    self._has_power = True
                except:
                    pass
            if "speed" in point:
                try:
                    new_point["speed"] = float(point["speed"])
                    self._has_speed = True
                except:
                    pass
            if "ngp" in point:
                try:
                    new_point["ngp"] = float(point["ngp"])
                    self._has_ngp = True
                except:
                    pass
            self._points.append(new_point)

        self._need_sort = True
        self._loaded = True

    def add_point(self, time=None, latitude=None, longitude=None, altitude=None, distance=None, heartrate=None, cadence=None, power=None, speed=None, ngp=None):
        new_point = _create_point()
        if time is not None:
            new_point["time"] = time
            self._has_time = True
        if latitude is not None:
            new_point["latitude"] = latitude
            self._has_latitude = True
        if longitude is not None:
            new_point["longitude"] = longitude
            self._has_longitude = True
        if altitude is not None:
            new_point["altitude"] = altitude
            self._has_altitude = True
        if distance is not None:
            new_point["distance"] = distance
            self._has_distance = True
        if heartrate is not None:
            new_point["heartrate"] = heartrate
            self._has_heartrate = True
        if cadence is not None:
            new_point["cadence"] = cadence
            self._has_cadence = True
        if power is not None:
            new_point["power"] = power
            self._has_power = True
        if speed is not None:
            new_point["speed"] = speed
            self._has_speed = True
        if ngp is not None:
            new_point["ngp"] = ngp
            self._has_ngp = True
        self._points.append(new_point)
        self._need_sort = True
        self._loaded = True

    @property
    def has_time(self) -> bool:
        return self._has_time

    @property
    def has_latitude(self) -> bool:
        return self._has_latitude

    @property
    def has_longitude(self) -> bool:
        return self._has_longitude

    @property
    def has_altitude(self) -> bool:
        return self._has_altitude

    @property
    def has_distance(self) -> bool:
        return self._has_distance

    @property
    def has_heartrate(self) -> bool:
        return self._has_heartrate

    @property
    def has_cadence(self) -> bool:
        return self._has_cadence

    @property
    def has_power(self) -> bool:
        return self._has_power

    @property
    def has_speed(self) -> bool:
        return self._has_speed

    @property
    def has_ngp(self) -> bool:
        return self._has_ngp

    def search_pauses(self) -> list:
        if self._need_sort:
            self._points = sorted(self._points, key=lambda point: point["time"])
            self._need_sort = False

        pauses = []

        previous_time = None
        previous_distance = None
        for point in self._points:
            if previous_time is not None and previous_distance is not None and previous_distance > 0.0 and point["time"] is not None and point["distance"] is not None:
                duration = point["time"] - previous_time
                if duration >= 30 * 1000:
                    distance = point["distance"] - previous_distance
                    if distance <= 10:
                        # Si 2 points sont séparés de plus de 30 secondes et que la distance n'a pas bougée de plus de 10 mètres, on considère que c'est une pause
                        pauses.append({"time_start": previous_time, "time_end": point["time"], "duration": duration})

            if point["time"] is not None:
                previous_time = point["time"]
            if point["distance"] is not None:
                previous_distance = point["distance"]

        return pauses

    def get_segment(self, time_start:int, time_end:int) -> dict:
        """ Calcule les informations d'un segment. """
        if time_end <= time_start:
            return None

        if not self._has_time or not self._has_distance:
            return None

        if self._need_sort:
            self._points = sorted(self._points, key=lambda point: point["time"])
            self._need_sort = False

        segment = {
            "time_start": time_start,
            "time_end": time_end,
            "duration": time_end - time_start,
            "distance": None,
            "point_start": None,
            "point_end": None,
        }

        point_start_a = point_start_b = None

        for point in range(len(self._points)):
            if self._points[point]["time"] is None or self._points[point]["distance"] is None:
                continue
            if point_start_a is None:
                if self._points[point]["time"] >= time_start:
                    if point == 0 or self._points[point]["time"] == time_start:
                        point_start_a = point
                        point_start_b = point
                    else:
                        point_start_a = point - 1
                        point_start_b = point
            elif self._points[point]["time"] >= time_end:
                distance_a = self._points[point_start_a]["distance"]
                distance_b = self._points[point_start_b]["distance"]
                time_a = self._points[point_start_a]["time"]
                time_b = self._points[point_start_b]["time"]
                if time_a == time_b:
                    distance_start = distance_b
                else:
                    distance_start = distance_a + (distance_b - distance_a) * (time_start - time_a) / (time_b - time_a)

                distance_a = self._points[point - 1]["distance"]
                distance_b = self._points[point]["distance"]
                time_a = self._points[point - 1]["time"]
                time_b = self._points[point]["time"]
                if time_a == time_b or time_b == time_end:
                    distance_end = distance_b
                else:
                    distance_end = distance_a + (distance_b - distance_a) * (time_end - time_a) / (time_b - time_a)

                segment["point_start"] = point_start_a
                segment["point_end"] = point
                segment["distance"] = distance_end - distance_start
                segment["speed"] = 1000 * segment["distance"] / segment["duration"]
                break

        if segment["distance"] is None or segment["point_start"] is None or segment["point_end"] is None:
            return None

        return segment

    def shift(self, time:int):
        """ Décale le temps de chaque point de la durée time en ms. """
        if not self.has_time:
            return

        for point in self._points:
            try:
                point["time"] += time
            except:
                pass

    def _search_point(self, time:int):
        prev_point = None
        pts = self.points

        for i in range(len(pts)):
            if pts[i]["time"] >= time:
                if prev_point is None:
                    if pts[i]["time"] - time <= 100:
                        return pts[i]  # First point
                    else:
                        # Before first point
                        new_point = _create_point()
                        new_point["time"] = time
                        self._points.insert(0, new_point)
                        return new_point
                else:
                    if time - prev_point["time"] <= pts[i]["time"] - time:
                        if time - prev_point["time"] <= 100:
                            return prev_point  # Previous points
                    else:
                        if pts[i]["time"] - time <= 100:
                            return pts[i]  # Next points
                    # Between the two points
                    new_point = _create_point()
                    new_point["time"] = time
                    self._points.insert(i, new_point)
                    return new_point
            prev_point = pts[i]

        if prev_point is not None and time - prev_point["time"] <= 100:
            return prev_point  # Last point

        # After last point
        new_point = _create_point()
        new_point["time"] = time
        self._points.append(new_point)
        return new_point

    def merge(self, other, types):
        if not self.has_time or not other.has_time:
            return

        types &= ~self.MASK_FORMAT_TIME

        if (types & self.MASK_FORMAT_LATITUDE) != 0 and not other.has_latitude:
            types &= ~self.MASK_FORMAT_LATITUDE
        if (types & self.MASK_FORMAT_LONGITUDE) != 0 and not other.has_longitude:
            types &= ~self.MASK_FORMAT_LONGITUDE
        if (types & self.MASK_FORMAT_ALTITUDE) != 0 and not other.has_altitude:
            types &= ~self.MASK_FORMAT_ALTITUDE
        if (types & self.MASK_FORMAT_DISTANCE) != 0 and not other.has_distance:
            types &= ~self.MASK_FORMAT_DISTANCE
        if (types & self.MASK_FORMAT_HEARTRATE) != 0 and not other.has_heartrate:
            types &= ~self.MASK_FORMAT_HEARTRATE
        if (types & self.MASK_FORMAT_CADENCE) != 0 and not other.has_cadence:
            types &= ~self.MASK_FORMAT_CADENCE
        if (types & self.MASK_FORMAT_POWER) != 0 and not other.has_power:
            types &= ~self.MASK_FORMAT_POWER
        if (types & self.MASK_FORMAT_SPEED) != 0 and not other.has_speed:
            types &= ~self.MASK_FORMAT_SPEED
        if (types & self.MASK_FORMAT_NGP) != 0 and not other.has_ngp:
            types &= ~self.MASK_FORMAT_NGP

        # Aucun type de données à utiliser
        if (types & self.MASK_FORMAT_ALL) == 0:
            return

        # Aucun point à fusionner
        if len(other.points) == 0 or len(self.points) == 0:
            return

        for p in other.points:
            point = self._search_point(p["time"])
            if (types & self.MASK_FORMAT_LATITUDE) != 0:
                point["latitude"] = p["latitude"]
            if (types & self.MASK_FORMAT_LONGITUDE) != 0:
                point["longitude"] = p["longitude"]
            if (types & self.MASK_FORMAT_ALTITUDE) != 0:
                point["altitude"] = p["altitude"]
            if (types & self.MASK_FORMAT_DISTANCE) != 0:
                point["distance"] = p["distance"]
            if (types & self.MASK_FORMAT_HEARTRATE) != 0:
                point["heartrate"] = p["heartrate"]
            if (types & self.MASK_FORMAT_CADENCE) != 0:
                point["cadence"] = p["cadence"]
            if (types & self.MASK_FORMAT_POWER) != 0:
                point["power"] = p["power"]
            if (types & self.MASK_FORMAT_SPEED) != 0:
                point["speed"] = p["speed"]
            if (types & self.MASK_FORMAT_NGP) != 0:
                point["ngp"] = p["ngp"]

        self._loaded = True

    def load_from_file(self, filename:str) -> bool:
        self.reset()

        try:
            with open(filename, "rb") as f:
                num_points, point_format = struct.unpack("<II", f.read(8))
                if num_points == 0:
                    return True

                # Format des points
                self._has_time = bool(point_format & self.MASK_FORMAT_TIME)
                self._has_latitude = bool(point_format & self.MASK_FORMAT_LATITUDE)
                self._has_longitude = bool(point_format & self.MASK_FORMAT_LONGITUDE)
                self._has_altitude = bool(point_format & self.MASK_FORMAT_ALTITUDE)
                self._has_distance = bool(point_format & self.MASK_FORMAT_DISTANCE)
                self._has_heartrate = bool(point_format & self.MASK_FORMAT_HEARTRATE)
                self._has_cadence = bool(point_format & self.MASK_FORMAT_CADENCE)
                self._has_power = bool(point_format & self.MASK_FORMAT_POWER)
                self._has_speed = bool(point_format & self.MASK_FORMAT_SPEED)
                self._has_ngp = bool(point_format & self.MASK_FORMAT_NGP)

                for i in range(num_points):
                    point = _create_point()
                    if self._has_time:
                        point["time"] = struct.unpack("<I", f.read(4))[0]
                    if self._has_latitude:
                        point["latitude"] = struct.unpack("<d", f.read(8))[0]
                    if self._has_longitude:
                        point["longitude"] = struct.unpack("<d", f.read(8))[0]
                    if self._has_altitude:
                        point["altitude"] = struct.unpack("<d", f.read(8))[0]
                    if self._has_distance:
                        point["distance"] = struct.unpack("<f", f.read(4))[0]
                    if self._has_heartrate:
                        point["heartrate"] = struct.unpack("<B", f.read(1))[0]
                        if point["heartrate"] == 0:
                            point["heartrate"] = None
                    if self._has_cadence:
                        point["cadence"] = struct.unpack("<H", f.read(2))[0]
                    if self._has_power:
                        point["power"] = struct.unpack("<I", f.read(4))[0]
                        if point["power"] == 0:
                            point["power"] = None
                    if self._has_speed:
                        point["speed"] = struct.unpack("<f", f.read(4))[0]
                    if self._has_ngp:
                        point["ngp"] = struct.unpack("<f", f.read(4))[0]
                    self._points.append(point)

                self._loaded = True
                return True
        except:
            return False

    def save_to_file(self, filename:str) -> bool:
        num_points = len(self._points)

        if self._need_sort:
            self._points = sorted(self._points, key=lambda point: point["time"])
            self._need_sort = False

        # Format des points
        point_format = 0
        if self._has_time:
            point_format |= self.MASK_FORMAT_TIME
        if self._has_latitude:
            point_format |= self.MASK_FORMAT_LATITUDE
        if self._has_longitude:
            point_format |= self.MASK_FORMAT_LONGITUDE
        if self._has_altitude:
            point_format |= self.MASK_FORMAT_ALTITUDE
        if self._has_distance:
            point_format |= self.MASK_FORMAT_DISTANCE
        if self._has_heartrate:
            point_format |= self.MASK_FORMAT_HEARTRATE
        if self._has_cadence:
            point_format |= self.MASK_FORMAT_CADENCE
        if self._has_power:
            point_format |= self.MASK_FORMAT_POWER
        if self._has_speed:
            point_format |= self.MASK_FORMAT_SPEED
        if self._has_ngp:
            point_format |= self.MASK_FORMAT_NGP

        try:
            with open(filename, "wb") as f:
                f.write(struct.pack("<I", num_points))
                f.write(struct.pack("<I", point_format))

                for point in self._points:
                    if self._has_time:
                        f.write(struct.pack("<I", point["time"] or 0))
                    if self._has_latitude:
                        f.write(struct.pack("<d", point["latitude"] or 0.0))
                    if self._has_longitude:
                        f.write(struct.pack("<d", point["longitude"] or 0.0))
                    if self._has_altitude:
                        f.write(struct.pack("<d", point["altitude"] or 0.0))
                    if self._has_distance:
                        f.write(struct.pack("<f", point["distance"] or 0.0))
                    if self._has_heartrate:
                        f.write(struct.pack("<B", point["heartrate"] or 0))
                    if self._has_cadence:
                        f.write(struct.pack("<H", point["cadence"] or 0))
                    if self._has_power:
                        f.write(struct.pack("<I", point["power"] or 0))
                    if self._has_speed:
                        f.write(struct.pack("<f", point["speed"] or 0.0))
                    if self._has_ngp:
                        f.write(struct.pack("<f", point["ngp"] or 0.0))
                return True
        except:
            return False
