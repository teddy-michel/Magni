# Generated by Django 2.2.7 on 2020-05-03 14:25

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('equipments', '0001_initial'),
        ('activities', '0002_activity_competition'),
    ]

    operations = [
        migrations.AddField(
            model_name='activity',
            name='equipments',
            field=models.ManyToManyField(blank=True, related_name='activities', to='equipments.Equipment', verbose_name='équipements'),
        ),
        migrations.AddField(
            model_name='activity',
            name='shoes',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='activities', to='equipments.Shoes', verbose_name='chaussures'),
        ),
    ]
