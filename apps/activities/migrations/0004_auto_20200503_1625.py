# Generated by Django 2.2.7 on 2020-05-03 14:25

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('activities', '0003_auto_20200503_1625'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.AddField(
            model_name='activity',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='activities', to=settings.AUTH_USER_MODEL, verbose_name='utilisateur'),
        ),
        migrations.AlterUniqueTogether(
            name='activityfile',
            unique_together={('activity', 'filehash')},
        ),
    ]
