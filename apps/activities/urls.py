from django.urls import path

from apps.activities import views


urlpatterns = [
    path("", views.view_activities, name="activities"),
    path("create", views.view_activity_create, name="create_activity"),
    path("import", views.view_activity_import, name="import_activity"),
    path("files", views.view_files, name="files"),
    path("map", views.view_map, name="map"),
    path("records", views.view_records, name="records"),
    path("records/evolution", views.view_record_evolution, name="record_evolution"),
    path("threshold", views.view_threshold, name="threshold"),
    path("progression", views.view_progression, name="progression"),
    path("calendar", views.view_calendar, name="calendar"),

    # Notes
    path("add_note", views.view_note_create, name="add_note"),
    path("edit_note-<id:id>", views.view_note_edit, name="edit_note"),
    path("remove_note-<id:id>", views.view_note_remove, name="remove_note"),
    path("notes", views.view_notes, name="notes"),

    path("stats", views.view_stats, name="stats"),
    path("stats/json", views.view_api_stats, name="api_stats"),

    path("json", views.view_api_activities, name="api_activities"),

    # Activity views
    path("<id:aid>", views.view_activity, name="activity"),
    path("<int:aid>", views.view_activity_redirect),
    path("<id:aid>/edit", views.view_activity_edit, name="activity_edit"),
    path("<int:aid>/edit", views.view_activity_edit_redirect),
    path("<id:aid>/remove", views.view_activity_remove, name="activity_remove"),
    path("<int:aid>/remove", views.view_activity_remove_redirect),
    path("<id:aid>/export", views.view_activity_export, name="activity_export"),
    path("<int:aid>/export", views.view_activity_export_redirect),
    path("<id:aid>/add_file", views.view_activity_add_file, name="activity_add_file"),
    path("<int:aid>/add_file", views.view_activity_add_file_redirect),
    path("<id:aid>/track", views.view_activity_track, name="activity_track"),
    path("<int:aid>/track", views.view_activity_track_redirect),
    path("<id:aid>/edit_records", views.view_activity_edit_records, name="activity_edit_records"),
    path("<int:aid>/edit_records", views.view_activity_edit_records_redirect),

    # Files
    path("<id:aid>/file/<slug:hash>/download", views.view_activity_download_file, name="activity_download_file"),
    path("<id:aid>/file/<slug:hash>/remove", views.view_activity_remove_file, name="activity_remove_file"),
    path("<id:aid>/file/<slug:hash>/recalculate", views.view_activity_recalculate_file, name="activity_recalculate_file"),

    # Segments
    path("<id:aid>/add_segment", views.view_activity_add_segment, name="activity_add_segment"),
    path("<id:aid>/add_segments", views.view_activity_add_segments, name="activity_add_segments"),
    path("<id:aid>/edit_segment", views.view_activity_edit_segment, name="activity_edit_segment"),
    path("<id:aid>/remove_segment", views.view_activity_remove_segment, name="activity_remove_segment"),
]
