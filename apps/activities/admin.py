from django.contrib import admin

from apps.activities.models import Activity, ActivityFile


@admin.register(Activity)
class ActivityAdmin(admin.ModelAdmin):
    list_display = ("date", "type", "duration", "distance", "user")


@admin.register(ActivityFile)
class ActivityFileAdmin(admin.ModelAdmin):
    list_display = ("activity", "filename", "filehash", "filesize")
