
import datetime
import copy
from collections import Counter

from django.db.models.functions import ExtractYear, ExtractMonth, ExtractWeek, ExtractDay, TruncDate
from django.utils import timezone

from apps.activities.models import Activity


class DataSource(object):

    @staticmethod
    def compute_week(date):
        """ Retourne le numéro de la semaine à utiliser pour l'année courante (entre 0 et 54),
            et un tuple(année, semaine) si la semaine courante est aussi sur une autre année.
        """
        week = date.isocalendar().week
        other_week = None

        if week >= 52:
            # Si on est dans la première semaine de l'année et que cette semaine fait partie de l'année précédente
            if date.month == 1:
                other_week = (date.year - 1, week)
                week = 0  # on considère qu'on est dans la semaine 0
            # Si on est dans la dernière semaine de l'année qui continue au début de l'année suivante
            elif datetime.datetime(date.year + 1, 1, 1).isocalendar().week == week:
                # On reporte le résultat sur la première semaine de l'année suivante
                other_week = (date.year + 1, 0)
        elif week == 1:
            # Si on est dans la dernière semaine de l'année et que cette semaine fait partie de l'année suivante
            if date.month == 12:
                week = datetime.datetime(date.year, 12, 24).isocalendar().week + 1  # on considère qu'on dans la semaine 53 ou 54
                other_week = (date.year + 1, 1)
            # Si on est dans la première semaine de l'année qui commence l'année précédente
            elif datetime.datetime(date.year - 1, 12, 31).isocalendar().week == week:
                # On reporte le résultat sur la dernière semaine de l'année précédente
                other_week = (date.year - 1, datetime.datetime(date.year, 12, 24).isocalendar().week + 1)

        return week, other_week

    def get_daily_data(self, start, end, user, type=None):
        result = {}
        d = start or timezone.make_aware(datetime.datetime(2000, 1, 1))
        if end is None:
            end = timezone.now()

        while d <= end:
            if str(d.year) not in result:
                result[str(d.year)] = {}
            if str(d.month) not in result[str(d.year)]:
                result[str(d.year)][str(d.month)] = {}
            if str(d.day) not in result[str(d.year)][str(d.month)]:
                result[str(d.year)][str(d.month)][str(d.day)] = 0
            d += datetime.timedelta(days=1)

        # TODO: est-ce vraiment nécessaire ?
        assert(str(end.year) in result)
        assert(str(end.month) in result[str(end.year)])
        assert(str(end.day) in result[str(end.year)][str(end.month)])
        assert(result[str(end.year)][str(end.month)][str(end.day)] == 0)

        return result

    def get_weekly_data(self, start, end, user, type=None):
        result = {}
        d = start or timezone.make_aware(datetime.datetime(2000, 1, 1))
        if end is None:
            end = timezone.now()

        while d <= end:
            if str(d.year) not in result:
                result[str(d.year)] = {}

            week = d.isocalendar().week
            if d.month == 1 and week >= 52:
                week = 0  # semaine 52 ou 53 de l'année précédente
            elif d.month == 12 and week == 1:
                week = datetime.datetime(d.year, 12, 24).isocalendar().week + 1  # semaine 1 de l'année suivante

            if str(week) not in result[str(d.year)]:
                result[str(d.year)][str(week)] = 0

            d += datetime.timedelta(days=1)

        # TODO: est-ce vraiment nécessaire ?
        week = end.isocalendar().week
        if end.month == 1 and week >= 52:
            week = 0  # semaine 52 ou 53 de l'année précédente
        elif end.month == 12 and week == 1:
            week = datetime.datetime(end.year, 12, 24).isocalendar().week + 1  # semaine 1 de l'année suivante
        assert(str(end.year) in result)
        assert(str(week) in result[str(end.year)])
        assert(result[str(end.year)][str(week)] == 0)

        return result

    def get_monthly_data(self, start, end, user, type=None):
        result = {}
        d = start or timezone.make_aware(datetime.datetime(2000, 1, 1))
        if end is None:
            end = timezone.now()

        while d <= end:
            if str(d.year) not in result:
                result[str(d.year)] = {}
            if str(d.month) not in result[str(d.year)]:
                result[str(d.year)][str(d.month)] = 0
            d += datetime.timedelta(days=1)

        # TODO: est-ce vraiment nécessaire ?
        assert(str(end.year) in result)
        assert(str(end.month) in result[str(end.year)])
        assert(result[str(end.year)][str(end.month)] == 0)

        return result

    def get_yearly_data(self, start, end, user, type=None):
        result = {}
        d = start or timezone.make_aware(datetime.datetime(2000, 1, 1))
        if end is None:
            end = timezone.now()

        while d <= end:
            if str(d.year) not in result:
                result[str(d.year)] = 0
            d += datetime.timedelta(days=1)

        # TODO: est-ce vraiment nécessaire ?
        assert(str(end.year) in result)
        assert(result[str(end.year)] == 0)

        return result


class DataSourceCount(DataSource):

    def _get_data(self, start, end, user, type=None):
        activities = Activity.objects.all()
        if user is not None:
            activities = Activity.objects.filter(user=user)
        if start is not None:
            activities = activities.filter(date__gte=start)
        if end is not None:
            activities = activities.filter(date__lte=end)
        if type is not None:
            activities = activities.filter(type=type)
        activities = activities.values_list("date", flat=True)
        return activities

    def get_daily_data(self, start, end, user, type=None):
        result = super().get_daily_data(start, end, user, type)
        activities = self._get_data(start, end, user, type)

        for activity_date in activities:
            result[str(activity_date.year)][str(activity_date.month)][str(activity_date.day)] += 1

        return result

    def get_weekly_data(self, start, end, user, type=None):
        result = super().get_weekly_data(start, end, user, type)
        activities = self._get_data(start, end, user, type)

        for activity_date in activities:
            week, other_week = self.compute_week(activity_date)
            result[str(activity_date.year)][str(week)] += 1
            if other_week is not None:
                if str(other_week[0]) in result and str(other_week[1]) in result[str(other_week[0])]:
                    result[str(other_week[0])][str(other_week[1])] += 1

        return result

    def get_monthly_data(self, start, end, user, type=None):
        result = super().get_monthly_data(start, end, user, type)
        activities = self._get_data(start, end, user, type)

        for activity_date in activities:
            result[str(activity_date.year)][str(activity_date.month)] += 1

        return result

    def get_yearly_data(self, start, end, user, type=None):
        result = super().get_yearly_data(start, end, user, type)
        activities = self._get_data(start, end, user, type)

        for activity_date in activities:
            result[str(activity_date.year)] += 1

        return result


class DataSourceTotalDuration(DataSource):

    def _get_data(self, start, end, user, type=None):
        activities = Activity.objects.all()
        if user is not None:
            activities = Activity.objects.filter(user=user)
        if start is not None:
            activities = activities.filter(date__gte=start)
        if end is not None:
            activities = activities.filter(date__lte=end)
        if type is not None:
            activities = activities.filter(type=type)
        activities = activities.values_list("date", "duration")
        return activities

    def get_daily_data(self, start, end, user, type=None):
        result = super().get_daily_data(start, end, user, type)
        activities = self._get_data(start, end, user, type)

        for activity in activities:
            if activity[1] is not None:
                result[str(activity[0].year)][str(activity[0].month)][str(activity[0].day)] += activity[1]

        return result

    def get_weekly_data(self, start, end, user, type=None):
        result = super().get_weekly_data(start, end, user, type)
        activities = self._get_data(start, end, user, type)

        for activity in activities:
            if activity[1] is None:
                continue

            week, other_week = self.compute_week(activity[0])
            result[str(activity[0].year)][str(week)] += activity[1]
            if other_week is not None:
                if str(other_week[0]) in result and str(other_week[1]) in result[str(other_week[0])]:
                    result[str(other_week[0])][str(other_week[1])] += activity[1]

        return result

    def get_monthly_data(self, start, end, user, type=None):
        result = super().get_monthly_data(start, end, user, type)
        activities = self._get_data(start, end, user, type)

        for activity in activities:
            if activity[1] is not None:
                result[str(activity[0].year)][str(activity[0].month)] += activity[1]

        return result

    def get_yearly_data(self, start, end, user, type=None):
        result = super().get_yearly_data(start, end, user, type)
        activities = self._get_data(start, end, user, type)

        for activity in activities:
            if activity[1] is not None:
                result[str(activity[0].year)] += activity[1]

        return result


class DataSourceAverageDuration(DataSource):

    def _get_data(self, start, end, user, type=None):
        activities = Activity.objects.all()
        if user is not None:
            activities = Activity.objects.filter(user=user)
        if start is not None:
            activities = activities.filter(date__gte=start)
        if end is not None:
            activities = activities.filter(date__lte=end)
        if type is not None:
            activities = activities.filter(type=type)
        activities = activities.values_list("date", "duration")
        return activities

    def get_daily_data(self, start, end, user, type=None):
        result = super().get_daily_data(start, end, user, type)
        activities = self._get_data(start, end, user, type)

        count = copy.deepcopy(result)
        for activity in activities:
            result[str(activity[0].year)][str(activity[0].month)][str(activity[0].day)] += activity[1]
            count[str(activity[0].year)][str(activity[0].month)][str(activity[0].day)] += 1

        for year in result.keys():
            for month in result[year].keys():
                for day in result[year][month].keys():
                    if count[year][month][day] > 0:
                        result[year][month][day] /= count[year][month][day]

        return result

    def get_weekly_data(self, start, end, user, type=None):
        result = super().get_weekly_data(start, end, user, type)
        activities = self._get_data(start, end, user, type)

        count = copy.deepcopy(result)
        for activity in activities:
            if activity[1] is None:
                continue

            week, other_week = self.compute_week(activity[0])
            result[str(activity[0].year)][str(week)] += activity[1]
            count[str(activity[0].year)][str(week)] += 1
            if other_week is not None:
                if str(other_week[0]) in result and str(other_week[1]) in result[str(other_week[0])]:
                    result[str(other_week[0])][str(other_week[1])] += activity[1]
                    count[str(other_week[0])][str(other_week[1])] += 1

        for year in result.keys():
            for week in result[year].keys():
                if count[year][week] > 0:
                    result[year][week] /= count[year][week]

        return result

    def get_monthly_data(self, start, end, user, type=None):
        result = super().get_monthly_data(start, end, user, type)
        activities = self._get_data(start, end, user, type)

        count = copy.deepcopy(result)
        for activity in activities:
            result[str(activity[0].year)][str(activity[0].month)] += activity[1]
            count[str(activity[0].year)][str(activity[0].month)] += 1

        for year in result.keys():
            for month in result[year].keys():
                if count[year][month] > 0:
                    result[year][month] /= count[year][month]

        return result

    def get_yearly_data(self, start, end, user, type=None):
        result = super().get_yearly_data(start, end, user, type)
        activities = self._get_data(start, end, user, type)

        count = copy.deepcopy(result)
        for activity in activities:
            result[str(activity[0].year)] += activity[1]
            count[str(activity[0].year)] += 1

        for year in result.keys():
            if count[year] > 0:
                result[year] /= count[year]

        return result


def DataSourceMovingAverage(data, window):
    if window <= 0:
        return data

    newdata = []
    for i in range(len(data)):
        d, v = data[i]
        total = 0
        if i + 1 < window:
            start = 0
        else:
            start = i - window + 1
        for d, v in data[start:i+1]:
            total += v
        newdata.append((d, total / window))
    return newdata
