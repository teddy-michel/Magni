
import math


def _compute_t2(t1, d1, d2):
    if t1 == 0 or d1 == 0 or d2 == 0:
        return None
    if d1 == d2:
        return t1
    if d2 > 5 * d1 or 10 * d2 < d1:
        return None

    if d2 > d1:
        return t1 * (d2 / d1) * (1 + math.log(d2 / d1) / math.log(10000))
    else:
        return t1 * (d2 / d1) / (1 + math.log(d1 / d2) / math.log(10000))


def compute_previsions(records):
    previsions = {}
    best_prev = {d: (None, False) for d in record_distances_running}
    for d1 in record_distances_running:
        if str(d1) not in records["Running"]["distance"]:
            continue
        t1 = records["Running"]["distance"][str(d1)]["time"]
        previsions[d1] = {}
        for d2 in record_distances_running:
            t2 = _compute_t2(t1, d1, d2)
            previsions[d1][d2] = t2
            if t2 is not None and (best_prev[d2][0] is None or t2 < best_prev[d2][0]):
                best_prev[d2] = (t2, (d1 == d2))

    previsions["best"] = best_prev
    previsions["factor"] = {}

    for d in record_distances_running:
        if str(d) in records["Running"]["distance"]:
            t = records["Running"]["distance"][str(d)]["time"]
            previsions["factor"][d] = int(100.0 * (t - best_prev[d][0]) / best_prev[d][0])
        else:
            previsions["factor"][d] = 100

    return previsions


def do_tests():
    print("Run tests...")

    # Check that _compute_t2(_compute_t2(X)) == X
    distances = [500, 1000, 2000, 3000, 5000, 10000, 15000, 20000]
    for d1 in distances:
        for d2 in distances:
            if d1 == d2:
                continue
            t1 = 6666
            t2 = _compute_t2(t1, d1, d2)
            if t2 is None:
                print("%5d => %5d: %f => ?" % (d1, d2, t1))
                continue
            t1_computed = _compute_t2(t2, d2, d1)
            if t1_computed is None:
                print("%5d => %5d: %f => %f => ?" % (d1, d2, t1, t2))
                continue
            print("%5d => %5d: %f => %f => %f" % (d1, d2, t1, t2, t1_computed))

    """
    records = {
        500:     65,
        1000:   150,
        2000:   330,
        10000: 2400,
    }

    previsions = {}
    for d1, t1 in records.items():
        previsions[d1] = {}
        for d2 in records.keys():
            t2 = _compute_t2(t1, d1, d2)
            previsions[d1][d2] = t2

    str1 = "|       |"
    str2 = "+-------+"
    for d1 in records.keys():
        str1 += " %5d |" % d1
        str2 += "-------+"
    print(str2)
    print(str1)
    print(str2)

    for d1, d2_list in previsions.items():
        str = "| %5d |" % d1
        for d2, t2 in d2_list.items():
            if t2 is None:
                str += "       |"
            else:
                str += " %5d |" % t2
        print(str)

    print(str2)
    """


if __name__ == "__main__":
    do_tests()
else:
    from apps.base.globals import record_distances_running
