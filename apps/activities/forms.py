
import datetime
import re

from django.core.exceptions import ValidationError
from django.forms import IntegerField, Textarea, TextInput, CharField, DateField, TimeField
from django.forms import ModelForm, Form
from django.utils import timezone
from django.utils.timezone import localtime, make_naive, make_aware
from django.utils.translation import gettext as _, pgettext

from apps.activities.models import Activity, Note
from apps.base.forms import HTML5DateInput, convert_duration_string


def convert_time_shift_string(string):
    string = string.replace(" ", "")
    if string == "":
        return 0

    m = re.match("^(-?)([0-9]{1,2}):([0-9]{2})$", string)
    if m:
        hours = int(m.group(2))
        if hours > 12:
            return None

        minutes = int(m.group(3))
        if hours >= 60:
            return None

        if m.group(1) == "-":
            return -(hours * 60 + minutes)
        else:
            return hours * 60 + minutes


class ActivityForm(ModelForm):
    date_date = DateField(label=_("Date"), required=True, widget=HTML5DateInput(format="%Y-%m-%d"))
    #date_time = TimeField(label=pgettext("datetime", "Time"), required=True, widget=HTML5TimeInput)
    date_time = TimeField(label=pgettext("datetime", "Time"), required=True)
    time_shift_txt = CharField(label=_("Time shift"), max_length=6, required=False, initial=0, widget=TextInput(attrs={"placeholder": "HH:MM"}))
    feeling = IntegerField(label=_("Feeling"), min_value=0, max_value=5, initial=0)
    exertion = IntegerField(label=_("Exertion"), min_value=0, max_value=10, initial=0)
    duration_txt = CharField(label=_("Duration"), max_length=20, required=False, widget=TextInput(attrs={"placeholder": "HH:MM:SS"}), help_text=_("Accepted formats: '45min17', '2475s', '50:24', '1h17'..."))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        try:
            s, ms = divmod(self.instance.duration, 1000)
            value_h = s // 3600
            value_m = (s % 3600) // 60
            value_s = (s % 60)
            self.initial["duration_txt"] = _("%(hours)02d:%(minutes)02d:%(seconds)02d") % {"hours": value_h, "minutes": value_m, "seconds": value_s}
        except:
            pass

        if "time_shift" in self.initial:
            time_shift = self.initial["time_shift"]
        else:
            time_shift = self.instance.time_shift

        value_h = time_shift // 60
        value_m = time_shift % 60
        self.initial["time_shift_txt"] = _("%(hours)02d:%(minutes)02d") % {"hours": value_h, "minutes": value_m}

        now = localtime(timezone.now())
        self.initial["date_date"] = now
        self.initial["date_time"] = now.strftime("%H:%M:00")

        try:
            self.initial["date_date"] = self.instance.date_local
            self.initial["date_time"] = self.instance.date_local
        except:
            pass

    def clean_duration_txt(self):
        duration = convert_duration_string(self.cleaned_data["duration_txt"])
        if duration is None:
            raise ValidationError(_("Invalid format."))
        else:
            return duration

    def clean_time_shift_txt(self):
        time_shift = convert_time_shift_string(self.cleaned_data["time_shift_txt"])
        if time_shift is None:
            raise ValidationError(_("Invalid format."))
        else:
            return time_shift

    def save(self, commit=True):
        duration = self.cleaned_data.get("duration_txt")
        self.instance.duration = duration

        time_shift = self.cleaned_data.get("time_shift_txt")
        self.instance.time_shift = time_shift

        sdate = self.cleaned_data.get("date_date")
        stime = self.cleaned_data.get("date_time")

        date_utc = datetime.datetime.combine(sdate, stime) - datetime.timedelta(seconds=time_shift * 60)
        date_utc = date_utc.replace(tzinfo=datetime.timezone.utc)
        self.instance.date = date_utc

        return super().save(commit=commit)

    class Meta:
        model = Activity
        fields = ["name", "type", "date_date", "date_time", "time_shift_txt", "duration_txt",
                  "distance", "feeling", "exertion", "notes", "equipments"]
        widgets = {
            "notes": Textarea(attrs={"rows": 4}),
        }


class ActivityCreateForm(ActivityForm):
    class Meta:
        model = Activity
        fields = ["name", "type", "date_date", "date_time", "time_shift_txt", "planned", "duration_txt",
                  "distance", "feeling", "exertion", "notes", "equipments"]
        widgets = {
            "notes": Textarea(attrs={"rows": 4}),
        }


class ActivityAdvancedForm(ModelForm):
    class Meta:
        model = Activity
        fields = ["trimp", "tss", "level", "calories", "device", "heart_rate_avg", "heart_rate_max", "cadence_avg", "cadence_max",
                  "power_avg", "power_max", "elevation_gain", "elevation_loss", "speed_avg", "speed_max", "ngp", "duration_total"]


class SegmentForm(Form):
    time_start = CharField(label=_("Start"), max_length=20, required=True, widget=TextInput(attrs={"placeholder": "HH:MM:SS"}))
    time_end = CharField(label=_("End"), max_length=20, required=True, widget=TextInput(attrs={"placeholder": "HH:MM:SS"}))
    description = CharField(label=_("Description"), max_length=200, required=False)

    def clean_time_start(self):
        start = convert_duration_string(self.cleaned_data["time_start"])
        if start is None:
            raise ValidationError(_("Invalid format."))
        else:
            return start

    def clean_time_end(self):
        end = convert_duration_string(self.cleaned_data["time_end"])
        if end is None:
            raise ValidationError(_("Invalid format."))
        else:
            return end

    def clean(self):
        cleaned_data = super().clean()
        start = cleaned_data.get("time_start")
        end = cleaned_data.get("time_end")

        if end <= start:
            self.add_error("time_start", _("Start time must be lower than end time."))
            self.add_error("time_end", _("End time must be greater than start time."))
            raise ValidationError(_("End time must be greater than start time."))


class SegmentEditForm(Form):
    time_start = CharField(label=_("Start"), max_length=20, required=False, disabled=True)
    time_end = CharField(label=_("End"), max_length=20, required=False, disabled=True)
    description = CharField(label=_("Description"), max_length=200, required=False)
    distance = IntegerField(label=_("Distance"), min_value=0, max_value=999999, required=False)


class MultiSegmentForm(Form):
    time_start = CharField(label=_("Start"), max_length=20, required=True, widget=TextInput(attrs={"placeholder": "HH:MM:SS"}), initial="20:00")
    duration1 = CharField(label=_("Duration of effort"), max_length=10, required=True, widget=TextInput(attrs={"placeholder": "MM:SS"}), initial="01:00")
    duration2 = CharField(label=_("Duration of recuperation"), max_length=10, required=True, widget=TextInput(attrs={"placeholder": "MM:SS"}), initial="01:00")
    repetitions = IntegerField(label=_("Repetitions"), min_value=1, max_value=50, initial=5)

    def clean_time_start(self):
        start = convert_duration_string(self.cleaned_data["time_start"])
        if start is None:
            raise ValidationError(_("Invalid format."))
        else:
            return start

    def clean_duration1(self):
        duration = convert_duration_string(self.cleaned_data["duration1"])
        if duration is None:
            raise ValidationError(_("Invalid format."))
        else:
            return duration

    def clean_duration2(self):
        duration = convert_duration_string(self.cleaned_data["duration2"])
        if duration is None:
            raise ValidationError(_("Invalid format."))
        else:
            return duration


class NoteForm(ModelForm):
    date_start_date = DateField(label=_("Start date"), required=True, widget=HTML5DateInput(format="%Y-%m-%d"))
    date_start_time = TimeField(label=pgettext("datetime", "start time"), required=False)
    date_end_date = DateField(label=_("End date"), required=False, widget=HTML5DateInput(format="%Y-%m-%d"))
    date_end_time = TimeField(label=pgettext("datetime", "end time"), required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        try:
            self.initial["date_start_date"] = make_naive(self.instance.date_start)
            self.initial["date_start_time"] = make_naive(self.instance.date_start)
        except:
            now = localtime(timezone.now())
            self.initial["date_start_date"] = now
            self.initial["date_start_time"] = now.strftime("%H:%M:00")

        try:
            self.initial["date_end_date"] = make_naive(self.instance.date_end)
            self.initial["date_end_time"] = make_naive(self.instance.date_end)
        except:
            pass

    def clean(self):
        if self.cleaned_data.get("date_start_time"):
            date_start = make_aware(datetime.datetime.combine(self.cleaned_data.get("date_start_date"), self.cleaned_data.get("date_start_time")))
        else:
            date_start = make_aware(datetime.datetime.combine(self.cleaned_data.get("date_start_date"), datetime.time(0, 0, 0)))

        if self.cleaned_data.get("date_end_date"):
            if self.cleaned_data.get("date_end_time"):
                date_end = make_aware(datetime.datetime.combine(self.cleaned_data.get("date_end_date"), self.cleaned_data.get("date_end_time")))
            else:
                date_end = make_aware(datetime.datetime.combine(self.cleaned_data.get("date_end_date"), datetime.time(23, 59, 59)))

            if date_end < date_start:
                self.add_error("date_end_date", "End date must be after start date.")
        else:
            if self.cleaned_data.get("date_end_time"):
                self.add_error("date_end_date", "You can't specify an end time with an empty end date.")

        return super().clean()

    def save(self, commit=True):
        if self.cleaned_data.get("date_start_time"):
            self.instance.date_start = make_aware(datetime.datetime.combine(self.cleaned_data.get("date_start_date"), self.cleaned_data.get("date_start_time")))
        else:
            self.instance.date_start = make_aware(datetime.datetime.combine(self.cleaned_data.get("date_start_date"), datetime.time(0, 0, 0)))

        if self.cleaned_data.get("date_end_date"):
            if self.cleaned_data.get("date_end_time"):
                self.instance.date_end = make_aware(datetime.datetime.combine(self.cleaned_data.get("date_end_date"), self.cleaned_data.get("date_end_time")))
            else:
                self.instance.date_end = make_aware(datetime.datetime.combine(self.cleaned_data.get("date_end_date"), datetime.time(23, 59, 59)))

        return super().save(commit=commit)

    class Meta:
        model = Note
        fields = ["title", "date_start_date", "date_start_time", "date_end_date", "date_end_time", "content"]
        widgets = {
            "content": Textarea(attrs={"rows": 4}),
        }
