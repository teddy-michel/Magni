from zipfile import ZipFile

from apps.activities.fileformats import Importer, ImporterKML


class ImporterKMZ(Importer):

    def __init__(self, fileobj, filename):
        super().__init__(fileobj, filename)

    def get_extension(self):
        return ".kmz"

    def is_valid(self):
        try:
            self._fileobj.seek(0)
            with ZipFile(self._fileobj) as myzip:
                with myzip.open("doc.kml") as f:
                    kml = ImporterKML(f, "doc.kml")
                    return kml.is_valid()
        except:
            return False

    def _load_activities(self):
        activities = []
        errors = []

        try:
            self._fileobj.seek(0)
            with ZipFile(self._fileobj) as myzip:
                with myzip.open("doc.kml") as f:
                    kml = ImporterKML(f, "doc.kml")
                    if kml.is_valid():
                        activities, errors = kml.load_activities()
        except:
            pass

        return activities, errors
