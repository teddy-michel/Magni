
import datetime
import math

import dateutil.parser
from defusedxml import ElementTree
from django.utils.translation import gettext as _

from apps.activities.fileformats import Importer
from apps.activities.models import Activity


def distance_latlng(origin, destination):
    lat1, lon1 = origin
    lat2, lon2 = destination
    radius = 6371000  # m

    dlat = math.radians(lat2 - lat1)
    dlon = math.radians(lon2 - lon1)
    a = (math.sin(dlat / 2) * math.sin(dlat / 2) +
         math.cos(math.radians(lat1)) * math.cos(math.radians(lat2)) *
         math.sin(dlon / 2) * math.sin(dlon / 2))
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    d = radius * c

    return d


class ImporterGPX(Importer):

    def __init__(self, fileobj, filename):
        super().__init__(fileobj, filename)
        try:
            self._fileobj.seek(0)
            self._tree = ElementTree.XML(self._fileobj.read())
        except:
            self._tree = None

    def get_extension(self):
        return ".gpx"

    def is_valid(self):
        if self._tree is None:
            return False
        if self._check_element_tag(self._tree, "gpx"):
            return True
        return False

    def _load_activities(self):
        if self._tree is None:
            return [], [_("Invalid XML file.")]

        activities = []
        errors = []

        try:
            if self._check_element_tag(self._tree, "gpx"):
                activity, err = self._parse_element_gpx(self._tree)
                errors += err
                if activity is not None:
                    activities.append(activity)
            else:
                errors.append(_("Root element should be '%(e1)s' instead of '%(e2)s'.") % {"e1": "gpx", "e2": self._tree.tag})
        except:
            errors.append(_("Unknown error."))

        return activities, errors

    def _check_element_tag(self, element, name):
        if element.tag == name:
            return True

        schemas = [
            "http://www.topografix.com/GPX/1/0",
            "http://www.topografix.com/GPX/1/1",
            "http://www.garmin.com/xmlschemas/TrackPointExtension/v1",
        ]

        for schema in schemas:
            if element.tag == "{" + schema + "}" + name:
                return True

        return False

    def _parse_element_gpx(self, element):
        errors = []

        date = None
        segments = []

        for element1 in element:
            if self._check_element_tag(element1, "metadata"):
                for element2 in element1:
                    if self._check_element_tag(element2, "time"):
                        if date is not None:
                            errors.append(_("Multiple elements '%s'.") % "metadata/time")
                        try:
                            date = dateutil.parser.parse(element2.text)
                        except ValueError:
                            errors.append(_("Can't convert '%s' to datetime.") % "metadata/time")
                    else:
                        errors.append(_("Invalid element '%(e1)s' in '%(e2)s'.") % {"e1": element2.tag, "e2": "metadata"})
            elif self._check_element_tag(element1, "trk"):
                if len(segments) != 0:
                    errors.append(_("Multiple elements '%s'.") % "trk")
                    continue
                for element2 in element1:
                    if self._check_element_tag(element2, "trkseg"):
                        segment, seg_errors = self._parse_segment(element2)
                        segments.append(segment)
                        errors += seg_errors
                    elif self._check_element_tag(element2, "name"):
                        pass  # TODO: récupérer cette valeur ?
                    elif self._check_element_tag(element2, "desc"):
                        pass  # TODO: récupérer cette valeur ?
                    else:
                        errors.append(_("Invalid element '%(e1)s' in '%(e2)s'.") % {"e1": element2.tag, "e2": "trk"})
            else:
                errors.append(_("Invalid element '%(e1)s' in '%(e2)s'.") % {"e1": element1.tag, "e2": "gpx"})

        if len(segments) > 1:
            errors.append(_("Multiple elements '%s'.") % "trkseg")

        heartrate_max = 0
        heartrate_total = 0
        heartrate_count = 0
        maxdatetime = None

        # Search start time, end time, distance, and max heart rate
        for segment in segments:
            if segment["datetime_start"] is not None:
                if date is None or date > segment["datetime_start"]:
                    date = segment["datetime_start"]

            for point in segment["points"]:
                if point["heartrate"] is not None:
                    if heartrate_max < point["heartrate"]:
                        heartrate_max = point["heartrate"]
                    heartrate_total += point["heartrate"]
                    heartrate_count += 1

                if point["datetime"] is not None:
                    if maxdatetime is None or point["datetime"] > maxdatetime:
                        maxdatetime = point["datetime"]
                    if date is None or date > point["datetime"]:
                        date = point["datetime"]

        # Compute relative time for each point
        points = []
        for segment in segments:
            for point in segment["points"]:
                if point["datetime"] is not None:
                    point["time"] = (point["datetime"] - date) // datetime.timedelta(microseconds=1000)
                else:
                    point["time"] = None
                del point["datetime"]
                points.append(point)

        # Sort points
        points = sorted(points, key=lambda p: p["time"])

        distance = None
        elevation_gain = elevation_loss = 0
        previous_altitude = None
        previous_distance = None
        previous_distance_time = None
        previous_latitude = previous_longitude = None
        point_speed = None

        # Compute elevation, distance, and speed
        for point in points:
            if point["altitude"] is not None:
                if previous_altitude is not None:
                    elevation = point["altitude"] - previous_altitude
                    if elevation > 0.0:
                        elevation_gain += elevation
                    elif elevation < 0.0:
                        elevation_loss -= elevation
                previous_altitude = point["altitude"]

            if point["latitude"] is not None and point["longitude"] is not None:
                if previous_latitude is not None and previous_longitude is not None:
                    if distance is None:
                        distance = 0.0
                    distance += distance_latlng((previous_latitude, previous_longitude), (point["latitude"], point["longitude"]))
                previous_latitude = point["latitude"]
                previous_longitude = point["longitude"]

            if distance is not None:
                point["distance"] = distance

            if point["time"] is not None:
                if previous_distance is not None and previous_distance_time is not None:
                    d = point["distance"] - previous_distance
                    t = point["time"] - previous_distance_time
                    if t > 0:
                        point_speed = 1000 * d / t
                previous_distance = point["distance"]
                previous_distance_time = point["time"]

            point["speed"] = point_speed

        # Lissage de la vitesse
        speed_max = None
        for i in range(len(points)):
            if points[i]["speed"] is not None:
                speeds = [points[i]["speed"]]
                for j in range(1, 5):
                    if 0 <= i - j < len(points) and points[i - j]["speed"] is not None:
                        speeds.append(points[i - j]["speed"])
                    if 0 <= i + j < len(points) and points[i + j]["speed"] is not None:
                        speeds.append(points[i + j]["speed"])
                points[i]["speed"] = sum(speeds) / len(speeds)
                if speed_max is None or speed_max < points[i]["speed"]:
                    speed_max = points[i]["speed"]

        if distance is None:
            distance = 0.0

        sport = "Other"
        if maxdatetime is None or date is None:
            duration = 0
        else:
            duration = (maxdatetime - date) // datetime.timedelta(microseconds=1000)

            # Estimate sport
            if duration > 0:
                speed = 3600 * distance / duration
                if speed <= 6.5:
                    sport = "Walking"
                elif 8 <= speed <= 14:
                    sport = "Running"
                elif speed >= 18:
                    sport = "Biking"

        if heartrate_count > 0:
            heartrate_avg = heartrate_total // heartrate_count
        else:
            heartrate_avg = 0

        activity = Activity(
            date=date,
            type=sport,
            duration=duration,
            duration_total=duration,
            distance=int(distance),
            heart_rate_avg=heartrate_avg,
            heart_rate_max=heartrate_max,
            speed_max=speed_max,
            elevation_gain=elevation_gain,
            elevation_loss=elevation_loss,
        )

        activity.trackfile.points = points

        return activity, errors

    def _parse_segment(self, element):
        infos = {"datetime_start": None, "datetime_end": None, "points": []}
        errors = []

        # TODO: récupérer la cadence si disponible
        # TODO: récupérer la puissance si disponible

        for subelement in element:
            if self._check_element_tag(subelement, "trkpt"):
                point = {"datetime": None, "altitude": None, "latitude": None, "longitude": None, "distance": None, "heartrate": None}

                if "lat" in subelement.attrib:
                    try:
                        point["latitude"] = float(subelement.attrib["lat"])
                    except ValueError:
                        errors.append(_("Can't convert '%s' to float.") % "trkpt:lat")

                if "lon" in subelement.attrib:
                    try:
                        point["longitude"] = float(subelement.attrib["lon"])
                    except ValueError:
                        errors.append(_("Can't convert '%s' to float.") % "trkpt:lon")

                for element2 in subelement:
                    if self._check_element_tag(element2, "ele"):
                        try:
                            point["altitude"] = float(element2.text)
                        except ValueError:
                            errors.append(_("Can't convert '%s' to float.") % "trkpt/ele")
                    elif self._check_element_tag(element2, "time"):
                        try:
                            point["datetime"] = dateutil.parser.parse(element2.text)
                            if infos["datetime_start"] is None or point["datetime"] < infos["datetime_start"]:
                                infos["datetime_start"] = point["datetime"]
                            if infos["datetime_end"] is None or point["datetime"] > infos["datetime_end"]:
                                infos["datetime_end"] = point["datetime"]
                        except ValueError:
                            errors.append(_("Can't convert '%s' to datetime.") % "trkpt/time")
                    elif self._check_element_tag(element2, "extensions"):
                        for element3 in element2:
                            if self._check_element_tag(element3, "TrackPointExtension"):
                                for element4 in element3:
                                    if self._check_element_tag(element4, "hr"):
                                        try:
                                            point["heartrate"] = int(element4.text)
                                        except ValueError:
                                            errors.append(_("Can't convert '%s' to integer.") % "trkpt/extensions/TrackPointExtension/hr")
                                    else:
                                        errors.append(_("Invalid element '%(e1)s' in '%(e2)s'.") % {"e1": element4.tag, "e2": "trkpt/extensions/TrackPointExtension"})
                            else:
                                errors.append(_("Invalid element '%(e1)s' in '%(e2)s'.") % {"e1": element3.tag, "e2": "trkpt/extensions"})
                    else:
                        errors.append(_("Invalid element '%(e1)s' in '%(e2)s'.") % {"e1": element2.tag, "e2": "trkpt"})

                infos["points"].append(point)
            else:
                errors.append(_("Invalid element '%(e1)s' in '%(e2)s'.") % {"e1": subelement.tag, "e2": "trkseg"})

        return infos, errors
