
import datetime
import math

import dateutil.parser
from defusedxml import ElementTree
from django.utils.translation import gettext as _

from apps.activities.fileformats import Importer
from apps.activities.models import Activity


def distance_latlng(origin, destination):
    lat1, lon1 = origin
    lat2, lon2 = destination
    radius = 6371000  # m

    dlat = math.radians(lat2 - lat1)
    dlon = math.radians(lon2 - lon1)
    a = (math.sin(dlat / 2) * math.sin(dlat / 2) +
         math.cos(math.radians(lat1)) * math.cos(math.radians(lat2)) *
         math.sin(dlon / 2) * math.sin(dlon / 2))
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    d = radius * c

    return d


class ImporterKML(Importer):

    def __init__(self, fileobj, filename):
        super().__init__(fileobj, filename)
        try:
            self._fileobj.seek(0)
            self._tree = ElementTree.XML(self._fileobj.read())
        except:
            self._tree = None

    def get_extension(self):
        return ".kml"

    def is_valid(self):
        if self._tree is None:
            return False
        if self._check_element_tag(self._tree, "kml"):
            return True
        return False

    def _load_activities(self):
        if self._tree is None:
            return [], [_("Invalid XML file.")]

        errors = []
        points = []

        try:
            if self._check_element_tag(self._tree, "kml"):
                for element1 in self._tree:
                    if self._check_element_tag(element1, "Document"):
                        for element2 in element1:
                            if self._check_element_tag(element2, "name"):
                                pass
                            elif self._check_element_tag(element2, "author", "http://www.w3.org/2005/Atom"):
                                pass
                            elif self._check_element_tag(element2, "open"):
                                pass
                            elif self._check_element_tag(element2, "visibility"):
                                pass
                            elif self._check_element_tag(element2, "Style"):
                                pass
                            elif self._check_element_tag(element2, "StyleMap"):
                                pass
                            elif self._check_element_tag(element2, "Schema"):
                                pass
                            elif self._check_element_tag(element2, "TimeSpan"):
                                pass
                            elif self._check_element_tag(element2, "Placemark"):
                                for element3 in element2:
                                    if self._check_element_tag(element3, "name"):
                                        pass
                                    elif self._check_element_tag(element3, "description"):
                                        pass
                                    elif self._check_element_tag(element3, "TimeStamp"):
                                        pass
                                    elif self._check_element_tag(element3, "Style"):
                                        pass
                                    elif self._check_element_tag(element3, "styleUrl"):
                                        pass
                                    elif self._check_element_tag(element3, "visibility"):
                                        pass
                                    elif self._check_element_tag(element3, "LineString"):
                                        for element4 in element3:
                                            if self._check_element_tag(element4, "tessellate"):
                                                pass
                                            elif self._check_element_tag(element4, "coordinates"):
                                                pass  # TODO
                                            else:
                                                errors.append(_("Invalid element '%(e1)s' in '%(e2)s'.") % {"e1": element4.tag, "e2": "kml/Document/Placemark/LineString"})
                                    elif self._check_element_tag(element3, "Point"):
                                        pass
                                    elif self._check_element_tag(element3, "MultiTrack", "http://www.google.com/kml/ext/2.2"):
                                        for element4 in element3:
                                            if self._check_element_tag(element4, "altitudeMode"):
                                                pass
                                            elif self._check_element_tag(element4, "interpolate", "http://www.google.com/kml/ext/2.2"):
                                                pass
                                            elif self._check_element_tag(element4, "Track", "http://www.google.com/kml/ext/2.2"):
                                                track_points, track_errors = self._parse_track(element4)
                                                points += track_points
                                                errors += track_errors
                                            else:
                                                errors.append(_("Invalid element '%(e1)s' in '%(e2)s'.") % {"e1": element4.tag, "e2": "kml/Document/Placemark/gx:MultiTrack"})
                                    else:
                                        errors.append(_("Invalid element '%(e1)s' in '%(e2)s'.") % {"e1": element3.tag, "e2": "kml/Document/Placemark"})
                            else:
                                errors.append(_("Invalid element '%(e1)s' in '%(e2)s'.") % {"e1": element2.tag, "e2": "kml/Document"})
                    else:
                        errors.append(_("Invalid element '%(e1)s' in '%(e2)s'.") % {"e1": element1.tag, "e2": "kml"})
            else:
                errors.append(_("Root element should be '%(e1)s' instead of '%(e2)s'.") % {"e1": "kml", "e2": self._tree.tag})
        except:
            errors.append(_("Unknown error."))

        # Get date minimum and maximum
        date_min = date_max = None
        for point in points:
            if date_min is None or point["datetime"] < date_min:
                date_min = point["datetime"]
            if date_max is None or point["datetime"] > date_max:
                date_max = point["datetime"]

        # Compute relative time for each point
        for point in points:
            point["time"] = (point["datetime"] - date_min) // datetime.timedelta(microseconds=1000)
            del point["datetime"]

        # Sort points
        points = sorted(points, key=lambda p: p['time'])

        elevation_gain = elevation_loss = 0
        previous_latitude = previous_longitude = None
        prev_altitude = None
        prev_speed_distance = None
        prev_speed_time = None
        point_speed = None
        distance = 0.0

        # Compute distance, elevation and speed
        for point in points:
            if point["altitude"] is not None:
                if prev_altitude is not None:
                    elevation = point["altitude"] - prev_altitude
                    if elevation > 0.0:
                        elevation_gain += elevation
                    elif elevation < 0.0:
                        elevation_loss -= elevation
                prev_altitude = point["altitude"]

            if point["latitude"] is not None and point["longitude"] is not None:
                if previous_latitude is not None and previous_longitude is not None:
                    distance += distance_latlng((previous_latitude, previous_longitude), (point["latitude"], point["longitude"]))
                previous_latitude = point["latitude"]
                previous_longitude = point["longitude"]
            point["distance"] = distance

            if point["time"] is not None:
                if prev_speed_distance is not None and prev_speed_time is not None:
                    d = point["distance"] - prev_speed_distance
                    t = point["time"] - prev_speed_time
                    if t > 0:
                        point_speed = 1000 * d / t
                prev_speed_distance = point["distance"]
                prev_speed_time = point["time"]
            point["speed"] = point_speed

        # Lissage de la vitesse
        # TODO: move to external function?
        speed_max = None
        for i in range(len(points)):
            if points[i]["speed"] is not None:
                speeds = [points[i]["speed"]]
                for j in range(1, 7):
                    if 0 <= i - j < len(points) and points[i - j]["speed"] is not None:
                        speeds.append(points[i - j]["speed"])
                    if 0 <= i + j < len(points) and points[i + j]["speed"] is not None:
                        speeds.append(points[i + j]["speed"])
                points[i]["speed"] = sum(speeds) / len(speeds)
                if speed_max is None or speed_max < points[i]["speed"]:
                    speed_max = points[i]["speed"]

        if date_min is not None:
            duration = int((date_max - date_min) // datetime.timedelta(microseconds=1000))

            activity = Activity(
                date=date_min,
                duration=duration,
                duration_total=duration,
                distance=distance,
                speed_max=speed_max,
                elevation_gain=elevation_gain,
                elevation_loss=elevation_loss,
            )

            activity.trackfile.points = points
            activities = [activity]
        else:
            errors.append(_("No valid points."))
            activities = []

        return activities, errors

    def _check_element_tag(self, element, name, schemas=None):
        if element.tag == name:
            return True

        if schemas is None:
            schemas = ["http://www.opengis.net/kml/2.2"]
        elif not isinstance(schemas, list):
            schemas = [schemas]

        for schema in schemas:
            if element.tag == "{" + schema + "}" + name:
                return True

        return False

    def _parse_track(self, element):
        errors = []

        list_date = []
        list_coords = []
        for subelement in element:
            if self._check_element_tag(subelement, "when"):
                date = None
                try:
                    date = dateutil.parser.parse(subelement.text)
                except ValueError:
                    errors.append(_("Can't convert '%s' to datetime.") % "gx:Track/when")
                list_date.append(date)
            elif self._check_element_tag(subelement, "coord", "http://www.google.com/kml/ext/2.2"):
                try:
                    v = subelement.text.split(' ')
                    list_coords.append((float(v[1]), float(v[0]), float(v[2])))
                except:
                    errors.append(_("Can't convert '%s' to floats.") % "gx:Track/gx:coord")
                    list_coords.append((None, None, None))
            elif self._check_element_tag(subelement, "ExtendedData"):
                pass
            else:
                errors.append(_("Invalid element '%(e1)s' in '%(e2)s'.") % {"e1": subelement.tag, "e2": "gx:Track"})

        if len(list_date) > len(list_coords):
            errors.append(_("Error in gx:Track (not enough coords)."))
            list_date = list_date[0:len(list_coords)]

        points = []
        for i in range(len(list_date)):
            points.append({"datetime": list_date[i], "latitude": list_coords[i][0], "longitude": list_coords[i][1], "altitude": list_coords[i][2]})

        return points, errors
