import math
import struct
import traceback
import datetime
from io import SEEK_END

from django.utils.timezone import make_aware, is_aware
from django.utils.translation import gettext as _
from fitparse import FitFile

from apps.activities.fileformats import Importer
from apps.activities.models import Activity


class ImporterFIT(Importer):

    def __init__(self, fileobj, filename):
        super().__init__(fileobj, filename)

    def get_extension(self):
        return ".fit"

    def is_valid(self):
        try:
            self._fileobj.seek(0)
            filesize = self._fileobj.seek(0, SEEK_END)
            if filesize < 20:
                return False
            self._fileobj.seek(0)
            header_data = self._fileobj.read(12)
            if len(header_data) != 12:
                return False
            if header_data[8:12] != b".FIT":
                return False
            header = struct.unpack("<BBHII", header_data)
            #print(header)  # TODO: DEBUG
            if filesize != header[0] + header[3] + 2:
                return False
            #print("Valid FIT file!")  # TODO: DEBUG
            return True
        except:
            return False

    """
    @staticmethod
    def _get_field_size(type):
        if type == 0 or type == 1 or type == 2 or type == 10:
            return 1
        if type == 3 or type == 4 or type == 11:
            return 2
        if type == 5 or type == 6 or type == 8 or type == 12:
            return 4
        if type == 9 or type == 14 or type == 15 or type == 16:
            return 8
        if type == 7 or type == 13:  # array
            return -1
        return 0

    @staticmethod
    def _get_field_value(type, endianness, data):
        if endianness == 0:
            format = "<"
        else:
            format = ">"

        if type == 0:
            format += "B"
        elif type == 1:
            format += "b"
        elif type == 2:
            format += "B"
        elif type == 3:
            format += "h"
        elif type == 4:
            format += "H"
        elif type == 5:
            format += "i"
        elif type == 6:
            format += "I"
        elif type == 7:
            pass  # string
            return None
        elif type == 8:
            format += "f"
        elif type == 9:
            format += "d"
        elif type == 10:
            format += "B"
        elif type == 11:
            format += "H"
        elif type == 12:
            format += "I"
        elif type == 13:
            pass  # bytes
            return None
        elif type == 14:
            format += "q"
        elif type == 15:
            format += "Q"
        elif type == 16:
            format += "Q"
        else:
            return None

        return struct.unpack(format, data)[0]

    def _read_record(self, offset, definitions, messages):
        errors = []

        print("Read record at offset %d" % offset)  # TODO: DEBUG
        record_header_data = self._fileobj.read(1)
        if len(record_header_data) != 1:
            errors.append("Invalid file.")
            return None, definitions, messages, errors
        offset += 1

        record_header = struct.unpack("<B", record_header_data)

        compressed_header_flag = bool(record_header[0] & 0x80)
        if compressed_header_flag:
            print(" => Compressed timestamp header")  # TODO: DEBUG
            message_type = (record_header[0] & 0x60) >> 5
            time_offset = record_header[0] & 0x1f
            print(" => Time offset = %d" % time_offset)  # TODO: DEBUG
            definition_flag = False
            developper_data_flag = False
        else:
            print(" => Normal header")  # TODO: DEBUG
            definition_flag = bool(record_header[0] & 0x40)
            developper_data_flag = bool(record_header[0] & 0x20)
            if record_header[0] & 0x10:
                errors.append("Invalid record header (offset %d)." % offset)
            message_type = record_header[0] & 0x0f

        print(" => Message type = %d" % message_type)  # TODO: DEBUG

        if definition_flag:
            print(" => Definition message...")  # TODO: DEBUG
            definition = {"fields": [], "size": 0, "endianness": 0}
            print(" => Developper data flag = %d" % developper_data_flag)  # TODO: DEBUG

            record_content_data = self._fileobj.read(2)
            if len(record_content_data) != 2:
                errors.append("Invalid file.")
                return None, definitions, messages, errors
            offset += 2

            record_content = struct.unpack("<BB", record_content_data)
            if record_content[0] != 0:
                errors.append("Invalid definition message (offset %d)." % offset)
            definition["endianness"] = record_content[1]
            print(" => Record arch = %d" % definition["endianness"])  # TODO: DEBUG

            record_content_data = self._fileobj.read(3)
            if len(record_content_data) != 3:
                errors.append("Invalid file.")
                return None, definitions, messages, errors
            offset += 3

            if definition["endianness"] == 0:
                record_content = struct.unpack("<HB", record_content_data)
            else:
                record_content = struct.unpack(">HB", record_content_data)
            global_message_number = record_content[0]
            fields_count = record_content[1]
            print(" => Global message number = %d" % global_message_number)  # TODO: DEBUG
            print(" => Fields count = %d" % fields_count)  # TODO: DEBUG

            # Read fields
            for field in range(fields_count):
                field_desc_data = self._fileobj.read(3)
                if len(field_desc_data) != 3:
                    errors.append("Invalid file.")
                    return None, definitions, messages, errors
                offset += 3

                field_desc = struct.unpack("<BBB", field_desc_data)
                field_definition_number = field_desc[0]
                field_size = field_desc[1]
                field_base_type = field_desc[2]
                field_endianness = bool(field_base_type & 0x80)
                base_type_number = field_base_type & 0x1f
                print("   => Field: definition number=%d size=%d base type=%d (endianess=%d, type=%d)" % (field_definition_number, field_size, field_base_type, field_endianness, base_type_number))  # TODO: DEBUG
                definition["fields"].append({"number": field_definition_number, "size": field_size, "type": base_type_number, "endianness": field_endianness})
                definition["size"] += field_size

            if developper_data_flag:
                record_content_data = self._fileobj.read(1)
                if len(record_content_data) != 1:
                    errors.append("Invalid file.")
                    return None, definitions, messages, errors
                offset += 1
                developper_fields_count = struct.unpack("<B", record_content_data)[0]
                print(" => Developper fields count = %d" % developper_fields_count)  # TODO: DEBUG

                # Read fields
                for field in range(developper_fields_count):
                    field_desc_data = self._fileobj.read(3)
                    if len(field_desc_data) != 3:
                        errors.append("Invalid file.")
                        return None, definitions, messages, errors
                    offset += 3

                    # TODO
                    field_desc = struct.unpack("<BBB", field_desc_data)
                    print("   => Field: %d %d %d" % field_desc)  # TODO: DEBUG
                    definition["fields"].append({"number": -1, "size": field_desc[1], "type": -1, "endianness": -1})
                    definition["size"] += field_desc[1]

            definitions[message_type] = definition
        else:
            print(" => Data message...")  # TODO: DEBUG
            message = []
            if message_type not in definitions:
                errors.append("Unknown message type (%d)" % message_type)
                return None, definitions, messages, errors

            # Read message fields
            for field in definitions[message_type]["fields"]:
                field_data = self._fileobj.read(field["size"])
                if len(field_data) != field["size"]:
                    errors.append("Invalid file.")
                    return None, definitions, messages, errors
                offset += field["size"]

                message.append(self._get_field_value(field["type"], definitions[message_type]["endianness"], field_data))

            # TODO
            messages.append(message)

        return offset, definitions, messages, errors

    def _load_activities(self):
        activities = []
        errors = []

        try:
            self._fileobj.seek(0)
            header_data = self._fileobj.read(12)
            header = struct.unpack("<BBHII", header_data)
            if header[0] > 12:
                self._fileobj.seek(header[0] - 12, SEEK_CUR)
            offset = header[0]

            definitions = {}
            messages = []
            while offset < header[3]:
                offset, definitions, messages, record_errors = self._read_record(offset, definitions, messages)
                errors += record_errors
                if offset is None:
                    break
            print(messages)  # TODO: DEBUG

            # TODO
        except:
            raise
            pass

        return activities, errors
    """

    @staticmethod
    def _haversine(coord1, coord2):
        radius = 6372800.0  # Earth radius in meters
        lat1, lon1 = coord1
        lat2, lon2 = coord2

        phi1, phi2 = math.radians(lat1), math.radians(lat2)
        dphi = math.radians(lat2 - lat1)
        dlambda = math.radians(lon2 - lon1)

        a = math.sin(dphi / 2.0) ** 2.0 + math.cos(phi1) * math.cos(phi2) * math.sin(dlambda / 2.0) ** 2
        return 2.0 * radius * math.atan2(math.sqrt(a), math.sqrt(1.0 - a))

    def _load_activities(self):
        activities = []
        errors = []

        sports_fit = {"cycling": "Biking"}  # TODO: complete list

        try:
            self._fileobj.seek(0)
            fitfile = FitFile(self._fileobj)

            device = ""
            points = []
            for record in fitfile.get_messages():
                if record.name == "record":
                    points.append({
                        "datetime": record.get_value("timestamp").replace(tzinfo=datetime.timezone.utc),
                        "altitude": record.get_value("enhanced_altitude"),
                        "latitude": record.get_value("position_lat") * 180.0 / 2**31,
                        "longitude": record.get_value("position_long") * 180.0 / 2**31,
                        "distance": record.get_value("distance"),
                        "heartrate": record.get_value("heart_rate"),
                        "cadence": record.get_value("cadence"),
                        "power": record.get_value("power"),
                        "speed": record.get_value("enhanced_speed"),
                    })
                elif record.name == "device_info":
                    device = record.get_value("manufacturer")
                    version = record.get_value("software_version")
                    if version:
                        device += " " + str(version)
                elif record.name == "session":
                    sport = record.get_value("sport")
                    if sport in sports_fit:
                        sport = sports_fit[sport]
                    else:
                        errors.append("Unknown FIT sport ('%s')" % sport)
                        sport = "Other"

                    date = record.get_value("start_time").replace(tzinfo=datetime.timezone.utc)

                    # Compute relative time for each point
                    for point in points:
                        if point["datetime"] is not None:
                            point["time"] = (point["datetime"] - date) // datetime.timedelta(microseconds=1000)
                            del point["datetime"]
                        else:
                            point["time"] = None

                    elevation_gain = 0
                    elevation_loss = 0
                    prev_altitude = None
                    prev_latitude = None
                    prev_longitude = None
                    distance = 0.0

                    # Compute elevation
                    for point in points:
                        if point["altitude"] is not None:
                            if prev_altitude is not None:
                                elevation = point["altitude"] - prev_altitude
                                if elevation > 0.0:
                                    elevation_gain += elevation
                                elif elevation < 0.0:
                                    elevation_loss -= elevation
                            prev_altitude = point["altitude"]

                        if point["latitude"] is not None and point["longitude"] is not None:
                            if prev_latitude is not None and prev_longitude is not None:
                                distance += self._haversine((prev_latitude, prev_longitude), (point["latitude"], point["longitude"]))
                                point["distance"] = distance
                            prev_latitude = point["latitude"]
                            prev_longitude = point["longitude"]

                    #print("Elavation gain: %f (%f)" % (elevation_gain, record.get_value("total_ascent")))
                    #print("Elavation loss: %f (%f)" % (elevation_loss, record.get_value("total_descent")))

                    activity = Activity(
                        date=date,
                        type=sport,
                        duration=record.get_value("total_elapsed_time") * 1000.0,
                        duration_total=record.get_value("total_elapsed_time") * 1000.0,
                        distance=record.get_value("total_distance"),
                        heart_rate_avg=record.get_value("avg_heart_rate"),
                        heart_rate_max=record.get_value("max_heart_rate"),
                        cadence_avg=record.get_value("avg_cadence"),
                        cadence_max=record.get_value("max_cadence"),
                        power_avg=record.get_value("avg_power"),
                        power_max=record.get_value("max_power"),
                        speed_max=record.get_value("enhanced_max_speed"),
                        calories=record.get_value("total_calories"),
                        elevation_gain=elevation_gain,
                        elevation_loss=elevation_loss,
                        device=device,
                    )

                    activity.trackfile.points = points
                    activities.append(activity)
                    points = []
                """
                else:
                    print(record.name)
                    for record_data in record:
                        if record_data.value:
                            if record_data.units:
                                print(" * %s: %s %s" % (record_data.name, record_data.value, record_data.units))
                            else:
                                print(" * %s: %s" % (record_data.name, record_data.value))
                        else:
                            print(" * %s: -" % record_data.name)
                    print("")
                """

            if points:
                errors.append(_("Records without a session in FIT file."))
        except:
            traceback.print_exc()
            errors.append(_("Unknown error."))

        return activities, errors
