import json


class Importer(object):
    """ Classe de base des importeurs de fichier. """

    def __init__(self, fileobj, filename):
        self._fileobj = fileobj
        self._filename = filename

    def get_extension(self):
        """
        Retourne l'extension par défaut correspondant au format de fichier.
        Cette méthode doit être redéfinies dans les classes dérivées."""
        return None

    def is_valid(self):
        """
        Indique si le fichier est valide pour ce format de fichier.
        Cette méthode doit être redéfinies dans les classes dérivées."""
        return False

    def _load_activities(self):
        """
        Crée les activités contenues dans le fichier.
        Retourne une liste d'activités et une liste de messages d'erreur.
        Cette méthode doit être redéfinies dans les classes dérivées.
        """
        return [], []

    def load_activities(self):
        """
        Crée les activités contenues dans le fichier.
        Retourne une liste d'activités et une liste de messages d'erreur.
        """
        activities, errors = self._load_activities()

        for activity in activities:
            # Compute time shift
            time_shift_delta = activity.date.utcoffset()
            activity.time_shift = time_shift_delta.days * 24 * 60 + time_shift_delta.seconds // 60

            # Search pauses
            pauses = activity.trackfile.search_pauses()
            if pauses:
                pauses_duration = 0
                for pause in pauses:
                    pauses_duration += pause["duration"]
                if activity.duration is not None:
                    activity.duration -= pauses_duration

                try:
                    data = json.loads(activity.data)
                except:
                    data = {}

                data["pauses"] = pauses
                activity.data = json.dumps(data)

            if activity.speed_avg is None and activity.distance is not None and activity.duration is not None and activity.duration > 0:
                activity.speed_avg = 1000 * activity.distance / activity.duration

            # TODO: lisser la vitesse ?
            # TODO: lisser la puissance ?

        return activities, errors
