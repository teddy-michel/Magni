
import datetime
import json
import traceback

import dateutil.parser
from defusedxml import ElementTree
from django.utils.timezone import is_aware
from django.utils.translation import gettext as _

from apps.activities.fileformats import Importer
from apps.base.globals import sports
from apps.activities.models import Activity


class ImporterTCX(Importer):

    def __init__(self, fileobj, filename):
        super().__init__(fileobj, filename)
        try:
            self._fileobj.seek(0)
            self._tree = ElementTree.XML(self._fileobj.read())
        except:
            #traceback.print_exc()
            self._tree = None

    def get_extension(self):
        return ".tcx"

    def is_valid(self):
        if self._tree is None:
            return False
        if self._check_element_tag(self._tree, "TrainingCenterDatabase"):
            return True
        return False

    def _load_activities(self):
        if self._tree is None:
            return [], [_("Invalid XML file.")]

        activities = []
        errors = []

        try:
            if self._check_element_tag(self._tree, "TrainingCenterDatabase"):
                for element1 in self._tree:
                    if self._check_element_tag(element1, "Activities"):
                        for element2 in element1:
                            if self._check_element_tag(element2, "Activity"):
                                activity, err = self._parse_activity(element2)
                                errors += err
                                if activity is not None:
                                    activities.append(activity)
                    elif self._check_element_tag(element1, "Author"):
                        pass
                    else:
                        errors.append(_("Invalid element '%(e1)s' in '%(e2)s'.") % {"e1": element1.tag, "e2": "TrainingCenterDatabase"})
            else:
                errors.append(_("Root element should be '%(e1)s' instead of '%(e2)s'.") % {"e1": "TrainingCenterDatabase", "e2": self._tree.tag})
        except:
            traceback.print_exc()
            errors.append(_("Unknown error."))

        return activities, errors

    def _check_element_tag(self, element, name):
        if element.tag == name:
            return True

        schemas = [
            "http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2",
            "http://www.garmin.com/xmlschemas/ActivityExtension/v2",
        ]

        for schema in schemas:
            if element.tag == "{" + schema + "}" + name:
                return True

        return False

    def _parse_activity(self, element):
        errors = []

        sport = "Other"
        creator = None
        laps = []

        try:
            if "Sport" in element.attrib:
                sport = element.attrib["Sport"]
            if sport not in sports:
                errors.append(_("Invalid sport ('%s').") % sport)
                sport = "Other"

            for subelement in element:
                if self._check_element_tag(subelement, "Lap"):
                    lap, err = self._parse_activity_lap(subelement)
                    errors += err
                    laps.append(lap)
                elif self._check_element_tag(subelement, "Creator"):
                    if creator is None:
                        creator = self._parse_activity_creator(subelement)
                    else:
                        errors.append(_("Several element '%(e1)s' found in '%(e2)s'.") % {"e1": "Creator", "e2": "Activity"})
        except:
            errors.append(_("Error while parsing element 'Activity'."))
            traceback.print_exc()
            return None, errors

        date = None
        heartrate_max = None
        heartrate_total = 0
        heartrate_count = 0
        maxdatetime = None
        cadence_max = None
        cadence_total = 0
        cadence_count = 0
        power_max = None
        power_total = 0
        power_count = 0
        distance = 0
        calories = 0
        data = {}

        # Search start time, end time, distance, and max heart rate
        for lap in laps:
            if lap["datetime_start"] is not None:
                if date is None or date > lap["datetime_start"]:
                    date = lap["datetime_start"]

            if lap["calories"] is not None:
                calories += lap["calories"]

            for point in lap["points"]:
                if point["heartrate"] is not None:
                    if heartrate_max is None or heartrate_max < point["heartrate"]:
                        heartrate_max = point["heartrate"]
                    heartrate_total += point["heartrate"]
                    heartrate_count += 1

                if point["cadence"] is not None:
                    if cadence_max is None or cadence_max < point["cadence"]:
                        cadence_max = point["cadence"]
                    cadence_total += point["cadence"]
                    cadence_count += 1

                if point["power"] is not None:
                    if power_max is None or power_max < point["power"]:
                        power_max = point["power"]
                    power_total += point["power"]
                    power_count += 1

                if point["distance"] is not None:
                    if point["distance"] > distance:
                        distance = point["distance"]

                if point["datetime"] is not None:
                    if maxdatetime is None or point["datetime"] > maxdatetime:
                        maxdatetime = point["datetime"]
                    if date is None or date > point["datetime"]:
                        date = point["datetime"]

        # Utilisation des données des laps si la distance est nulle
        if distance == 0:
            for lap in laps:
                if lap["distance"] is not None:
                    distance += lap["distance"]

        # Compute relative time for each point
        points = []
        for lap_index in range(len(laps)):
            pt_index = 0
            for point in laps[lap_index]["points"]:
                point["_tmp_lap"] = lap_index
                point["_tmp_pt"] = pt_index
                if point["datetime"] is not None:
                    point["time"] = (point["datetime"] - date) // datetime.timedelta(microseconds=1000)
                else:
                    point["time"] = None
                del point["datetime"]
                points.append(point)
                pt_index += 1

        # Sort points
        points = sorted(points, key=lambda p: p["time"])

        def get_point_index(points:list, lap:int, pt:int) -> int:
            index = 0
            for index in range(len(points)):
                if points[index]["_tmp_lap"] == lap and points[index]["_tmp_pt"] == pt:
                    return index
            return None

        # Create segments for manual laps
        segments = []
        for lap_index in range(len(laps)):
            lap = laps[lap_index]
            if lap["method"] == "Manual" and lap["duration"] > 0:
                segments.append({
                    "time_start": (lap["datetime_start"] - date) // datetime.timedelta(microseconds=1000),
                    "time_end": (lap["datetime_end"] - date) // datetime.timedelta(microseconds=1000),
                    "duration": lap["duration"],
                    "distance": lap["distance"],
                    "point_start": get_point_index(points, lap_index, 0),
                    "point_end": get_point_index(points, lap_index, len(lap["points"]) - 1),
                    "speed": 1000 * lap["distance"] / lap["duration"],
                })

        data["segments"] = sorted(segments, key=lambda segment: segment["time_start"])

        elevation_gain = 0
        elevation_loss = 0
        prev_altitude = None
        #prev_distance = None
        prev_speed_distance = None
        prev_speed_time = None
        point_speed = None

        # Compute elevation and speed
        for point in points:
            del point["_tmp_lap"]
            del point["_tmp_pt"]

            if point["altitude"] is not None:
                if prev_altitude is not None:
                    elevation = point["altitude"] - prev_altitude
                    if elevation > 0.0:
                        elevation_gain += elevation
                    elif elevation < 0.0:
                        elevation_loss -= elevation
                prev_altitude = point["altitude"]

            if point["distance"] is not None:
                # La distance doit être une fonction croissante
                """
                if prev_distance is not None and point["distance"] < prev_distance:
                    point["distance"] = prev_distance  # TODO: faire une interpolation entre le point précédent et le point suivant valides ?
                    # TODO: compute distance from coordinates?
                prev_distance = point["distance"]
                """

                if point["time"] is not None:
                    if prev_speed_distance is not None and prev_speed_time is not None:
                        d = point["distance"] - prev_speed_distance
                        t = point["time"] - prev_speed_time
                        if t > 0:
                            point_speed = 1000 * d / t
                    prev_speed_distance = point["distance"]
                    prev_speed_time = point["time"]
            point["speed"] = point_speed

        # Lissage de la vitesse
        # TODO: move to external function?
        speed_max = None
        for i in range(len(points)):
            if points[i]["speed"] is not None:
                speeds = [points[i]["speed"]]
                for j in range(1, 7):
                    if 0 <= i - j < len(points) and points[i - j]["speed"] is not None:
                        speeds.append(points[i - j]["speed"])
                    if 0 <= i + j < len(points) and points[i + j]["speed"] is not None:
                        speeds.append(points[i + j]["speed"])
                points[i]["speed"] = sum(speeds) / len(speeds)
                if speed_max is None or speed_max < points[i]["speed"]:
                    speed_max = points[i]["speed"]

        # TODO: lissage de la puissance ?

        # Utilisation des données des laps si la vitesse maximale est nulle
        if speed_max is None:
            for lap in laps:
                if lap["speed_max"] is not None:
                    if speed_max is None or speed_max < lap["speed_max"]:
                        speed_max = lap["speed_max"]

        # Utilisation des données des laps si la durée est nulle
        if maxdatetime is None:
            duration = 0
            for lap in laps:
                if lap["duration"] is not None:
                    duration += lap["duration"]
        else:
            duration = (maxdatetime - date) // datetime.timedelta(microseconds=1000)

        # Utilisation des données des laps si la fréquence cardiaque moyenne ou maximale est nulle
        if heartrate_max is None:
            for lap in laps:
                if lap["heartrate_max"] is not None and (heartrate_max is None or heartrate_max < lap["heartrate_max"]):
                    heartrate_max = lap["heartrate_max"]

        if heartrate_count > 0:
            heartrate_avg = heartrate_total // heartrate_count
        else:
            heartrate_avg = None

        if cadence_count > 0:
            cadence_avg = cadence_total // cadence_count
        else:
            cadence_avg = None

        if power_count > 0:
            power_avg = power_total // power_count
        else:
            power_avg = None

        activity = Activity(
            date=date,
            type=sport,
            duration=duration,
            duration_total=duration,
            distance=distance,
            device=creator,
            heart_rate_avg=heartrate_avg,
            heart_rate_max=heartrate_max,
            cadence_avg=cadence_avg,
            cadence_max=cadence_max,
            power_avg=power_avg,
            power_max=power_max,
            speed_max=speed_max,
            calories=calories,
            elevation_gain=elevation_gain,
            elevation_loss=elevation_loss,
            data=json.dumps(data),
        )

        activity.trackfile.points = points

        return activity, errors

    def _parse_activity_lap(self, element):
        infos = {"datetime_start": None, "datetime_end": None, "method": None, "points": [], "duration": 0, "distance": 0, "calories": 0, "speed_max": None, "heartrate_avg": None, "heartrate_max": None}
        errors = []

        if "StartTime" in element.attrib:
            try:
                infos["datetime_start"] = dateutil.parser.parse(element.attrib["StartTime"])
            except ValueError:
                traceback.print_exc()
                errors.append(_("Can't convert '%s' to datetime.") % "Lap:StartTime")

        for subelement in element:
            if self._check_element_tag(subelement, "Track"):
                track, err = self._parse_activity_track(subelement)
                errors += err
                infos["points"] += track["points"]

                if infos["datetime_start"] is None or (track["datetime_start"] is not None and track["datetime_start"] < infos["datetime_start"]):
                    infos["datetime_start"] = track["datetime_start"]
                if infos["datetime_end"] is None or (track["datetime_end"] is not None and track["datetime_end"] > infos["datetime_end"]):
                    infos["datetime_end"] = track["datetime_end"]
            elif self._check_element_tag(subelement, "TotalTimeSeconds"):
                try:
                    infos["duration"] = int(float(subelement.text) * 1000)
                except ValueError:
                    traceback.print_exc()
                    errors.append(_("Can't convert '%s' to float.") % "Lap/TotalTimeSeconds")
            elif self._check_element_tag(subelement, "DistanceMeters"):
                try:
                    infos["distance"] = float(subelement.text)
                except ValueError:
                    traceback.print_exc()
                    errors.append(_("Can't convert '%s' to float.") % "Lap/DistanceMeters")
            elif self._check_element_tag(subelement, "Calories"):
                try:
                    infos["calories"] = int(subelement.text)
                except ValueError:
                    traceback.print_exc()
                    errors.append(_("Can't convert '%s' to integer.") % "Lap/Calories")
            elif self._check_element_tag(subelement, "MaximumSpeed"):
                try:
                    infos["speed_max"] = float(subelement.text)
                except ValueError:
                    traceback.print_exc()
                    errors.append(_("Can't convert '%s' to float.") % "Lap/MaximumSpeed")
            elif self._check_element_tag(subelement, "AverageHeartRateBpm"):
                for subelement2 in subelement:
                    if self._check_element_tag(subelement2, "Value"):
                        try:
                            infos["heartrate_avg"] = int(subelement2.text)
                        except ValueError:
                            traceback.print_exc()
                            errors.append(_("Can't convert '%s' to integer.") % "Lap/AverageHeartRateBpm")
                    else:
                        errors.append(_("Invalid element '%(e1)s' in '%(e2)s'.") % {"e1": subelement.tag, "e2": "Lap/AverageHeartRateBpm"})
            elif self._check_element_tag(subelement, "MaximumHeartRateBpm"):
                for subelement2 in subelement:
                    if self._check_element_tag(subelement2, "Value"):
                        try:
                            infos["heartrate_max"] = int(subelement2.text)
                        except ValueError:
                            traceback.print_exc()
                            errors.append(_("Can't convert '%s' to integer.") % "Lap/MaximumHeartRateBpm")
                    else:
                        errors.append(_("Invalid element '%(e1)s' in '%(e2)s'.") % {"e1": subelement.tag, "e2": "Lap/MaximumHeartRateBpm"})
            elif self._check_element_tag(subelement, "Cadence"):
                pass
            elif self._check_element_tag(subelement, "Intensity"):
                pass
            elif self._check_element_tag(subelement, "TriggerMethod"):
                infos["method"] = subelement.text
            elif self._check_element_tag(subelement, "Extensions"):
                pass
            else:
                errors.append(_("Invalid element '%(e1)s' in '%(e2)s'.") % {"e1": subelement.tag, "e2": "Lap"})

        if infos["duration"] == 0 and infos["datetime_start"] is not None and infos["datetime_end"] is not None:
            infos["duration"] = ((infos["datetime_end"] - infos["datetime_start"]).seconds + 1) * 1000

        return infos, errors

    def _parse_activity_creator(self, element):
        for subelement in element:
            if self._check_element_tag(subelement, "Name"):
                return subelement.text
        return None

    def _parse_activity_track(self, element):
        infos = {"datetime_start": None, "datetime_end": None, "points": []}
        errors = []

        for element2 in element:
            if self._check_element_tag(element2, "Trackpoint"):
                point = {"datetime": None, "altitude": None, "latitude": None, "longitude": None, "distance": None, "heartrate": None, "cadence": None, "power": None}
                for element3 in element2:
                    if self._check_element_tag(element3, "Time"):
                        try:
                            point["datetime"] = dateutil.parser.parse(element3.text)
                            if infos["datetime_start"] is None or point["datetime"] < infos["datetime_start"]:
                                infos["datetime_start"] = point["datetime"]
                            if infos["datetime_end"] is None or point["datetime"] > infos["datetime_end"]:
                                infos["datetime_end"] = point["datetime"]
                        except ValueError:
                            traceback.print_exc()
                            errors.append(_("Can't convert '%s' to datetime.") % "Trackpoint/Time")
                    elif self._check_element_tag(element3, "Position"):
                        for element5 in element3:
                            if self._check_element_tag(element5, "LatitudeDegrees"):
                                try:
                                    point["latitude"] = float(element5.text)
                                except ValueError:
                                    traceback.print_exc()
                                    errors.append(_("Can't convert '%s' to float.") % "Trackpoint/Position/LatitudeDegrees")
                            elif self._check_element_tag(element5, "LongitudeDegrees"):
                                try:
                                    point["longitude"] = float(element5.text)
                                except ValueError:
                                    traceback.print_exc()
                                    errors.append(_("Can't convert '%s' to float.") % "Trackpoint/Position/LongitudeDegrees")
                            else:
                                errors.append(_("Invalid element '%(e1)s' in '%(e2)s'.") % {"e1": element5.tag, "e2": "Trackpoint/Position"})
                    elif self._check_element_tag(element3, "AltitudeMeters"):
                        try:
                            point["altitude"] = float(element3.text)
                        except ValueError:
                            traceback.print_exc()
                            errors.append(_("Can't convert '%s' to float.") % "Trackpoint/AltitudeMeters")
                    elif self._check_element_tag(element3, "DistanceMeters"):
                        try:
                            point["distance"] = float(element3.text)
                        except ValueError:
                            traceback.print_exc()
                            errors.append(_("Can't convert '%s' to float.") % "Trackpoint/DistanceMeters")
                    elif self._check_element_tag(element3, "HeartRateBpm"):
                        for element5 in element3:
                            if self._check_element_tag(element5, "Value"):
                                try:
                                    point["heartrate"] = int(element5.text)
                                except ValueError:
                                    traceback.print_exc()
                                    errors.append(_("Can't convert '%s' to integer.") % "Trackpoint/HeartRateBpm/Value")
                            else:
                                errors.append(_("Invalid element '%(e1)s' in '%(e2)s'.") % {"e1": element5.tag, "e2": "Trackpoint/HeartRateBpm"})
                    elif self._check_element_tag(element3, "Cadence"):
                        if element3.text is not None:
                            try:
                                point["cadence"] = int(element3.text)
                            except ValueError:
                                traceback.print_exc()
                                errors.append(_("Can't convert '%s' to integer.") % "Trackpoint/Cadence")
                    elif self._check_element_tag(element3, "Extensions"):
                        for element4 in element3:
                            if self._check_element_tag(element4, "TPX"):
                                for element5 in element4:
                                    if self._check_element_tag(element5, "Watts"):
                                        if element5.text is not None:
                                            try:
                                                point["power"] = int(element5.text)
                                            except ValueError:
                                                traceback.print_exc()
                                                errors.append(_("Can't convert '%s' to integer.") % "Trackpoint/Extensions/TPX/Watts")
                                    elif self._check_element_tag(element5, "Speed"):
                                        pass  # TODO: récupérer cette valeur ?
                                    else:
                                        errors.append(_("Invalid element '%(e1)s' in '%(e2)s'.") % {"e1": element5.tag, "e2": "Extensions/TPX"})
                            else:
                                errors.append(_("Invalid element '%(e1)s' in '%(e2)s'.") % {"e1": element4.tag, "e2": "Extensions"})
                    elif self._check_element_tag(element3, "SensorState"):
                        pass
                    else:
                        errors.append(_("Invalid element '%(e1)s' in '%(e2)s'.") % {"e1": element3.tag, "e2": "Trackpoint"})
                infos["points"].append(point)
            else:
                errors.append(_("Invalid element '%(e1)s' in '%(e2)s'.") % {"e1": element2.tag, "e2": "Track"})

        return infos, errors
