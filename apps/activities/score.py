import datetime
import json
from math import exp

from dateutil.rrule import DAILY, rrule

from apps.base.globals import record_distances_running, record_distances_biking, record_durations, \
    record_power_durations, record_distances_swimming
from apps.activities.models import Activity


def get_heartrate_stats(points):
    heartrate_stats = {}
    previous_time = None

    for point in points:
        if point["time"] is not None:
            if previous_time is None:
                previous_time = point["time"]
            else:
                if point["heartrate"] is not None and point["heartrate"] >= 40:
                    if point["heartrate"] not in heartrate_stats:
                        heartrate_stats[point["heartrate"]] = 0

                    duration = point["time"] - previous_time
                    # Si 2 points sont séparés de plus de 30 secondes, on considère que c'est une pause
                    if duration > 30 * 1000:
                        duration = 30 * 1000

                    heartrate_stats[point["heartrate"]] += duration
                    previous_time = point["time"]

    return heartrate_stats


def get_power_stats(points):
    power_time = {}
    previous_time = None
    #previous_powers = []
    #max_powers = {}

    for point in points:
        if point["time"] is not None:
            if previous_time is None:
                previous_time = point["time"]
                continue
            if point["power"] is not None:
                duration = point["time"] - previous_time
                if duration > 0:
                    if point["power"] > 0:
                        if point["power"] not in power_time:
                            power_time[point["power"]] = 0
                        if duration > 2000:
                            duration = 2000
                        power_time[point["power"]] += duration

                    """
                    point_powers = []
                    if duration <= 3600000:
                        point_powers.append((duration, duration * point["power"]))
                        for d, p in previous_powers:
                            if d + duration > 3600000:
                                break
                            point_powers.append((d + duration, p + duration * point["power"]))

                        for d, p in point_powers:
                            if d < 1000:
                                continue
                            ms = d % 1000
                            if ms != 0:
                                p *= 1.0 - (ms / d)
                                d -= ms
                            p /= d
                            d //= 1000
                            if d not in max_powers or max_powers[d] < p:
                                max_powers[d] = p

                    previous_powers = point_powers
                    """

                previous_time = point["time"]

    #print(max_powers)  # DEBUG

    return power_time


def compute_trimp(heartrate_stats, user):
    if user.gender not in ["M", "F"] or user.heartrate_rest is None or user.heartrate_max is None:
        return None
    if user.heartrate_rest >= user.heartrate_max or user.heartrate_max > 220:
        return None

    if user.gender == "M":
        coef = 1.92
    elif user.gender == "F":
        coef = 1.67
    else:
        coef = 1.0

    trimp = 0.0
    for hr, time in heartrate_stats.items():
        tmp = (int(hr) - user.heartrate_rest) / (user.heartrate_max - user.heartrate_rest)
        trimp += time * tmp * 0.64 * exp(coef * tmp) / 60000

    return trimp


def compute_level(activity):
    if activity.speed_avg is None or activity.heart_rate_avg is None or activity.heart_rate_avg == 0:
        return None
    if activity.type != "Running":
        return None

    score = 60 * activity.speed_avg / activity.heart_rate_avg
    if score > 1.8:
        return 100.0
    if score < 0.8:
        return 0.0
    return 100 * (score - 0.8)


# TODO: améliorer l'algo
def compute_rtss(points, user):
    if user.threshold is None or user.threshold == 0:
        return None

    speed_time = {s / 10.0: 0 for s in range(0, 100)}
    previous_time = None

    for point in points:
        if point["time"] is not None:
            if previous_time is not None and point["ngp"] is not None:
                ngp = round(point["ngp"], 1)
                if ngp in speed_time:
                    speed_time[ngp] += point["time"] - previous_time
                else:
                    print("Error: NGP not in list (%f)" % point["ngp"])
            previous_time = point["time"]

    rtss = 0.0
    for s, time in speed_time.items():
        tmp = s / user.threshold
        rtss += 100 * (time / (15*1000)) * tmp * exp(tmp) / exp(1.0)

    return rtss


def compute_ngp(points):
    interval_duration = 30 * 1000  # en millisecondes

    ngp = None
    start_time = 0
    start_distance = 0
    start_altitude = None
    interval_points = []
    ngp_sum = 0.0
    duration_sum = 0

    for point in points:
        if start_altitude is None and point["altitude"] is not None:
            start_altitude = point["altitude"]
        if point["speed"] is not None:
            interval_points.append(point)
        if point["time"] is not None:
            duration = point["time"] - start_time
            if duration >= interval_duration:  # Nouvel interval
                if point["distance"] is not None:
                    distance = point["distance"] - start_distance
                    if point["altitude"] is not None:
                        if distance > 0:
                            grade = (point["altitude"] - start_altitude) / distance
                            if grade < 0.0:
                                coef = 1.815
                            else:
                                coef = 3.3
                            for p in interval_points:
                                p["ngp"] = p["speed"] * (1.0 + coef * grade)
                            interval_points = []
                            ngp_sum += distance * (1.0 - coef * grade)
                            duration_sum += duration
                        start_altitude = point["altitude"]
                        start_distance = point["distance"]
                        start_time = point["time"]

    if ngp_sum > 0:
        for point in points:
            if "ngp" not in point or point["ngp"] is None:
                point["ngp"] = point["speed"]

        if duration_sum > 0:
            ngp = 1000 * ngp_sum / duration_sum

    return points, ngp


def compute_fitness(scores, first_year, with_tss=False):
    """
        :scores is a dict with date as key and TSS as value.
        :years is a list of years for which fitness is computed.
    """

    date_start = datetime.date(first_year, 1, 1)
    date_end = datetime.datetime.now() + datetime.timedelta(days=180)

    # Fill scores with all date
    for date in rrule(DAILY, dtstart=date_start, until=date_end):
        date = date.date()
        if date not in scores:
            scores[date] = {"trimp": 0, "tss": 0, "level": 0, "fitness": 0, "fatigue": 0, "form": 0}

    scores = dict(sorted(scores.items()))

    alpha_fitness = 2.0 / (42 + 1)  # Moyenne glissante sur 42 jours
    alpha_fatigue = 2.0 / (7 + 1)   # Moyenne glissante sur 7 jours

    # Facteur de multiplication pour convertir les TRIMP en TSS (approximatif)
    # TODO: déterminer empiriquement
    if with_tss:
        factor_trimp_tss = 1.0
    else:
        factor_trimp_tss = 0.67

    previous_fitness = 0.0
    previous_fatigue = 0.0
    for date, score in scores.items():
        score["form"] = previous_fitness - previous_fatigue

        if with_tss:
            tss = score["tss"]
        else:
            tss = score["trimp"]

        score["fitness"] = previous_fitness * (1.0 - alpha_fitness) + tss * alpha_fitness * factor_trimp_tss
        score["fatigue"] = previous_fatigue * (1.0 - alpha_fatigue) + tss * alpha_fatigue * factor_trimp_tss
        previous_fitness = score["fitness"]
        previous_fatigue = score["fatigue"]

    return scores


def compute_best_performances(points, sport):
    records = []

    if sport == "Running":
        record_distances = record_distances_running
    elif sport == "Biking":
        record_distances = record_distances_biking
    elif sport == "HomeTrainer":
        record_distances = None
    else:
        return records

    if record_distances:
        records_by_distance = {d: None for d in record_distances}
        previous_distance_a = 0

    records_by_duration = {d: None for d in record_durations}
    records_by_power = {d: None for d in record_power_durations}

    record_all_durations = sorted(set(record_durations + record_power_durations))

    for point_a in range(len(points)):
        if points[point_a]["time"] is None:
            continue

        if record_distances:
            has_distance = True
            if points[point_a]["distance"] is None:
                has_distance = False

            if has_distance:
                if previous_distance_a is not None and previous_distance_a > points[point_a]["distance"]:
                    has_distance = False
                else:
                    previous_distance_a = points[point_a]["distance"]

            previous_distance_b = 0
            cur_distance_index = 0
            cur_distance = record_distances[cur_distance_index]
        else:
            has_distance = False

        cur_duration_index = 0
        cur_duration = record_all_durations[cur_duration_index]

        previous_time = points[point_a]["time"]
        power = None

        for point_b in range(point_a + 1, len(points)):
            if points[point_b]["time"] is None:
                continue

            if has_distance and points[point_b]["distance"] is not None:
                if previous_distance_b is not None and previous_distance_b > points[point_b]["distance"]:
                    continue

                previous_distance_b = points[point_b]["distance"]

                d = points[point_b]["distance"] - points[point_a]["distance"]
                if d <= 0.0:
                    d = None
            else:
                d = None

            if points[point_b]["power"] is not None and points[point_b]["power"] > 0:
                if power is None:
                    power = 0
                power_time = points[point_b]["time"] - previous_time
                if power_time > 2000:
                    power_time = 2000
                power += points[point_b]["power"] * power_time
                previous_time = points[point_b]["time"]

            t = points[point_b]["time"] - points[point_a]["time"]

            no_more_duration = False
            while t >= cur_duration:
                if has_distance and d is not None and cur_duration in records_by_duration:
                    estimated_distance = cur_duration * d / t
                    if records_by_duration[cur_duration] is None or estimated_distance > records_by_duration[cur_duration]["distance"]:
                        records_by_duration[cur_duration] = {
                            "type": "duration",
                            "distance": estimated_distance,
                            "point_start": point_a,
                            "point_end": point_b,
                            "time_start": points[point_a]["time"],
                            "time_end": points[point_b]["time"],
                        }

                if power is not None and cur_duration in records_by_power:
                    estimated_power = power / t
                    if records_by_power[cur_duration] is None or estimated_power > records_by_power[cur_duration]["power"]:
                        records_by_power[cur_duration] = {
                            "type": "power",
                            "power": estimated_power,
                            "point_start": point_a,
                            "point_end": point_b,
                            "time_start": points[point_a]["time"],
                            "time_end": points[point_b]["time"],
                        }

                cur_duration_index += 1
                if cur_duration_index >= len(record_all_durations):
                    no_more_duration = True
                    break
                cur_duration = record_all_durations[cur_duration_index]

            if has_distance and d is not None and points[point_b]["distance"] is not None:
                no_more_distance = False
                while d >= cur_distance:
                    estimated_time = cur_distance * t / d
                    if records_by_distance[cur_distance] is None or estimated_time < records_by_distance[cur_distance]["time"]:
                        records_by_distance[cur_distance] = {
                            "type": "distance",
                            "time": estimated_time,
                            "point_start": point_a,
                            "point_end": point_b,
                            "time_start": points[point_a]["time"],
                            "time_end": points[point_b]["time"],
                        }
                    cur_distance_index += 1
                    if cur_distance_index >= len(record_distances):
                        no_more_distance = True
                        break
                    cur_distance = record_distances[cur_distance_index]

                if no_more_distance and no_more_duration:
                    break

    for d in record_durations:
        if records_by_duration[d] is not None:
            record = records_by_duration[d]
            record["duration"] = d
            records.append(record)

    for d in record_power_durations:
        if records_by_power[d] is not None:
            record = records_by_power[d]
            record["duration"] = d
            records.append(record)

    if record_distances:
        for d in record_distances:
            if records_by_distance[d] is not None:
                record = records_by_distance[d]
                record["distance"] = d
                s, ms = divmod(record["time"], 1)
                if ms > 0.0:
                    record["time"] = s
                else:
                    record["time"] = s + 1
                records.append(record)

    return records


def update_user_records(user):
    records = {
        "Running": {
            "distance": {d: {"time": None, "distance": d, "activity_id": None} for d in record_distances_running},
            "duration": {d: {"distance": None, "duration": d, "activity_id": None} for d in record_durations},
            "power": {d: {"power": None, "duration": d, "activity_id": None} for d in record_power_durations},
            "max_distance": {"distance": None, "activity_id": None},
            "max_duration": {"duration": None, "activity_id": None},
        },
        "Biking": {
            "distance": {d: {"time": None, "distance": d, "activity_id": None} for d in record_distances_biking},
            "duration": {d: {"distance": None, "duration": d, "activity_id": None} for d in record_durations},
            "power": {d: {"power": None, "duration": d, "activity_id": None} for d in record_power_durations},
            "max_distance": {"distance": None, "activity_id": None},
            "max_duration": {"duration": None, "activity_id": None},
        },
        "HomeTrainer": {
            "power": {d: {"power": None, "duration": d, "activity_id": None} for d in record_power_durations},
            "max_duration": {"duration": None, "activity_id": None},
        },
        "Swimming": {
            "distance": {d: {"time": None, "distance": d, "activity_id": None} for d in record_distances_swimming},
            "max_distance": {"distance": None, "activity_id": None},
            "max_duration": {"duration": None, "activity_id": None},
        },
    }

    activities = Activity.objects.filter(type__in=["Running", "Biking", "HomeTrainer", "Swimming"], user=user)
    for activity in activities:
        if activity.type != "HomeTrainer":
            if activity.distance is not None and (records[activity.type]["max_distance"]["distance"] is None or activity.distance > records[activity.type]["max_distance"]["distance"]):
                records[activity.type]["max_distance"]["distance"] = activity.distance
                records[activity.type]["max_distance"]["activity_id"] = activity.id
            if activity.duration is not None and (records[activity.type]["max_duration"]["duration"] is None or activity.duration > records[activity.type]["max_duration"]["duration"]):
                records[activity.type]["max_duration"]["duration"] = activity.duration
                records[activity.type]["max_duration"]["activity_id"] = activity.id

        try:
            activity_records = json.loads(activity.data)["records"]
        except:
            activity_records = []

        for record in activity_records:
            if record["type"] == "duration":
                if activity.type == "HomeTrainer":
                    continue

                if "duration" not in record or record["duration"] is None:
                    continue
                if "distance" not in record or record["distance"] is None:
                    continue

                if record["duration"] not in record_durations:
                    continue

                if records[activity.type]["duration"][record["duration"]]["distance"] is None or record["distance"] > records[activity.type]["duration"][record["duration"]]["distance"]:
                    records[activity.type]["duration"][record["duration"]]["distance"] = record["distance"]
                    records[activity.type]["duration"][record["duration"]]["activity_id"] = activity.id
            elif record["type"] == "distance":
                if activity.type == "HomeTrainer":
                    continue

                if "distance" not in record or record["distance"] is None:
                    continue
                if "time" not in record or record["time"] is None:
                    continue

                if activity.type == "Running" and record["distance"] not in record_distances_running:
                    continue
                elif activity.type == "Biking" and record["distance"] not in record_distances_biking:
                    continue
                elif activity.type == "Swimming" and record["distance"] not in record_distances_swimming:
                    continue

                if records[activity.type]["distance"][record["distance"]]["time"] is None or record["time"] < records[activity.type]["distance"][record["distance"]]["time"]:
                    records[activity.type]["distance"][record["distance"]]["time"] = record["time"]
                    records[activity.type]["distance"][record["distance"]]["activity_id"] = activity.id
            elif record["type"] == "power":
                if "power" not in record or record["power"] is None:
                    continue
                if "duration" not in record or record["duration"] is None:
                    continue

                if record["duration"] not in record_power_durations:
                    continue

                if records[activity.type]["power"][record["duration"]]["power"] is None or record["power"] > records[activity.type]["power"][record["duration"]]["power"]:
                    records[activity.type]["power"][record["duration"]]["power"] = record["power"]
                    records[activity.type]["power"][record["duration"]]["activity_id"] = activity.id

    return records
