from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class ActivitiesAppConfig(AppConfig):
    name = "apps.activities"
    verbose_name = _("Activities")
