﻿
// Returns the ISO week of the date.
Date.prototype.getWeek = function() {
    var date = new Date(this.getTime());
    date.setHours(0, 0, 0, 0);
    // Thursday in current week decides the year.
    date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
    // January 4 is always in week 1.
    var week1 = new Date(date.getFullYear(), 0, 4);
    // Adjust to Thursday in week 1 and count number of weeks from date to week1.
    return 1 + Math.round(((date.getTime() - week1.getTime()) / 86400000 - 3 + (week1.getDay() + 6) % 7) / 7);
}

// Returns the four-digit year corresponding to the ISO week of the date.
Date.prototype.getWeekYear = function() {
    var date = new Date(this.getTime());
    date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
    return date.getFullYear();
}

/*
TODO:
- Voir si TODAY se retrouve à la fin du mois précédent ou au début du mois suivant
- Limiter le nombre d'évènements par jour
*/

class Calendar {

    locales = {
        today: "Today",
        week: "Week",
        activities: "Activities:",
        duration: "Duration:",
        trimp: "TRIMP:",
        days: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
        months: ["january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december"],
    };

    constructor(selector, weeks, events, notes) {
        this.selector = selector;

        this.weeks = weeks;
        this.events = events;
        this.notes = notes;

        this.show_notes = false;
        this.hidden_types = [];

        for (let eventIndex in this.events) {
            let event = this.events[eventIndex];
            if (event["end"] === undefined) {
                event["end"] = event["start"];
            }
        }

        for (let noteIndex in this.notes) {
            let note = this.notes[noteIndex];
            if (note["end"] === undefined) {
                note["end"] = note["start"];
            }
        }

        const today = new Date();
        today.setHours(0, 0, 0, 0);
        this.month = today.getMonth();
        this.year = today.getFullYear();
        this.today = today;

        this.init();

        const $this = this;
        window.setInterval(function() {
            const today = new Date();
            today.setHours(0, 0, 0, 0);
            if (today.getTime() != $this.today.getTime()) {
                $this.render();
            }
         }, 1000 * 60);
    }

    init() {
        $(this.selector).html('<div class="calendar-toolbox"><div><div class="btn-group"><button type="button" class="btn btn-primary" id="calendar-btn-month-prev"><span class="fa fa-chevron-left"></span></button><button type="button" class="btn btn-primary" id="calendar-btn-month-next"><span class="fa fa-chevron-right"></span></button></div> <div class="btn-group"><button type="button" class="btn btn-primary" id="calendar-btn-year-prev"><span class="fa fa-angle-double-left"></span></button><button type="button" class="btn btn-primary" id="calendar-btn-year-next"><span class="fa fa-angle-double-right"></span></button></div></div><h2 id="calendar-title"></h2><button type="button" class="btn btn-primary" id="calendar-btn-today">' + this.locales.today + '</button></div>');

        $(this.selector).append('<table class="calendar-table"><thead><tr><th>' + this.locales.week + '</th><th>' + this.locales.days[0] + '</th><th>' + this.locales.days[1] + '</th><th>' + this.locales.days[2] + '</th><th>' + this.locales.days[3] + '</th><th>' + this.locales.days[4] + '</th><th>' + this.locales.days[5] + '</th><th>' + this.locales.days[6] + '</th></tr></thead><tbody></tbody></table>');

        for (let w = 0; w < 6; ++w) {
            $(this.selector + " table.calendar-table").append('<tr id="calendar-week-' + w + '"><td><div class="calendar-week-number">0</div><div class="calendar-week-stats">' + this.locales.activities + ' <span class="calendar-week-activities">0</span></div><div class="calendar-week-stats">' + this.locales.duration + ' <span class="calendar-week-duration">0</span></div><div class="calendar-week-stats">' + this.locales.trimp + ' <span class="calendar-week-trimp">0</span></div></td></tr>');
            for (let d = 0; d < 7; ++d) {
                $("#calendar-week-" + w).append('<td class="calendar-day-' + d + '"><div class="calendar-day-number">0</div></td>');
            }
        }

        const $this = this;
        document.getElementById("calendar-btn-month-prev").onclick = function() { $this.goPrevMonth(); };
        document.getElementById("calendar-btn-month-next").onclick = function() { $this.goNextMonth(); };
        document.getElementById("calendar-btn-year-prev").onclick = function() { $this.goPrevYear(); };
        document.getElementById("calendar-btn-year-next").onclick = function() { $this.goNextYear(); };
        document.getElementById("calendar-btn-today").onclick = function() { $this.goToday(); };

        this.render();
    }

    goToday() {
        const today = new Date();
        var month = today.getMonth();
        var year = today.getFullYear();

        if (month != this.month || year != this.year) {
            this.month = month;
            this.year = year;
            this.render();
        }
    }

    goPrevMonth() {
        if (this.month == 0) {
            this.month = 11;
            this.year--;
        } else {
            this.month--;
        }
        this.render();
    }

    goNextMonth() {
        if (this.month == 11) {
            this.month = 0;
            this.year++;
        } else {
            this.month++;
        }
        this.render();
    }

    goPrevYear() {
        this.year--;
        this.render();
    }

    goNextYear() {
        this.year++;
        this.render();
    }

    hideNotes() {
        if (this.show_notes === true) {
            this.show_notes = false;
            this.render();
        }
    }

    showNotes() {
        if (this.show_notes === false) {
            this.show_notes = true;
            this.render();
        }
    }

    hideEventType(type) {
        if (!this.hidden_types.includes(type)) {
            this.hidden_types.push(type);
            this.render();
        }
    }

    showEventType(type) {
        if (this.hidden_types.includes(type)) {
            this.hidden_types = this.hidden_types.filter(function(item) {
                return item !== type;
            })

            this.render();
        }
    }

    daysInMonth(month, year) {
        if (month == 0 || month == 2 || month == 4 || month == 6 || month == 7 || month == 9 || month == 11) {
            return 31;
        }
        if (month == 3 || month == 5 || month == 8 || month == 10) {
            return 30;
        }
        if (year % 4 == 0) {
            if (year % 400 == 0) {
                return 28;
            } else {
                return 29;
            }
        } else {
            return 28;
        }
    }

    render() {
        // Update title
        let element_title = document.getElementById("calendar-title");
        let month_name = this.locales.months[this.month];
        element_title.textContent = month_name + " " + this.year;

        let days_list = [];
        let weeks_list = [];

        // Get DOM element for each day and each week
        for (let w = 0; w < 6; ++w) {
            days_list.push([]);
            weeks_list.push({"week": null, "year": null});
            for (let d = 0; d < 7; ++d) {
                days_list[w].push({"date": null, "day": null, "month": null, "year": null, "other_month": null, "element": null, "events": [], "rows": []});
                days_list[w][d]["element"] = $("tr#calendar-week-" + w + " td.calendar-day-" + d)[0];
            }
            weeks_list[w]["element"] = document.getElementById("calendar-week-" + w);
        }

        // Get the first day of the month
        let date = new Date(this.year, this.month, 1);
        let weekDay = date.getDay(); // 0 = dimanche, 1 = lundi, etc.
        if (weekDay == 0) {
            weekDay = 6;
        } else {
            weekDay--;
        }

        let current_day = 0;
        let next_month = false;

        // Get infos for each day displayed
        for (let w = 0; w < 6; ++w) {
            for (let d = 0; d < 7; ++d) {
                if (w == 0) {
                    if (d < weekDay) {
                        days_list[w][d]["other_month"] = true;

                        let previous_month;
                        let previous_year;
                        if (this.month == 0) {
                            previous_month = 11;
                            previous_year = this.year - 1;
                        } else {
                            previous_month = this.month - 1;
                            previous_year = this.year;
                        }

                        days_list[w][d]["day"] = this.daysInMonth(previous_month, previous_year) - weekDay + 1 + d;
                        days_list[w][d]["month"] = previous_month;
                        days_list[w][d]["year"] = previous_year;
                    } else {
                        current_day++;
                        days_list[w][d]["other_month"] = false;
                        days_list[w][d]["day"] = current_day;
                        days_list[w][d]["month"] = this.month;
                        days_list[w][d]["year"] = this.year;
                    }
                } else {
                    current_day++;
                    if (next_month) {
                        days_list[w][d]["other_month"] = true;
                        days_list[w][d]["day"] = current_day;

                        if (this.month == 11) {
                            days_list[w][d]["month"] = 0;
                            days_list[w][d]["year"] = this.year + 1;
                        } else {
                            days_list[w][d]["month"] = this.month + 1;
                            days_list[w][d]["year"] = this.year;
                        }
                    } else {
                        days_list[w][d]["other_month"] = false;
                        days_list[w][d]["day"] = current_day;
                        days_list[w][d]["month"] = this.month;
                        days_list[w][d]["year"] = this.year;

                        if (current_day >= this.daysInMonth(this.month, this.year)) {
                            next_month = true;
                            current_day = 0;
                        }
                    }
                }

                days_list[w][d]["date"] = new Date(days_list[w][d]["year"], days_list[w][d]["month"], days_list[w][d]["day"], 0, 0, 0, 0);
                days_list[w][d]["date_end"] = new Date(days_list[w][d]["year"], days_list[w][d]["month"], days_list[w][d]["day"], 0, 0, 0, 0);
                days_list[w][d]["date_end"].setDate(days_list[w][d]["day"] + 1);
            }

            weeks_list[w]["week"] = days_list[w][0]["date"].getWeek();
            weeks_list[w]["year"] = days_list[w][0]["date"].getWeekYear();
        }

        // Events
        $(".calendar-event").remove();
        $(".calendar-event-spacer").remove();

        let first_date = days_list[0][0]["date"];
        let last_date = new Date(first_date);
        last_date.setDate(first_date.getDate() + 6 * 7);

        for (let eventIndex in this.events) {
            let event = this.events[eventIndex];

            // Hide events by type
            if (this.hidden_types.includes(event["type"])) {
                continue
            }

            if ((event["start"] >= first_date && event["start"] <= last_date) || (event["end"] >= first_date && event["end"] <= last_date)) {
                if (event["start"] >= first_date) {
                    let d = (event["start"] - first_date) / 86400000;
                    let w = Math.floor(d / 7);
                    d = Math.floor(d - 7 * w);
                    days_list[w][d]["events"].push(event);
                }
            }
        }

        if (this.show_notes) {
            for (let noteIndex in this.notes) {
                let note = notes[noteIndex];
                note["_row"] = null;

                // Check if the note is in the range of all shown days for this month
                if (note["start"] < last_date && note["end"] > first_date) {
                    for (let w = 0; w < 6; ++w) {
                        for (let d = 0; d < 7; ++d) {
                            // Check if the note is in the range of the current day
                            if (note["start"] < days_list[w][d]["date_end"] && note["end"] > days_list[w][d]["date"]) {
                                days_list[w][d]["events"].push(note);
                            }
                        }
                    }
                }
            }
        }

        let today = new Date();
        today.setHours(0, 0, 0, 0);
        this.today = today;

        // Enable or disable button "Today"
        if (today.getMonth() == this.month && today.getFullYear() == this.year) {
            document.getElementById("calendar-btn-today").disabled = true;
        } else {
            document.getElementById("calendar-btn-today").disabled = false;
        }

        // Update CSS and DOM
        for (let w = 0; w < 6; ++w) {
            let notes_before_row = 0;

            for (let d = 0; d < 7; ++d) {
                let day = days_list[w][d];
                let element = day["element"];
                element.classList.remove("calendar-other-month");
                element.classList.remove("calendar-today");
                if (day["other_month"]) {
                    element.classList.add("calendar-other-month");
                }

                $(element).find(".calendar-day-number").text(day["day"]);

                if (day["date"].getTime() == today.getTime()) {
                    element.classList.add("calendar-today");
                }

                day["events"].sort(function(a, b) { return (a["start"] - b["start"]); });

                // Add the events which has started before the current day
                for (let eventIndex in day["events"]) {
                    let event = day["events"][eventIndex];
                    if (event["start"] < day["date"]) {
                        if (d == 0) {
                            event["_row"] = notes_before_row;
                            ++notes_before_row;
                        }
                        day["rows"][event["_row"]] = event;
                    }
                }

                // Add all the other events
                for (let eventIndex in day["events"]) {
                    let event = day["events"][eventIndex];
                    if (event["start"] >= day["date"]) {
                        let placed = false;
                        for (let row = 0; row < day["rows"].length; ++row) {
                            if (day["rows"][row] === undefined) {
                                day["rows"][row] = event;
                                event["_row"] = row;
                                placed = true;
                                break;
                            }
                        }
                        if (placed === false) {
                            event["_row"] = day["rows"].length;
                            day["rows"].push(event);
                        }
                    }
                }

                // Add events
                for (let eventIndex = 0; eventIndex < day["rows"].length; ++eventIndex) {
                    let event = day["rows"][eventIndex];

                    // TODO: limiter le nombre d'events ?
                    if (event === undefined) {
                        $(element).append('<a class="calendar-event-spacer">-</a>');
                    } else {
                        let code = '<a class="calendar-event';
                        if (event["start"].getTime() < day["date"].getTime()) {
                            code += " calendar-event-not-start";
                        }
                        if (event["end"].getTime() > day["date"].getTime() + 86400000) {
                            code += " calendar-event-not-end";
                        }
                        code += '" href="' + event["url"] + '" style="background-color: ' + event["color"] + '; border-color: ' + event["color"] + '; color: ' + event["textColor"] + ';">';
                        if (event["icon"]) {
                            code += '<i class="fas fa-' + event["icon"] + '"></i> ';
                        }
                        code += event["title"] + '</a>';
                        $(element).append(code);
                    }
                }
            }

            $(weeks_list[w]["element"]).find(".calendar-week-number").text(weeks_list[w]["week"]);

            if (this.weeks[weeks_list[w]["year"]] !== undefined && this.weeks[weeks_list[w]["year"]][weeks_list[w]["week"]] !== undefined) {
                $(weeks_list[w]["element"]).find(".calendar-week-activities").text(this.weeks[weeks_list[w]["year"]][weeks_list[w]["week"]]["count"]);
                $(weeks_list[w]["element"]).find(".calendar-week-duration").text(this.weeks[weeks_list[w]["year"]][weeks_list[w]["week"]]["duration"]);
                $(weeks_list[w]["element"]).find(".calendar-week-trimp").text(this.weeks[weeks_list[w]["year"]][weeks_list[w]["week"]]["trimp"]);
            } else {
                $(weeks_list[w]["element"]).find(".calendar-week-activities").text("0");
                $(weeks_list[w]["element"]).find(".calendar-week-duration").text("0");
                $(weeks_list[w]["element"]).find(".calendar-week-trimp").text("0");
            }
        }
    }
}
